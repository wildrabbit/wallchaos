
#include "cGraphicsLayer.h"
#include "cLog.h"
#include <string>
#include <sstream>
#include <stdio.h>

#include "cScene.h"
#include "cPlayer.h"
#include "cEnemy.h"
#include "cProjectile.h"
#include "cItem.h"

#include "cUtils.h"


cGraphicsLayer::cGraphicsLayer()
{
}

cGraphicsLayer::~cGraphicsLayer(){}

bool cGraphicsLayer::Init(HWND hWnd)
{
	cLog *Log = cLog::Instance();
	HRESULT hr;

	D3DVIEWPORT9 viewPort = { 0, 0, SCREEN_RES_X, SCREEN_RES_Y, 0.0f, 1.0f };

	g_pD3D = Direct3DCreate9( D3D_SDK_VERSION );
	if(g_pD3D==NULL)
	{
		Log->Msg("Error creating Direct3D object");
		return false;
	}

	D3DPRESENT_PARAMETERS d3dpp; 
	ZeroMemory( &d3dpp, sizeof( d3dpp ) );
#ifdef FSCREEN
	d3dpp.Windowed               = FALSE;
#else
	d3dpp.Windowed				= TRUE;
#endif
	d3dpp.SwapEffect             = D3DSWAPEFFECT_DISCARD;	//Efficient page flipping
	d3dpp.BackBufferWidth        = SCREEN_RES_X;
    d3dpp.BackBufferHeight       = SCREEN_RES_Y;
    d3dpp.BackBufferFormat       = D3DFMT_X8R8G8B8;

	hr = g_pD3D->CreateDevice(	D3DADAPTER_DEFAULT, 
								D3DDEVTYPE_HAL, 
								hWnd,
								D3DCREATE_SOFTWARE_VERTEXPROCESSING,
								&d3dpp, 
								&g_pD3DDevice );
	if(FAILED(hr))
	{
		Log->Error(hr,"Creating Direct3D device");
		return false;
	}

	hr = g_pD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	if(FAILED(hr))
	{
		Log->Error(hr,"Setting render state");
		return false;
	}

	hr = g_pD3DDevice->SetViewport(&viewPort);
	if(FAILED(hr))
	{
		Log->Error(hr,"Setting viewport");
		return false;
	}

	return true;
}

void cGraphicsLayer::Finalize()
{
	if(g_pD3DDevice)
	{
		g_pD3DDevice->Release();
		g_pD3DDevice = NULL;
	}
	if(g_pD3D)
	{
		g_pD3D->Release();
		g_pD3D = NULL;
	}
}


void cGraphicsLayer::LoadData(FPDATALOADER loader)
{
	D3DXCreateSprite( g_pD3DDevice, &g_pSprite ); 
	if (loader)
		(this->*loader)();

}


void cGraphicsLayer::UnLoadData(FPDATAUNLOADER unloader)
{
	if (unloader)
		(this->*unloader)();
	if(g_pSprite)
	{
		g_pSprite->Release();
		g_pSprite = NULL;
	}
}

void cGraphicsLayer::Render(TRenderingData* renderingData, FPRENDERER renderer)
{
	g_pD3DDevice->Clear( 0, NULL, D3DCLEAR_TARGET, 0xFF000000, 0, 0 );

	(this->*renderer)(renderingData);
	
	g_pD3DDevice->Present( NULL, NULL, NULL, NULL );
}
void cGraphicsLayer::loadGameData()
{
	textures.clear();
	LPDIRECT3DTEXTURE9 tmp;
	std::string path(DATA_FOLDER_IMAGES);
	path.append("\\");

	std::string fname;
			

	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(TILESET_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xFFFF00ff,NULL,NULL,&tmp);
	textures[TILESET_INDEX]=tmp;
	
	fname.clear();
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(MINIMAP_TILESET_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xFFFF00ff,NULL,NULL,&tmp);
	textures[MINIMAP_TILESET_INDEX]=tmp;

	fname.clear();

	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(SORA_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xFFFF00ff,NULL,NULL,&tmp);
	textures[SORA_INDEX]=tmp;
	fname.clear();
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(ENEMY_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xFFFF00FF,NULL,NULL,&tmp);
	textures[ENEMY_INDEX]=tmp;
	fname.clear();
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(PROJECTILE_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xFFFF00FF,NULL,NULL,&tmp);
	textures[PROJECTILE_INDEX]=tmp;
	fname.clear();
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(SHADOWS_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xFFFF00FF,NULL,NULL,&tmp);
	textures[SHADOWS_INDEX]=tmp;
	fname.clear();
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(GUI_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xFFFF00FF,NULL,NULL,&tmp);
	textures[GUI_INDEX]=tmp; 
	
	fname.clear();
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(ITEM_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xFFFF00FF,NULL,NULL,&tmp);
	textures[ITEM_INDEX]=tmp; 

	tmp=NULL;

	LPD3DXFONT font;
	D3DXCreateFont(g_pD3DDevice, 12, 0, FW_BOLD, 0, FALSE, 
						DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, 
						DEFAULT_PITCH | FF_DONTCARE, TEXT("Arial"), 
						&font );
	fonts[0]=font;
}
void cGraphicsLayer::loadIntroData()
{
	textures.clear();
	LPDIRECT3DTEXTURE9 tmp;
	std::string path(DATA_FOLDER_IMAGES);
	path.append("\\");

	std::string fname;
			

	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(INTRO_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0,NULL,NULL,&tmp);
	textures[INTRO_INDEX]=tmp;
	fonts.clear();

}
void cGraphicsLayer::loadMenuData()
{
	textures.clear();
	LPDIRECT3DTEXTURE9 tmp;
	std::string path(DATA_FOLDER_IMAGES);
	path.append("\\");

	std::string fname;
			

	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(MENU_BG_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0,NULL,NULL,&tmp);
	textures[MENU_BG_INDEX]=tmp;
	fname.clear();
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(MENU_CURSORS_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xFFFF00FF,NULL,NULL,&tmp);
	textures[MENU_CURSORS_INDEX]=tmp;
}
void cGraphicsLayer::loadGameEndedData()
{
	textures.clear();
	LPDIRECT3DTEXTURE9 tmp;
	std::string path(DATA_FOLDER_IMAGES);
	path.append("\\");

	std::string fname;
			
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(GOVER_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0,NULL,NULL,&tmp);
	textures[GOVER_INDEX]=tmp;
	fname.clear();
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(GCLEARED_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0,NULL,NULL,&tmp);
	textures[GCLEARED_INDEX]=tmp;
	fname.clear();
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(GUI_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xffff00ff,NULL,NULL,&tmp);
	textures[GUI_INDEX]=tmp;
	fname.clear();
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(MENU_CURSORS_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xFFFF00FF,NULL,NULL,&tmp);
	textures[MENU_CURSORS_INDEX]=tmp;
}
void cGraphicsLayer::loadLevelClearedData()
{
	textures.clear();
	std::string path(DATA_FOLDER_IMAGES);
	path.append("\\");

	std::string fname;
			

	LPDIRECT3DTEXTURE9 tmp;
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(LVLCLEARED_BG_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0,NULL,NULL,&tmp);
	textures[LVLCLEARED_BG_INDEX]=tmp;
	fname.clear();
	D3DXCreateTextureFromFileEx(g_pD3DDevice,fname.append(path).append(GUI_PATH).c_str(),0,0,1,0,D3DFMT_UNKNOWN,
								D3DPOOL_DEFAULT,D3DX_FILTER_NONE,D3DX_FILTER_NONE,
								0xFFFF00FF,NULL,NULL,&tmp);
	textures[GUI_INDEX]=tmp;

}
void cGraphicsLayer::unloadAllData()
{
	TTextureMapIterator it;
	for (it=textures.begin();it!=textures.end();it++)
	{
		if((it->second)!=NULL)
		{
			it->second->Release();
			it->second=NULL;
		}
	}
	textures.clear();
	TFontMap::iterator fIt;
	for (fIt=fonts.begin();fIt!=fonts.end();fIt++)
	{
		if ((fIt->second)!=NULL)
		{
			fIt->second->Release();
			fIt->second=NULL;
		}
	}
	fonts.clear();
}
void cGraphicsLayer::renderGameScene(TRenderingData* renderingData)
{
	if (renderingData && renderingData->gameData.scene)
	{	
		//Render the items partially transparent when the game's paused
		D3DCOLOR inkColor = renderingData->gameData.paused?0x77FFFFFF:0xFFFFFFFF;

		D3DVIEWPORT9 oldVP;
		g_pD3DDevice->GetViewport(&oldVP);

		//Draw the GUI bar on top
		g_pD3DDevice->BeginScene();
			D3DVIEWPORT9 guiVP ={0,0,SCREEN_RES_X,GUI_HEIGHT, 0.0f, 1.0f };
			g_pD3DDevice->SetViewport(&guiVP);
			g_pSprite->Begin(D3DXSPRITE_ALPHABLEND);
				drawInGameGUI(renderingData,inkColor);
			g_pSprite->End();
		g_pD3DDevice->EndScene();

		//...then, render the scene
		g_pD3DDevice->BeginScene();
			D3DVIEWPORT9 gameVP ={0,GUI_HEIGHT,SCREEN_RES_X,SCREEN_RES_Y-GUI_HEIGHT,0.0f,1.0f};
			g_pD3DDevice->SetViewport(&gameVP);
			
			g_pSprite->Begin(D3DXSPRITE_ALPHABLEND);
				int baseX,baseY;
				TGameRunningRenderingData gameData = renderingData->gameData;
				gameData.scene->getBasePosition(baseX,baseY);
				RECT tileRect;
				for (int z=0;z<SCENE_LAYERS;z++)
				{
					for (int y=0;y<SCENE_HEIGHT;y++)
					{
						for (int x=0;x<SCENE_WIDTH;x++)
						{
							TTile* tile = &(gameData.scene->map[y][x][z]);
							tileRect=gameData.scene->getTileRect(tile);
							int	tileIndex=tile->index;
							if (tileIndex!=NOTILE)
								g_pSprite->Draw(textures[TILESET_INDEX],&tileRect,NULL, 
										&D3DXVECTOR3( float((x-y-1)*(TILE_WIDTH>>1)-baseX), float((x+y)*(TILE_HEIGHT>>1)-baseY-z*TILE_HEIGHT), 0.0f), 
										inkColor);						
							if(z==1)
							{
								int scrX,scrY;
								gameData.scene->translateCoordinates(0,0,scrX,scrY);
								drawPlayer(gameData.player,x,y,scrX,scrY,inkColor);
								drawEnemies(gameData.enemies,x,y,scrX,scrY,inkColor);
								drawItems(gameData.items,x,y,scrX,scrY,inkColor);
							}
						}
					}
				}
				if(gameData.showMinimap && !gameData.paused)
					drawMinimap(renderingData,0x99ffffff);
			g_pSprite->End();
		g_pD3DDevice->EndScene();
		//Lower left corner
		//Restore the original viewport
		g_pD3DDevice->SetViewport(&oldVP);
	}
}
void cGraphicsLayer::renderIntroScene(TRenderingData *renderingData)
{
		g_pD3DDevice->BeginScene();
		g_pSprite->Begin(D3DXSPRITE_ALPHABLEND);
			g_pSprite->Draw(textures[INTRO_INDEX],NULL,NULL, 
				&D3DXVECTOR3( 0.0f, 0.0f, 0.0f), 
				0xffFFFFFF);
		g_pSprite->End();
		g_pD3DDevice->EndScene();
}
void cGraphicsLayer::renderMenuScene(TRenderingData* renderingData)
{
	if(renderingData)
	{
		TMouseRenderingData* menuData=&renderingData->menuData;
		g_pD3DDevice->BeginScene();
		g_pSprite->Begin(D3DXSPRITE_ALPHABLEND);
			g_pSprite->Draw(textures[MENU_BG_INDEX],NULL,NULL, 
				&D3DXVECTOR3( 0.0f, 0.0f, 0.0f), 
				0xffFFFFFF);
			if(menuData->mouseOverRect)
			{
				g_pSprite->Draw(textures[MENU_CURSORS_INDEX],menuData->mouseOverClippingRect,NULL,
					&D3DXVECTOR3(float(menuData->mouseOverRect->left-3),float(menuData->mouseOverRect->top-3),0.0f),
					0xFFFFFFFF);
			}
			g_pSprite->Draw(textures[MENU_CURSORS_INDEX],menuData->mouseCursorClippingRect,NULL,
				&D3DXVECTOR3(float(menuData->mouseX),float(menuData->mouseY),0.0f),
				0xFFFFFFFF);

		g_pSprite->End();
		g_pD3DDevice->EndScene();
	}
}
void cGraphicsLayer::renderGameEndedScene(TRenderingData* renderingData)
{
	if(renderingData)
	{
		TGameEndedRenderingData* gameEndedData=&renderingData->gameEndedData;
		g_pD3DDevice->BeginScene();
			g_pSprite->Begin(D3DXSPRITE_ALPHABLEND);
				int bgIndex = gameEndedData->gameCleared?GCLEARED_INDEX:GOVER_INDEX;
					g_pSprite->Draw(textures[bgIndex],NULL,NULL, 
						&D3DXVECTOR3( 0.0f, 0.0f, 0.0f), 
						0xffFFFFFF);
				RECT scoreRect = {96,32,224,64};
				g_pSprite->Draw(textures[GUI_INDEX],&scoreRect,NULL, 
						&D3DXVECTOR3( float(160), float(284), 0.0f), 
						0xffffffff);						
				int score = gameEndedData->score;
				std::string scoreStr = Utils::getPaddedIntegerString('0',8,score);
				drawNumericString(scoreStr,430,284);
				if(gameEndedData->mouseData.mouseOverRect)
				{
					g_pSprite->Draw(textures[MENU_CURSORS_INDEX],gameEndedData->mouseData.mouseOverClippingRect,NULL,
						&D3DXVECTOR3(float(gameEndedData->mouseData.mouseOverRect->left-3),float(gameEndedData->mouseData.mouseOverRect->top-3),0.0f),
						0xFFFFFFFF);
				}
				g_pSprite->Draw(textures[MENU_CURSORS_INDEX],gameEndedData->mouseData.mouseCursorClippingRect,NULL,
					&D3DXVECTOR3(float(gameEndedData->mouseData.mouseX),float(gameEndedData->mouseData.mouseY),0.0f),
					0xFFFFFFFF);

			g_pSprite->End();
		g_pD3DDevice->EndScene();
	}
}
void cGraphicsLayer::renderLevelClearedScene(TRenderingData* renderingData)
{
	if(renderingData)
	{
		TLevelClearedRenderingData* levelClearedData=&renderingData->levelClearedData;
		g_pD3DDevice->BeginScene();
			g_pSprite->Begin(D3DXSPRITE_ALPHABLEND);
			g_pSprite->Draw(textures[LVLCLEARED_BG_INDEX],NULL,NULL, 
						&D3DXVECTOR3( 0.0f, 0.0f, 0.0f), 
						0xffFFFFFF);
			
			//Hide the "press any key..."
			//A more correct alternative would be to simply use a font to render that text
			// -_-U
			if(levelClearedData->timeBonus>0)
			{
				RECT coverRect = {256,512,550,560};
				g_pSprite->Draw(textures[LVLCLEARED_BG_INDEX],&coverRect,NULL, 
						&D3DXVECTOR3( 256.0f, 460.0f, 0.0f), 
						0xffFFFFFF);
			}
			drawNumericString(Utils::getPaddedIntegerString('0',8,levelClearedData->levelBaseScore),496,160);
			drawNumericString(Utils::getPaddedIntegerString('0',3,levelClearedData->timeBonus),376,200);
			drawNumericString(Utils::getPaddedIntegerString('0',8,levelClearedData->timeScore),496,200);
			drawNumericString(Utils::getPaddedIntegerString('0',8,levelClearedData->levelTotalScore),496,272);
			drawNumericString(Utils::getPaddedIntegerString('0',8,levelClearedData->accumulatedScore),496,344);
			drawNumericString(Utils::getPaddedIntegerString('0',8,levelClearedData->totalScore),496,384);

			g_pSprite->End();
		g_pD3DDevice->EndScene();
	}
}
void cGraphicsLayer::drawPlayer(cPlayer* player,int cX, int cY,int screenZeroX, int screenZeroY,D3DCOLOR inkColor)
{
	int cellX,cellY;	
	player->getCellPosition(cellX,cellY);
	if (cellX==cX && cellY==cY)
	{
		int sX,sY;
		player->getPosition(sX,sY);
		RECT shadowRect;
		SetRect(&shadowRect,0,8,16,16);
		g_pSprite->Draw(textures[SHADOWS_INDEX],&shadowRect,NULL, 
			&D3DXVECTOR3( float(screenZeroX+sX-8), float(screenZeroY+sY-4), 0.0f),inkColor);
		g_pSprite->Draw(textures[player->getTextureID()],&player->getTextureDimensions(),NULL, 
			&D3DXVECTOR3( float(screenZeroX+sX-PLAYER_ANCHOR_OFFSET_X), float(screenZeroY+sY-PLAYER_ANCHOR_OFFSET_Y), 0.0f), (player->inGracePeriod()&& inkColor==0xffffffff)?0xccffff00:inkColor);
	}
	if (player->hasProjectiles())
	{
		for (TProjectileList::const_iterator it=player->getProjectiles().begin();it!=player->getProjectiles().end();it++)
		{
			drawProjectile(*it,cX, cY, screenZeroX,screenZeroY,inkColor);
		}
	}
}
void cGraphicsLayer::drawEnemies(TEnemyList* enemies,int cX, int cY, int screenZeroX, int screenZeroY,D3DCOLOR inkColor)
{
	for (TEnemyList::const_iterator it=enemies->begin();it!=enemies->end();it++)
	{
		cEnemy* enemy = *it;
		if(enemy)
		{
			int cellX,cellY;
			enemy->getCellPosition(cellX,cellY);
			if (cellX==cX && cellY==cY)
			{
				int sX,sY;
				enemy->getPosition(sX,sY);
				RECT shadowRect;
				SetRect(&shadowRect,0,16,16,24);
				g_pSprite->Draw(textures[SHADOWS_INDEX],&shadowRect,NULL, 
					&D3DXVECTOR3( float(screenZeroX+sX-8), float(screenZeroY+sY-3), 0.0f),inkColor);
				g_pSprite->Draw(textures[enemy->getTextureID()],&enemy->getTextureDimensions(),NULL, 
					&D3DXVECTOR3( float(screenZeroX+sX-ENEMY_ANCHOR_OFFSET_X), float(screenZeroY+sY-ENEMY_ANCHOR_OFFSET_Y), 0.0f),inkColor);
			}
		}
	}
}
void cGraphicsLayer::drawProjectile(cProjectile* projectile, int cX, int cY, int screenZeroX, int screenZeroY,D3DCOLOR inkColor)
{
	if(projectile)
	{
		int cellX,cellY;
		projectile->getCellPosition(cellX,cellY);
		if (cellX==cX && cellY==cY)
		{
			int sX,sY;
			projectile->getPosition(sX,sY);
			RECT shadowRect;
			SetRect(&shadowRect,0,0,8,8);
			g_pSprite->Draw(textures[SHADOWS_INDEX],&shadowRect,NULL, 
				&D3DXVECTOR3( float(screenZeroX+sX-4), float(screenZeroY+sY-4), 0.0f),inkColor);
			g_pSprite->Draw(textures[projectile->getTextureID()],&projectile->getTextureDimensions(),NULL, 
				&D3DXVECTOR3( float(screenZeroX+sX-PROJECTILE_ANCHOR_OFFSET_X), float(screenZeroY+sY-PROJECTILE_ANCHOR_OFFSET_Y-projectile->getElevation()), 0.0f),inkColor);

		}
	}
}
void cGraphicsLayer::drawItems(TItemList* items,int cX, int cY, int screenZeroX, int screenZeroY,D3DCOLOR inkColor)
{
	for (TItemList::const_iterator it=items->begin();it!=items->end();it++)
	{
		cItem* item = *it;
		if(item)
		{
			int cellX,cellY;
			item->getCellPosition(cellX,cellY);
			if (cellX==cX && cellY==cY)
			{
				int sX,sY;
				item->getPosition(sX,sY);

				if (item->getType()==ITEM_COIN)
				{
				RECT shadowRect;
				SetRect(&shadowRect,0,16,16,24);
				g_pSprite->Draw(textures[SHADOWS_INDEX],&shadowRect,NULL, 
					&D3DXVECTOR3( float(screenZeroX+sX-8), float(screenZeroY+sY-3), 0.0f),inkColor);
				}

				g_pSprite->Draw(textures[item->getTextureID()],&item->getTextureDimensions(),NULL, 
					&D3DXVECTOR3( float(screenZeroX+sX-ITEM_ANCHOR_OFFSET_X), float(screenZeroY+sY-ITEM_ANCHOR_OFFSET_Y), 0.0f),inkColor);
			}
		}
	}
}

void cGraphicsLayer::drawInGameGUI(TRenderingData* renderingData,D3DCOLOR inkColor)
{
	if(renderingData)
	{
		TGameRunningRenderingData* gameData = &renderingData->gameData;
		//Draw score
		RECT scoreRect = {96,32,224,64};
		g_pSprite->Draw(textures[GUI_INDEX],&scoreRect,NULL, 
			&D3DXVECTOR3( float(16), float(8), 0.0f), 
			inkColor);		
		int score = gameData->score;
		std::string scoreStr = Utils::getPaddedIntegerString('0',8,score);
		drawNumericString(scoreStr,152,8);

		//Draw player's lives
		RECT livesRect={192,64,224,96};
		g_pSprite->Draw(textures[GUI_INDEX],&livesRect,NULL, 
			&D3DXVECTOR3( float(16), float(48), 0.0f), 
			inkColor);
		RECT xRect={64,32,96,64};
		g_pSprite->Draw(textures[GUI_INDEX],&xRect,NULL, 
			&D3DXVECTOR3( float(48), float(48), 0.0f), 
			inkColor);	
		int playerLives= gameData->player->getNumLives();
		std::string livesStr = Utils::getPaddedIntegerString('0',2,playerLives);
		drawNumericString(livesStr,80,48);

		//draw HP
		int playerHP= gameData->player->getHP();
		int row,column;
		row=playerHP/2;
		column=playerHP%2;
		RECT hpRect={96*column,64+32*row,96*(column+1),96+32*row};
		int startX=152,startY=48;
		g_pSprite->Draw(textures[GUI_INDEX],&hpRect,NULL, 
			&D3DXVECTOR3( float(startX), float(startY), 0.0f), 
			inkColor);			

		//Draw remaining time
		RECT timesRect={224,64,256,96};
		g_pSprite->Draw(textures[GUI_INDEX],&timesRect,NULL, 
			&D3DXVECTOR3( float(656), float(8), 0.0f), 
			inkColor);
		int remainingTime= gameData->remainingTime;
		std::string timeStr = Utils::getPaddedIntegerString('0',3,remainingTime);
		drawNumericString(timeStr,688,8);

		//And last, draw the remaining enemies indicator
		RECT foeRect={192,96,224,128};
		g_pSprite->Draw(textures[GUI_INDEX],&foeRect,NULL, 
			&D3DXVECTOR3( float(656), float(48), 0.0f), 
			inkColor);
		g_pSprite->Draw(textures[GUI_INDEX],&xRect,NULL, 
			&D3DXVECTOR3( float(688), float(48), 0.0f), 
			inkColor);
		int remainingEnemies = gameData->enemies->size();
		std::string enemiesStr = Utils::getPaddedIntegerString('0',2,remainingEnemies);
		drawNumericString(enemiesStr,720,48);
	}
}
void cGraphicsLayer::drawNumericString(const std::string& digits,int startX,int startY)
{
	RECT digitRect;
	int digit,row,column;
	int sX=startX,sY=startY;
	for (unsigned int i=0;i<digits.length();i++)
	{
		digit=(int)digits.at(i)-'0';
		row=digit/8;
		column=digit%8;
		SetRect(&digitRect,32*column,32*row,32*(column+1),32*(row+1));
		g_pSprite->Draw(textures[GUI_INDEX],&digitRect,NULL, 
							&D3DXVECTOR3( float(sX), float(sY), 0.0f), 
							0xffffffff);
		sX+=32;
	}
}
void cGraphicsLayer::drawMinimap(TRenderingData* renderingData,D3DCOLOR inkColor)
{
	int baseX,baseY;
	TGameRunningRenderingData gameData = renderingData->gameData;
	gameData.scene->getBasePosition(baseX,baseY);
	RECT tileRect;
	float drawX,drawY;
	for (int y=0;y<SCENE_HEIGHT;y++)
	{
		for (int x=0;x<SCENE_WIDTH;x++)
		{
			if (gameData.scene->isFreeCell(x,y))
			{
				SetRect(&tileRect,0,MINIMAP_TILE_HEIGHT,MINIMAP_TILE_WIDTH,2*MINIMAP_TILE_HEIGHT);
			}
			else if(gameData.scene->isWallCell(x,y))
			{
				SetRect(&tileRect,0,0,MINIMAP_TILE_WIDTH,MINIMAP_TILE_HEIGHT);
			}
			else SetRect(&tileRect,MINIMAP_TILE_WIDTH,0,2*MINIMAP_TILE_WIDTH,MINIMAP_TILE_HEIGHT);
			drawX = float ((x-y-1)*(MINIMAP_TILE_WIDTH>>1)+MINIMAP_SCENE_Xo);
			drawY = float((x+y)*(MINIMAP_TILE_HEIGHT>>1)+MINIMAP_SCENE_Yo);
			g_pSprite->Draw(textures[MINIMAP_TILESET_INDEX],&tileRect,NULL, 
				&D3DXVECTOR3(drawX,drawY, 0.0f), inkColor);						
		}
	}
	//draw player
	int cellX,cellY;
	gameData.player->getCellPosition(cellX,cellY);
	drawX = float ((cellX-cellY-1)*(MINIMAP_TILE_WIDTH>>1)+MINIMAP_SCENE_Xo);
	drawY = float((cellX+cellY)*(MINIMAP_TILE_HEIGHT>>1)+MINIMAP_SCENE_Yo);
	RECT entityRect = {0,2*MINIMAP_TILE_HEIGHT,MINIMAP_TILE_WIDTH,3*MINIMAP_TILE_HEIGHT};
	g_pSprite->Draw(textures[MINIMAP_TILESET_INDEX],&entityRect,NULL,
		&D3DXVECTOR3( drawX,drawY,0.0f), inkColor);						
	//draw enemy
	SetRect(&entityRect,MINIMAP_TILE_WIDTH,MINIMAP_TILE_HEIGHT,2*MINIMAP_TILE_WIDTH,2*MINIMAP_TILE_HEIGHT);
	for (TEnemyList::const_iterator it = gameData.enemies->begin();it!=gameData.enemies->end();it++)
	{
		(*it)->getCellPosition(cellX,cellY);
		drawX = float ((cellX-cellY-1)*(MINIMAP_TILE_WIDTH>>1)+MINIMAP_SCENE_Xo);
		drawY = float((cellX+cellY)*(MINIMAP_TILE_HEIGHT>>1)+MINIMAP_SCENE_Yo);
		g_pSprite->Draw(textures[MINIMAP_TILESET_INDEX],&entityRect,NULL,
			&D3DXVECTOR3( drawX,drawY, 0.0f), inkColor);						
	}
	//draw items
	SetRect(&entityRect,MINIMAP_TILE_WIDTH,2*MINIMAP_TILE_HEIGHT,2*MINIMAP_TILE_WIDTH,3*MINIMAP_TILE_HEIGHT);
	for (TItemList::const_iterator it = gameData.items->begin();it!=gameData.items->end();it++)
	{
		(*it)->getCellPosition(cellX,cellY);
		drawX = float ((cellX-cellY-1)*(MINIMAP_TILE_WIDTH>>1)+MINIMAP_SCENE_Xo);
		drawY = float((cellX+cellY)*(MINIMAP_TILE_HEIGHT>>1)+MINIMAP_SCENE_Yo);
		g_pSprite->Draw(textures[MINIMAP_TILESET_INDEX],&entityRect,NULL,
			&D3DXVECTOR3( drawX,drawY, 0.0f), inkColor);						
	}

}