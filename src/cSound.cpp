#include "cSound.h"
cSoundItem::cSoundItem(const std::string& soundId):id(soundId),channel(-1),volume(MAX_VOLUME>>1){
defaultFreq=-1;}

const std::string& cSoundItem::getId() const
{
	return id;
}
const int& cSoundItem::getChannel() const
{
	return channel;
}
const int& cSoundItem::getVolume() const
{
	return volume;
}
void cSoundItem::raiseVolume()
{
	setVolume(volume+VOLUME_DELTA);
}
void cSoundItem::lowerVolume()
{
	setVolume(volume-VOLUME_DELTA);
}
const int& cSoundItem::getDefaultFreq() const
{
	return defaultFreq;
}
//
cSample::cSample(const std::string& soundId, FSOUND_SAMPLE* theSample): cSoundItem(soundId),sample(theSample)
{
}
cSample::~cSample()
{
	if (sample)
	{
		FSOUND_Sample_Free(sample);
		sample=NULL;
		volume=0;
		channel=-1;
	}
}
bool cSample::Play(bool loop)
{
	if(sample)
	{
		if(!FSOUND_Sample_SetMode(sample,(loop)?FSOUND_LOOP_NORMAL:FSOUND_LOOP_OFF))
			return false;
		if ((channel=FSOUND_PlaySound(FSOUND_FREE,sample))>=0)
		{
			defaultFreq=FSOUND_GetFrequency(channel);
			return true;
		}
	}
	return false;
		
}
bool cSample::Pause()
{
	//TODO: check isPlaying's return value when paused
	if (sample && channel>=0 && FSOUND_IsPlaying(channel) && !FSOUND_GetPaused(channel))
		return (FSOUND_SetPaused(channel, 1))?true:false;
	return false;
}
bool cSample::Unpause()
{
	//watch out for "isPlaying"
	if (sample && channel>=0 && FSOUND_IsPlaying(channel) && FSOUND_GetPaused(channel))
		return (FSOUND_SetPaused(channel,0))?true:false;
	return false;
}
bool cSample::Stop()
{
	if(sample && channel>=0 && FSOUND_IsPlaying(channel))
		return (FSOUND_StopSound(channel))?true:false;
	return false;
}
bool cSample::isPlaying()
{
	return sample && channel>=0 && FSOUND_IsPlaying(channel);
}
bool cSample::isPaused()
{
	return sample && channel>=0 && FSOUND_GetPaused(channel);
}
bool cSample::Reset()
{
	if(sample && channel>=0)
	{
		return (FSOUND_SetCurrentPosition(channel,0))?true:false;
	}
	return false;
}
void cSample::setVolume(int volume)
{
	if(sample && channel>=0)
	{
		int adjustVolume = (volume<0)?0:(volume>MAX_VOLUME)?MAX_VOLUME:volume;
		FSOUND_SetVolume(channel,adjustVolume);
		volume=FSOUND_GetVolume(channel);
	}
}
void cSample::speedUpSound(float amount)
{
	if(sample && channel>=0)
	{
		if (defaultFreq==-1)
			defaultFreq = FSOUND_GetFrequency(channel);
		FSOUND_SetFrequency(channel,(int)(FSOUND_GetFrequency(channel)*amount));
	}
}
void cSample::slowDownSound(float amount)
{
	if (sample && channel>=0 && amount>0.00000000001f)
	{
		if(defaultFreq==-1)
			defaultFreq=FSOUND_GetFrequency(channel);
		FSOUND_SetFrequency(channel,(int)(FSOUND_GetFrequency(channel)/amount));
	}
}
void cSample::restoreSoundSpeed()
{
	if (sample && channel>=0 && defaultFreq>=0 && FSOUND_GetFrequency(channel)!=defaultFreq)
		FSOUND_SetFrequency(channel,defaultFreq);
}


//---
cStream::cStream(const std::string& soundId, FSOUND_STREAM* theStream): cSoundItem(soundId),stream(theStream)
{
}
cStream::~cStream()
{
	if (stream)
	{
		FSOUND_Stream_Close(stream);
		stream=NULL;
		volume=0;
		channel=-1;
	}
}
bool cStream::Play(bool loop)
{
	if(stream)
	{
		if(!FSOUND_Stream_SetMode(stream,(loop)?FSOUND_LOOP_NORMAL:FSOUND_LOOP_OFF))
			return false;
		if ((channel=FSOUND_Stream_Play(FSOUND_FREE,stream))>=0)
			return true;
	}
	return false;
		
}
bool cStream::Pause()
{
	//TODO: check isPlaying's return value when paused
	if (!isPaused())
		return (FSOUND_SetPaused(channel, 1))?true:false;
	return false;
}
bool cStream::Unpause()
{
	//watch out for "isPlaying"
	if (isPaused())
		return (FSOUND_SetPaused(channel,0))?true:false;
	return false;
}
bool cStream::Stop()
{
	if(isPlaying())
		return (FSOUND_Stream_Stop(stream))?true:false;
	return false;
}
bool cStream::isPlaying()
{
	return stream && channel>=0 && FSOUND_IsPlaying(channel);
}
bool cStream::isPaused()
{
	return stream && channel>=0 && FSOUND_IsPlaying(channel) && FSOUND_GetPaused(channel);
}
bool cStream::Reset()
{
	if(stream && channel>=0)
	{
		return (FSOUND_Stream_SetPosition(stream,0))?true:false;
	}
	return false;
}
void cStream::setVolume(int volume)
{
	if(stream && channel>=0)
	{
		int adjustVolume = (volume<0)?0:(volume>MAX_VOLUME)?MAX_VOLUME:volume;
		FSOUND_SetVolume(channel,adjustVolume);
		volume=FSOUND_GetVolume(channel);
	}
}
void cStream::speedUpSound(float amount)
{
	if(stream && channel>=0)
	{
		if (defaultFreq==-1)
			defaultFreq = FSOUND_GetFrequency(channel);
		FSOUND_SetFrequency(channel,(int)(FSOUND_GetFrequency(channel)*amount));
	}
}
void cStream::slowDownSound(float amount)
{
	if (stream && channel>=0 && amount>0.00000000001f)
	{
		if(defaultFreq==-1)
			defaultFreq=FSOUND_GetFrequency(channel);
		FSOUND_SetFrequency(channel,(int)(FSOUND_GetFrequency(channel)/amount));
	}
}
void cStream::restoreSoundSpeed()
{
	if (stream && channel>=0 && defaultFreq>=0 && FSOUND_GetFrequency(channel)!=defaultFreq)
		FSOUND_SetFrequency(channel, defaultFreq);
}

//----------------------------------------------
cSequence::cSequence(const std::string &id, FMUSIC_MODULE *theSequence):cSoundItem(id),
sequence(theSequence){}
cSequence::~cSequence()
{
	if (sequence)
	{
		FMUSIC_FreeSong(sequence);
		sequence=NULL;
		volume=0;
		channel=-1;
	}
}
bool cSequence::Play(bool loop)
{
	if(sequence)
	{
		if (!FMUSIC_SetLooping(sequence,loop))
			return false;
		if (!FMUSIC_PlaySong(sequence))
			return false;
	}
	return true;
}
bool cSequence::Reset()
{
	return true;//does nothing
}
bool cSequence::Stop()
{
	if (sequence)
		return (FMUSIC_StopSong(sequence))?true:false;
	return false;
}
bool cSequence::Pause()
{
	
	if (sequence)
	{
		signed char playing=FMUSIC_IsPlaying(sequence);
		signed char unpaused=!FMUSIC_GetPaused(sequence);
		return (playing && unpaused && FMUSIC_SetPaused(sequence,1))?true:false;
	}
	return false;
}
bool cSequence::Unpause()
{
	if (sequence)
	{
		signed char playing=FMUSIC_IsPlaying(sequence);
		signed char paused= FMUSIC_GetPaused(sequence);
		return (playing && paused && FMUSIC_SetPaused(sequence,0))?true:false;
	}
	return false;
}
bool cSequence::isPlaying()
{
	return sequence && FMUSIC_IsPlaying(sequence);
}
bool cSequence::isPaused()
{
	return sequence && FMUSIC_IsPlaying(sequence) && FMUSIC_GetPaused(sequence);
}

void cSequence::setVolume(int volume)
{
	if(sequence)
	{
		//max_volume in sequences ranges to 256
		int adjustedVolume = (volume<0)?0:(volume>MAX_VOLUME+1)?MAX_VOLUME+1:volume;
		FMUSIC_SetMasterVolume(sequence,adjustedVolume);
	}
}
void cSequence::speedUpSound(float amount)
{
	if(sequence && amount>0.0f && amount<10.0f)
	{
		FMUSIC_SetMasterSpeed(sequence,amount);
	}
}
void cSequence::slowDownSound(float amount)
{
	if (sequence && amount>1.0f)
	{
		FMUSIC_SetMasterSpeed(sequence,(1/(float)amount));
	}
}
void cSequence::restoreSoundSpeed()
{
	if(sequence)
		FMUSIC_SetMasterSpeed(sequence,1.0f);
}
