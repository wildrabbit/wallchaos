#ifndef COMMON_TYPESH_H
#define COMMON_TYPESH_H

#include <string>
#include <map>
#include <vector>
#include <windows.h>

//Game states
typedef enum
{
	MIN_ID=-1,
	STATE_NULL=MIN_ID,
	INTRO,MENU,
	GAME_RUNNING,GAME_PAUSED,GAME_ENDED,
	LEVEL_CLEARED,LEVEL_STARTED,
	EXIT,
	MAX_ID=EXIT
} TGameStateID;

//Directions
typedef enum
{
	ORIENTATION_SOUTH=0,
	ORIENTATION_SOUTHWEST,
	ORIENTATION_WEST,
	ORIENTATION_NORTHWEST,
	ORIENTATION_NORTH,
	ORIENTATION_NORTHEAST,
	ORIENTATION_EAST,
	ORIENTATION_SOUTHEAST
} TOrientation;

//Struct containing relevant information to pass to the graphics layer,
//in charge for rendering a scene
class cScene;
class cPlayer;
class cEnemy;
typedef std::vector<cEnemy*> TEnemyList;

class cItem;
typedef std::vector<cItem*> TItemList;

//Animation types
typedef struct
{
	int status;	
	int sequenceDelay;
	int sequenceNumFrames;
} TAnimData;
typedef std::map<int,TAnimData> TAnimations;

//Map element datatype
typedef enum
{
	NOTILE=-1,
	FLOOR_00,
	FLOOR_01,
	FLOOR_02,
	FLOOR_03,
	TOXIC_FLOOR,
	WALL_00,
	WALL_01,
	WALL_02,
	WALL_03
} TTileIndex;
typedef struct
{
	TTileIndex index;
	bool walkable;
	int frame;
	int totalFrames;//1 =>Non-animated tile; >1=>animated tile
	int delay;
	int ticks;
	bool wall;
} TTile;

//In-game key-map related types
typedef enum {
	KEY_DIR_N=1,
	KEY_DIR_W,
	KEY_DIR_NW,
	KEY_DIR_S,
	KEY_DIR_SW=6,
	KEY_DIR_E=8,
	KEY_DIR_NE,
	KEY_DIR_SE=12
} TKeyDirs;

class TestState;
typedef void (TestState::*FPKeyDirCommand)();

typedef std::map<TKeyDirs,TOrientation>TKeyDirMapper;

typedef enum
{
	MENU_NONE=0,
	MENU_NEW_GAME,
	//...OTHERS
	MENU_EXIT
} TMenuOptions;

typedef enum
{
	GOVER_NONE,
	GOVER_MENU,
	GOVER_RESTART,
	GOVER_QUIT
} TGOverOptions;

//-------------------------
//Rendering data datatypes
//-------------------------
typedef struct
{
	int mouseX,mouseY;
	int clickedX,clickedY;
	RECT* mouseOverRect;
	//These two select the portion of the texture to blit
	RECT* mouseOverClippingRect;
	RECT* mouseCursorClippingRect;
} TMouseRenderingData;

typedef struct
{
	cScene* scene;
	cPlayer* player;
	TEnemyList* enemies;
	TItemList* items;
	int score;
	int remainingTime;
	bool paused;
	bool showMinimap;
}TGameRunningRenderingData;

typedef struct
{
	TMouseRenderingData mouseData;
	int score;
	bool gameCleared;
}TGameEndedRenderingData;

typedef struct
{
	int levelBaseScore;
	int timeBonus;
	int timeScore;
	int levelTotalScore;
	int accumulatedScore;
	int totalScore;
} TLevelClearedRenderingData;

typedef union
{
	TMouseRenderingData menuData;
	TGameRunningRenderingData gameData;
	TGameEndedRenderingData gameEndedData;
	TLevelClearedRenderingData levelClearedData;
} TRenderingData;

//Callback functions used by the graphics layer
class cGraphicsLayer;
typedef void (cGraphicsLayer::*FPRENDERER)(TRenderingData*);
typedef void (cGraphicsLayer::*FPDATALOADER)();
typedef void (cGraphicsLayer::*FPDATAUNLOADER)();

//Callback function types used by the sound layer
class cSoundItem;
class cSoundLayer;

typedef std::map<std::string,cSoundItem*> TSoundsMap;
typedef bool (cSoundLayer::*FPSOUNDLOADER)();
typedef void (cSoundLayer::*FPSOUNDUNLOADER)();

typedef enum
{
	SPIDER=0,
	ZOMBIE
} TEnemyType;

#endif