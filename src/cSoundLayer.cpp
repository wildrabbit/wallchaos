#include "fmod.h"
#include "cSound.h"
#include "cSoundLayer.h"

cSoundLayer::cSoundLayer()
{}
cSoundLayer::~cSoundLayer()
{}

bool cSoundLayer::Init(int outputRate, int channels,unsigned int flags)
{
	mute=false;
	return (FSOUND_Init(outputRate,channels,flags))?true:false;
}
void cSoundLayer::Finalize()
{
	if (!soundBank.empty())
	{
		for(TSoundsMap::iterator mapIt=soundBank.begin();mapIt!=soundBank.end();mapIt++)
		{
			if((mapIt->second)!=NULL)
			{
				delete mapIt->second;
				mapIt->second=NULL;
			}
		}
		soundBank.clear();
	}
	FSOUND_Close();
}
bool cSoundLayer::LoadData(FPSOUNDLOADER loader)
{
	return (this->*loader)();
}
void cSoundLayer::UnLoadData(FPSOUNDUNLOADER unloader)
{
	(this->*unloader)();
}

cSoundItem* cSoundLayer::loadFromFile(const std::string& filename, const std::string& id,bool asStream)
{
	int dotPos=filename.find_last_of(".");
	std::string extension=filename.substr(dotPos+1);
	cSoundItem* sound=NULL;
	std::string soundId=(!id.empty())?id:filename;
	std::string path(DATA_FOLDER_SOUNDS);
	path.append("\\").append(filename);
	if (extension.compare("mid")==0 || extension.compare("rmi")==0 || extension.compare("mod")==0)//use a vector of constant strings or something
		sound=new cSequence(soundId,FMUSIC_LoadSong(path.c_str()));
	else if (asStream)
			sound=new cStream(soundId,FSOUND_Stream_Open(path.c_str(),FSOUND_LOOP_NORMAL|FSOUND_STEREO|FSOUND_2D,0,0));
		else
			sound= new cSample(soundId,FSOUND_Sample_Load(FSOUND_FREE,path.c_str(),FSOUND_LOOP_OFF|FSOUND_STEREO|FSOUND_2D,0,0));
	soundBank[soundId]=sound;
	return sound;	
}	
bool cSoundLayer::unloadSound(const std::string& id)
{
	if(soundBank.find(id)!=soundBank.end())
	{
		delete soundBank[id];
		return true;
	}
	else return false;
}
cSoundItem* cSoundLayer::getSound(const std::string& id)
{
	if (soundBank.find(id)!=soundBank.end())
		return soundBank[id];
	return false;
}
bool cSoundLayer::playSound(const std::string& soundId, bool loop)
{
	cSoundItem* snd=getSound(soundId);
	if (snd)
	{
		return snd->Play(loop);
	}
	return false;
}
bool cSoundLayer::pauseSound(const std::string& soundId)
{
	cSoundItem* snd=getSound(soundId);
	if(snd)
	{
		return snd->Pause();
	}
	return false;
}
bool cSoundLayer::unpauseSound(const std::string& soundId)
{
	cSoundItem* snd=getSound(soundId);
	if(snd)
	{
		return snd->Unpause();
	}
	return false;
}
bool cSoundLayer::stopSound(const std::string& soundId)
{
	cSoundItem* snd=getSound(soundId);
	if(snd)
		return snd->Stop();
	return false;
}
void cSoundLayer::raiseSoundVolume(const std::string& soundId)
{
	cSoundItem* snd=getSound(soundId);
	if(snd)
	{
		snd->raiseVolume();
	}
}
void cSoundLayer::lowerSoundVolume(const std::string& soundId)
{
	cSoundItem* snd=getSound(soundId);
	if(snd)
	{
		snd->lowerVolume();
	}
}
bool cSoundLayer::isSoundPaused(const std::string& soundId)
{
	cSoundItem* snd=getSound(soundId);
	if(snd)
	{
		return snd->isPaused();
	}
	return false;
}
bool cSoundLayer::isSoundPlaying(const std::string &soundId)
{
	cSoundItem* snd=getSound(soundId);
	if(snd)
	{
		return snd->isPlaying();
	}
	return false;
}
bool cSoundLayer::resetSound(const std::string& soundId)
{
	cSoundItem* snd=getSound(soundId);
	if(snd)
	{
		return snd->Reset();
	}
	return false;
}
//------------------------------------------
bool cSoundLayer::loadIntroData()
{	
	if (!loadFromFile(ZELDA,ZELDA_ID)) return false;
	return true;
}
bool cSoundLayer::loadMenuData()
{
	if(!loadFromFile(MENU_MUSIC,MENU_MUSIC_ID,true)) return false;
	if(!loadFromFile(MENU_CLICK,MENU_CLICK_ID)) return false;
	return true;
}
bool cSoundLayer::loadGameData()
{
	if(!loadFromFile(FIREBALL,FIREBALL_ID)) return false;
	if (!loadFromFile(ALEXKIDD,ALEXKIDD_ID,true)) return false;
	if (!loadFromFile(WIN,WIN_ID)) return false;
	if (!loadFromFile(DEATH,DEATH_ID)) return false;
	if (!loadFromFile(HURRYUP,HURRYUP_ID)) return false;
	if (!loadFromFile(PAUSE,PAUSE_ID)) return false;
	if (!loadFromFile(ENEMY_DAMAGED,ENEMY_DAMAGED_ID)) return false;
	if (!loadFromFile(ENEMY_DEAD,ENEMY_DEAD_ID)) return false;
	if (!loadFromFile(PL_DAMAGED,PL_DAMAGED_ID)) return false;
	if (!loadFromFile(PL_DEAD,PL_DEAD_ID)) return false;
	if (!loadFromFile(ITEM_COIN_SND,ITEM_COIN_SND_ID)) return false;
	if (!loadFromFile(ITEM_HP_SND,ITEM_HP_SND_ID)) return false;
	return true;
}
bool cSoundLayer::loadGameEndedData()
{
	if(!loadFromFile(MENU_CLICK,MENU_CLICK_ID)) return false;
	if (!loadFromFile(WIN,WIN_ID)) return false;
	if (!loadFromFile(DEATH,DEATH_ID)) return false;
	return true;
}
bool cSoundLayer::loadLevelClearedData()
{
	if (!loadFromFile(LVL_CLEARED_SND,LVL_CLEARED_SND_ID)) return false;
	if (!loadFromFile(MENU_CLICK,MENU_CLICK_ID))return false;
	return true;
}
void cSoundLayer::unloadAllData()
{
	for (TSoundsMap::iterator it=soundBank.begin();it!=soundBank.end();it++)
	{
		if (it->second!=NULL){
			delete (it->second);
			it->second=NULL;
		}
	}
	soundBank.clear();
}
void cSoundLayer::setMute(bool mute)
{
	this->mute=mute;
	FSOUND_SetMute(FSOUND_ALL,this->mute?1:0);
	FMUSIC_StopAllSongs();
}
bool cSoundLayer::isMute() const
{
	return mute;
}
void cSoundLayer::speedUpSound(const std::string& soundId, float amount)
{
	cSoundItem* snd= getSound(soundId);
	if(snd)
		snd->speedUpSound(amount);
}
void cSoundLayer::slowDownSound(const std::string& soundId, float amount)
{
	cSoundItem* snd= getSound(soundId);
	if(snd)
		snd->slowDownSound(amount);
}
void cSoundLayer::restoreSoundSpeed(const std::string& soundId)
{
	cSoundItem* snd= getSound(soundId);
	if(snd)
		snd->restoreSoundSpeed();
}