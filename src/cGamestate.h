#ifndef GAMESTATEH_H
#define GAMESTATEH_H
#include <cassert>
#include "cGame.h"
/*
	Code adapted from the article at Lazyfoo.net covering
	state machines.
	URL: http://lazyfoo.net/articles/article06/index.php
*/
//The game's states will be modelled as classes
//inheriting CGameState.
class cGame;
class GameState
{
	protected:
		cGame* parent;
    public:
		GameState(cGame* parentApp);
		virtual void handleInput() = 0;
		virtual void logic(int ticks) = 0;
		virtual void render() = 0;
		virtual void init()=0;
		virtual void finalize()=0;
		virtual ~GameState(){};
		cGame* getParent();
};

class GameStateFactory
{
public:
	virtual GameState* createGameState(int stateId, cGame* app)=0;
};

#endif
