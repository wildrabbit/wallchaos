#ifndef SOUNDLAYERH_H
#define SOUNDLAYERH_H

#pragma comment(lib,"fmodvc.lib")

#include <map>


#define	DEFAULT_OUTPUT_RATE		 44100
#define	DEFAULT_SOFTWARE_CHANNELS	32

#define DATA_FOLDER_SOUNDS	"snd"

#define FIREBALL "fireball.wav"
#define FIREBALL_ID "fireball"
#define ZELDA	 "dkong.mp3"
#define ZELDA_ID	"dkong"
#define ALEXKIDD "alexkidd.mp3"
#define ALEXKIDD_ID	"alex"
#define MENU_CLICK "1up.wav"
#define MENU_CLICK_ID "1up"
#define MENU_MUSIC "Castlevania.mp3"
#define MENU_MUSIC_ID "castlevania"
//Game Over event sounds
#define WIN	"Defeat Bowser.wav"
#define WIN_ID "gameover-victory"
#define DEATH "Game Over.wav"
#define DEATH_ID "gameover-defeat"
//-------------
#define PL_DEAD "Death.wav"
#define PL_DEAD_ID "death"
#define PL_DAMAGED "pdamage.mp3"
#define PL_DAMAGED_ID "player-damaged"
#define HURRYUP "hurryup.mp3"
#define HURRYUP_ID "hurryup"
#define ENEMY_DAMAGED "edamage.mp3"
#define ENEMY_DAMAGED_ID "enemy-damaged"
#define ENEMY_DEAD "edeath.mp3"
#define ENEMY_DEAD_ID "enemy-dead"
//#define PROJECTILE_BOUNCE
//#define PROJECTILE_BOUNCE_ID "proj-bounce"
#define PAUSE "pause.wav"
#define PAUSE_ID "pause"
#define LVL_CLEARED_SND "win.wav"
#define LVL_CLEARED_SND_ID "level-cleared"

#define ITEM_COIN_SND "coin.wav"
#define ITEM_COIN_SND_ID "item-coin"
#define ITEM_HP_SND "1up.wav"
#define ITEM_HP_SND_ID "item-hp"

class cSoundItem;
class cSoundLayer;

typedef std::map<std::string,cSoundItem*> TSoundsMap;
typedef bool (cSoundLayer::*FPSOUNDLOADER)();
typedef void (cSoundLayer::*FPSOUNDUNLOADER)();

class cSoundLayer
{
public: 
	cSoundLayer();
	virtual ~cSoundLayer();

	bool Init(int outputRate=DEFAULT_OUTPUT_RATE, int channels=DEFAULT_SOFTWARE_CHANNELS,
		unsigned int flags=0);
	void Finalize();

	bool LoadData(FPSOUNDLOADER loader);
	void UnLoadData(FPSOUNDUNLOADER unloader);

	cSoundItem* loadFromFile(const std::string& filename, const std::string& id="", bool asStream=false);
	
	bool unloadSound(const std::string& id);
	cSoundItem* getSound(const std::string& id);

	bool playSound(const std::string& soundId, bool loop=false);
	bool pauseSound(const std::string& soundId);
	bool unpauseSound(const std::string& soundId);
	bool stopSound(const std::string& soundId);
	void raiseSoundVolume(const std::string& soundId);
	void lowerSoundVolume(const std::string& soundId);
	bool isSoundPaused(const std::string& soundId);
	bool isSoundPlaying(const std::string& soundId);
	bool resetSound(const std::string& soundId);
	void setMute(bool mute);
	bool isMute() const;
	void speedUpSound(const std::string& soundId, float amount=1.25f);
	void slowDownSound(const std::string& soundId, float amount=1.25f);
	void restoreSoundSpeed(const std::string& soundId);


	//
	bool loadGameData();
	bool loadIntroData();
	bool loadMenuData();
	bool loadGameEndedData();
	bool loadLevelClearedData();
	void unloadAllData();


private:
	TSoundsMap soundBank;
	bool mute;
};
#endif