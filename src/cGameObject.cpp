#include "cGameObject.h"
GameObject::GameObject(TGameObjectID id)
{
	ID=id;
}
const TGameObjectID& GameObject::getID() const
{
	return ID;
}
void GameObject::setID (const TGameObjectID& newID)
{
	ID=newID;
}

GameComponent* GameObject::getComponent(const TGameComponentID& componentID)
{	
	if (!components.empty())
	{
		return components[componentID];
	}
	return NULL;
}
GameComponent* GameObject::setComponent(GameComponent* newGameComponent)
{
	if (newGameComponent)
	{
		components[newGameComponent->getComponentID()]=newGameComponent;
	}
	return newGameComponent;
}
void GameObject::clearComponents()
{
	if (!components.empty())
	{
		for(TComponentMapIterator it=components.begin();it!=components.end();it++)
		{
			if ((it->second)!=NULL)
			{
				delete (it->second);
				it->second=NULL;
			}
		}
		components.clear();
	}
}

GameComponent::GameComponent(GameObject* gameObject):parent(gameObject){}
void GameComponent::update(){}
void GameComponent::setParent(GameObject* newParent)
{
	parent=newParent;
}
GameObject* GameComponent::getParent() const
{
	return parent;
}
//---------------
GameComponentTemplate::GameComponentTemplate(){}
void GameObjectTemplate::clear()
{
	for (TGameComponentTemplateList::iterator it=templates.begin();it!=templates.end();it++)
	{
	}
}
const std::string& GameObjectTemplate::getName() const
{
	return name;
}
void GameObjectTemplate::setName(const std::string& newName)
{
	name=newName;
}
TGameComponentTemplateList& GameObjectTemplate::getTemplates()
{
	return templates;
}
void GameObjectTemplate::addGameComponentTemplate(GameComponentTemplate* newTemplate)
{
	if (newTemplate)
		templates.push_back(newTemplate);
}
GameComponentTemplate* GameObjectTemplate::getGameComponentTemplate(const TGameComponentID& ID) const
{
	TGameComponentTemplateList::const_iterator it=templates.begin();
	while (it!=templates.end())
	{
		if((*it)->getComponentID()==ID)
			break;
	}
	return (it!=templates.end())?(*it):NULL;
}
GameObjectTemplate::GameObjectTemplate(const std::string& templateName):name(templateName){}
