#ifndef GAMEOBJECTH_H
#define GAMEOBJECTH_H
#include <string>
#include <map>
#include <list>

//Code adapted from "Game Programming Gems 6, chapter 4-6: Game Object Component System"
//The objective is to prevent deep hierarchical trees when modelling the different
//game's entities.
//For instance

typedef std::string TGameObjectID;	//Consider changing it for an int for efficiency
typedef std::string TGameComponentID;

class GameComponent;
typedef std::map<TGameComponentID,GameComponent*> TComponentMap;
typedef TComponentMap::iterator TComponentMapIterator;

//Class to model a game entity
//Players, units, objects,... will be instances of this particular class
//The differences in their data/behaviours will be determined by the
//game object's component list.
//
//Thus, a Rock would be modelled as a GameObject whose only component
//was "Renderable" (as they have no animation and no AI. Of course, this
//depends on the particular meaning of a Rock in our game)
class GameObject
{
	public:
		GameObject(TGameObjectID id);

		const TGameObjectID& getID() const;
		void setID (const TGameObjectID& newID);

		GameComponent* getComponent(const TGameComponentID& componentID);
		GameComponent* setComponent(GameComponent* newGameComponent);
		void clearComponents();
	private:
		TGameObjectID ID;
		TComponentMap components;
};
//A component will define some characteristic of
//certain game objects. They may be grouped in families
//
//As examples of components, we can have a "PhysicsComponent", 
//or "DamageComponent", among others
class GameComponent
{
public:
	TGameComponentID componentID;
	TGameComponentID componentFamilyID;
	GameComponent(GameObject* gameObject);
	virtual ~GameComponent()=0;
	
	virtual const TGameComponentID& getComponentID() const=0;
	virtual const TGameComponentID& getComponentFamilyID() const=0;

	virtual void update();
	void setParent(GameObject* newParent);
	GameObject* getParent() const;
private:
	GameObject* parent;
};

//Template to simplify the creation and initialization
//of game object components
class GameComponentTemplate
{
public:
	GameComponentTemplate();
	virtual~ GameComponentTemplate() = 0;
	virtual const TGameComponentID& getComponentID() const =0;
	virtual const TGameComponentID& getFamilyID() const=0;
	virtual GameComponent* makeComponent(GameObject* gameObject)=0;
};

typedef std::list<GameComponentTemplate*> TGameComponentTemplateList;
//Template to simplify the creation and initialization
//of game objects: it uses a list of
//game component templates.
class GameObjectTemplate
{	
public:
	~GameObjectTemplate();
	void clear();
	const std::string& getName() const;
	void setName(const std::string& newName);
	TGameComponentTemplateList& getTemplates();

	void addGameComponentTemplate(GameComponentTemplate* newTemplate);
	GameComponentTemplate* getGameComponentTemplate(const TGameComponentID& ID) const;
protected:
	GameObjectTemplate(const std::string& name);
	std::string name;
	TGameComponentTemplateList templates;
};

#endif