#ifndef ITEMSH_H
#define ITEMSH_H

#include <string>
#include <windows.h>

#define COIN_SCORE_INCREASE	50
#define HP_HEALTH_INCREASE 1

#define ITEM_INDEX 15
#define ITEM_PATH "items00.png"

#define ITEM_WIDTH 32
#define ITEM_HEIGHT 32

#define ITEM_ANCHOR_OFFSET_X	16
#define ITEM_ANCHOR_OFFSET_Y	24

#define ITEM_ANIM_TOTAL_FRAMES	4
#define ITEM_ANIM_DELAY	 100

typedef enum
{
	ITEM_COIN=0,
	ITEM_HP
} TItemType;

class cPlayer;
//typedef void (cItem::*FPITEMEVENTHANDLER)(cPlayer*);

class cItem
{
private:
	TItemType type;

	int cellX, cellY;
	int posX, posY;

	RECT collisionRect;

	//Current animation data
	int seqFrame;
	int seqTotalFrames;
	int seqDelay;
	int animTicks;
public:
	void getCellPosition(int& cX, int& cY) const;
	void setCellPosition(int cX,int cY);
	
	void getPosition(int& sX, int& sY) const;
	void setPosition(int sX, int sY);
	
	void setTicks(int ticks);

	cItem(TItemType type);

	TItemType getType() const;
	virtual ~cItem();
	void onPickup (cPlayer* reciever);
	
	RECT getCollisionRect() const;
	void setCollisionRect(RECT* rect);

	int getTextureID() const;
	RECT getTextureDimensions() const;

	void increaseHP(cPlayer* reciever);
	void increaseScore(cPlayer* reciever);

	void initAnimations();	//<- at the moment, both items will be animated in the same way
	std::string getSoundId() const;


	void update(int ticks);

	bool isDead;
};

#endif