#include "cTimer.h"
#include <windows.h>

Timer::Timer()
{
	startTicks=0;
	pausedTicks=0;
	status=STOPPED;
}

void Timer::start()
{
	status=RUNNING;
	startTicks=GetTickCount();
}

void Timer::stop()
{
	status=STOPPED;
}

void Timer::pause()
{
	if (status== RUNNING)
	{
		status=PAUSED;
		pausedTicks=GetTickCount()-startTicks;
	}
}

void Timer::unpause()
{
	if (status==PAUSED)
	{
		status=RUNNING;
		startTicks=GetTickCount()-pausedTicks;
		pausedTicks=0;
	}
}

int Timer::getTicks() const
{
	if(status==PAUSED) return pausedTicks;
	if(status==RUNNING)	return GetTickCount()-startTicks;
	return 0;
}

bool Timer::isStarted() const
{
	return status==RUNNING;
}
bool Timer::isPaused() const
{
	return status==PAUSED;
}
