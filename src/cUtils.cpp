#include "cUtils.h"
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <iomanip>

using namespace std;
//Returns a random float number
//between 0 and 1
float Utils::float_rand()
{
	return rand()/(float)RAND_MAX;
}

void Utils::int_swap(int& val1, int& val2)
{
	int tmp=val1;
	val1=val2;
	val2=tmp;
}
string Utils::getPaddedIntegerString(char fillChar, int width, int value)
{
	ostringstream ostr("");
	ostr<<right;
	ostr.fill(fillChar);
	ostr<<setw(width)<<dec<<value;
	return ostr.str();
}