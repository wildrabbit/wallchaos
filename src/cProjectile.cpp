#include "cPlayer.h"
#include "cProjectile.h"
#include "cGame.h"
#include <cmath>

void TProjectileData::initAnimations(std::string fname)
{
	animations.clear();
	animations[STATUS_SPAWNING].status=STATUS_SPAWNING;
	animations[STATUS_SPAWNING].sequenceDelay=30;
	animations[STATUS_SPAWNING].sequenceNumFrames=6;
	animations[STATUS_TRAVELLING].status=STATUS_TRAVELLING;
	animations[STATUS_TRAVELLING].sequenceDelay=25;
	animations[STATUS_TRAVELLING].sequenceNumFrames=1;
	animations[STATUS_BOUNCING].status=STATUS_BOUNCING;
	animations[STATUS_BOUNCING].sequenceDelay=25;
	animations[STATUS_BOUNCING].sequenceNumFrames=1;
	animations[STATUS_EXPLODING].status=STATUS_EXPLODING;
	animations[STATUS_EXPLODING].sequenceDelay=25;
	animations[STATUS_EXPLODING].sequenceNumFrames=1;
	animations[STATUS_DYING].status=STATUS_DYING;
	animations[STATUS_DYING].sequenceDelay=25;
	animations[STATUS_DYING].sequenceNumFrames=1;
}

cProjectile::cProjectile(std::string id,TProjectileData* projData, cPlayer* _parent):id(id),
projectileInfo(*projData),
caster(_parent)
{
}
cProjectile::~cProjectile()
{
}
const std::string& cProjectile::getId() const
{
	return id;
}

void cProjectile::getCellPosition(int& cX, int& cY) const
{
	cX=cellX;
	cY=cellY;
}
void cProjectile::setCellPosition(int cX,int cY)
{
	cellX=cX;
	cellY=cY;
}
void cProjectile::getPosition(int& sX, int& sY) const
{
	sX=posX;
	sY=posY;
}
void cProjectile::setPosition(int sX, int sY)
{
	posX=sX;
	posY=sY;
}
const TProjectileStatus& cProjectile::getStatus()const
{
	return status;
}
void cProjectile::setStatus(const TProjectileStatus& newStatus)
{
	status=newStatus;	
	//seqFrame=0;
	seqDelay=projectileInfo.animations[status].sequenceDelay;
	seqTotalFrames=projectileInfo.animations[status].sequenceNumFrames;
}
int cProjectile::getTextureID() const
{
	return PROJECTILE_INDEX;
}
RECT cProjectile::getTextureDimensions() const
{
	//[TODO] Get correct values
	RECT rc;
	rc.left=rc.top=0;
	rc.right=PROJECTILE_WIDTH;
	rc.bottom=PROJECTILE_HEIGHT;
	return rc;
}

void cProjectile::changeSpeed(int speedX, int speedY)
{
	this->speedX=speedX;
	this->speedY=speedY;
}
void cProjectile::move(cScene* scene)
{
}

void cProjectile::setTicks(int ticks)
{
	this->animationTicks=ticks;
}
void cProjectile::update(int ticks)
{
	if(!isDead)
	{
		updateAnimations(ticks);	
		if (status!=STATUS_DYING && status!=STATUS_EXPLODING)
		{
			//update cell information
			int newX,newY;
			newX=posX+speedX;
			newY=posY+speedY;	

			//Check collisions against a wall
			int cX,cY,scrX,scrY;
			cScene* scene = &(caster->getParent()->Scene);
			scene->translateCoordinates(0,0,scrX,scrY);
			scene->TileClicked(newX+scrX,newY+scrY,&cX,&cY);
			//now we'll get the offsets:
			int dX=cX-cellX,
				dY=cY-cellY;
			TOrientation collisionDir;
			if ((dX!=0 || dY!=0)&& scene->isWallCell(cX,cY))	//a collision has occurred
			{
				if (dX>0)
					collisionDir=(dY>0)?ORIENTATION_SOUTH:((dY<0)?ORIENTATION_EAST:ORIENTATION_SOUTHEAST);
				else if (dX==0) //NE-SW axis
					collisionDir=(dY>0)?ORIENTATION_SOUTHWEST:ORIENTATION_NORTHEAST; //SW
				else //dX<0
					collisionDir =(dY>0)?ORIENTATION_WEST:((dY<0)?ORIENTATION_NORTH:ORIENTATION_NORTHWEST);
				
				decreaseLifetime();
				if (lifetime==0)
				{
					setStatus(STATUS_DYING); //Don't bother to modify the speed anymore
					seqFrame=0;
				}
				else
				{
					setStatus(STATUS_BOUNCING);
					resolveBounce(collisionDir,newX,newY);
				}
			}
			else
			{
				posX=newX;
				posY=newY;
				scene->TileClicked(scrX+posX,scrY+posY,&cellX,&cellY);
			}
		}
	}
}

void cProjectile::initAnimations(std::string fname)
{
	projectileInfo.initAnimations(fname);
}
RECT cProjectile::getCollisionRect() const
{
	return collisionRect;
}

void cProjectile::setCollisionRect(const RECT& collisionRect)
{
	this->collisionRect=collisionRect;
}
int cProjectile::getCastingDelay() const
{
	return projectileInfo.castingDelay;
}
void cProjectile::setCastingDelay(int delay)
{
	projectileInfo.castingDelay=delay;
}
int cProjectile::getDamage() const
{
	return projectileInfo.damage;
}
void cProjectile::setDamage(int damage)
{
	projectileInfo.damage=damage;
}

const TOrientation& cProjectile::getOrientation()const
{
	return facing;
}
void cProjectile::setOrientation(const TOrientation& orientation)
{
	facing=orientation;
}

void cProjectile::setElevation(int elev)
{
	elevation=elev;
}
int cProjectile::getElevation() const
{
	return elevation;
}

void cProjectile::initData(int ticks)
{
	animationTicks=ticks;
	lifetime=projectileInfo.lifetime;
	damage=projectileInfo.damage;
	castingDelay=projectileInfo.castingDelay;
	collisionRect=projectileInfo.collisionRect;
	initAnimations();	
}

void cProjectile::updateAnimations(int ticks)
{
	//update animations
	if (ticks-animationTicks>seqDelay)
	{
		seqFrame++;
		if (seqFrame==seqTotalFrames)
		{
			switch(status)
			{
				case STATUS_SPAWNING:					
					setStatus(STATUS_TRAVELLING);
					break;
				case STATUS_BOUNCING:				
					setStatus(STATUS_TRAVELLING);
					//since the lifetime will have been
					//decremented at the time of
					//detecting the collision, (to determine 
					//the bouncing/dying status), it won't
					//be necessary to modify it here.
					break;
				case STATUS_EXPLODING:
					if (projectileInfo.isPiercing)
					{
						setStatus(STATUS_TRAVELLING);
						decreaseLifetime();
						if (lifetime==0)
							isDead=true;
						
					}
					else
					{
						decreaseLifetime();
						if(lifetime==0)
							isDead=true;
					}
					break;
				case STATUS_DYING:
					isDead=true;
					break;
				default:break;	//The "moving" status requires no special processing
			}
			seqFrame=0;
		}
		animationTicks=ticks;
	}
}
void cProjectile::setSeqFrame(int sqFrame)
{
	seqFrame=sqFrame;
}
void cProjectile::resolveBounce(TOrientation collisionDir, int newX, int newY)
{
	cScene* scene = &(caster->getParent()->Scene);
	bool wallNE= scene->isWallCell(cellX,cellY-1);
	bool wallNW = scene->isWallCell(cellX-1,cellY);
	bool wallSE = scene->isWallCell(cellX+1,cellY);
	bool wallSW= scene->isWallCell(cellX,cellY+1);
	switch(collisionDir)
	{
	case ORIENTATION_NORTH:
		resolveNorth(newX,newY,wallNE,wallNW);
		break;
	case ORIENTATION_SOUTH:
		resolveSouth(newX,newY,wallSE,wallSW);
		break;
	case ORIENTATION_EAST:
		resolveEast(newX,newY,wallNE,wallSE);
		break;
	case ORIENTATION_WEST:
		resolveWest(newX,newY,wallNW,wallSW);
		break;
	case ORIENTATION_NORTHEAST:
		resolveNorthEast();
		break;
	case ORIENTATION_SOUTHEAST:
		resolveSouthEast();
		break;
	case ORIENTATION_SOUTHWEST:
		resolveSouthWest();
		break;
	case ORIENTATION_NORTHWEST:
		resolveNorthWest();
		break;
	}
}
//Collision against wall resolution methods. Beware! "Here be dragons"
//(or, more accurately, loads and loads of spaghetti code) -_-U
void cProjectile::resolveNorth(int newX,int newY, bool wallNE, bool wallNW)
{
	int fineCoord = newX%TILE_WIDTH;
	bool vertex = (fineCoord==(TILE_WIDTH>>1))||(fineCoord==((TILE_WIDTH>>1)+1));
	bool fineLeft = fineCoord<(TILE_WIDTH>>1);
	bool fineRight = fineCoord>((TILE_WIDTH>>1)+1);
	if (facing==ORIENTATION_NORTH)
	{	//There may be 4 configurations: isolated north wall, a north-northeast wall, a north-northwest wall, or a corner
		if(!wallNE && !wallNW)
		{
			if (vertex)
			{
					facing=ORIENTATION_SOUTH;
					speedY=2*projectileInfo.speed+1; //Correction factor to make up for the slowness in straight directions
					speedX=0;
				}
				else if (fineLeft)
				{
					facing=ORIENTATION_SOUTHWEST;
					speedY=static_cast<int>(ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
					speedX=-2*speedY;
				}
				else if (fineRight)
				{
					facing=ORIENTATION_SOUTHEAST;
					speedY=static_cast<int>(ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
					speedX=2*speedY;
				}
			}
			else if (wallNE && wallNW)
			{
				if (vertex)
				{
					facing=ORIENTATION_SOUTH;
					speedY=2*projectileInfo.speed+1;
					speedX=0;
				}
				else if (fineLeft)
				{
					facing=ORIENTATION_EAST;
					speedY=0;
					speedX=2*projectileInfo.speed+1;
				}
				else if (fineRight)
				{
					facing=ORIENTATION_WEST;
					speedY=0;
					speedX=-2*projectileInfo.speed-1;
				}
			}
			else if (wallNE && !wallNW)
			{
					facing=ORIENTATION_WEST;
					speedY=0;
					speedX=-2*projectileInfo.speed-1;
			}
			else if (!wallNE && wallNW)
			{
				facing=ORIENTATION_EAST;
				speedY=0;
				speedX=2*projectileInfo.speed+1;
			}
		}
}
void cProjectile::resolveSouth(int newX, int newY, bool wallSE, bool wallSW)
{
	int fineCoord = newX%TILE_WIDTH;
	
	bool vertex = (fineCoord==(TILE_WIDTH>>1))||(fineCoord==((TILE_WIDTH>>1)+1));
	bool fineLeft = fineCoord<(TILE_WIDTH>>1);
	bool fineRight = fineCoord>((TILE_WIDTH>>1)+1);
	if (facing==ORIENTATION_SOUTH)
	{	//There may be 4 configurations: isolated north wall, a north-northeast wall, a north-northwest wall, or a corner
		if(!wallSE && !wallSW)
		{
			if (vertex)
			{
					facing=ORIENTATION_NORTH;
					speedY=-2*projectileInfo.speed-1;
					speedX=0;
				}
				else if (fineLeft)
				{
					facing=ORIENTATION_NORTHWEST;
					speedY=-static_cast<int>( ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
					speedX=2*speedY;
				}
				else if (fineRight)
				{
					facing=ORIENTATION_NORTHEAST;
					speedY=-static_cast<int>(ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
					speedX=-2*speedY;
				}
			}
			else if (wallSE && wallSW)
			{
				if (vertex)
				{
					facing=ORIENTATION_NORTH;
					speedY=-2*projectileInfo.speed-1;
					speedX=0;
				}
				else if (fineLeft)
				{
					facing=ORIENTATION_EAST;
					speedY=0;
					speedX=2*projectileInfo.speed+1;
				}
				else if (fineRight)
				{
					facing=ORIENTATION_WEST;
					speedY=0;
					speedX=-2*projectileInfo.speed-1;
				}
			}
			else if (wallSE && !wallSW)
			{
					facing=ORIENTATION_WEST;
					speedY=0;
					speedX=-2*projectileInfo.speed-1;
			}
			else if (!wallSE && wallSW)
			{
				facing=ORIENTATION_EAST;
				speedY=0;
				speedX=2*projectileInfo.speed+1;
			}
		}
}
void cProjectile::resolveEast(int newX,int newY, bool wallNE, bool wallSE)
{
	int fineCoord = newY%TILE_HEIGHT;
	bool vertex = (fineCoord==(TILE_HEIGHT>>1))||(fineCoord==((TILE_HEIGHT>>1)+1));
	bool fineUp = fineCoord<(TILE_HEIGHT>>1);
	bool fineDown = fineCoord>((TILE_HEIGHT>>1)+1);
	if (facing==ORIENTATION_EAST)
	{	//There may be 4 configurations: isolated north wall, a north-northeast wall, a north-northwest wall, or a corner
		if(!wallSE && !wallNE)
		{
			if (vertex)
			{
					facing=ORIENTATION_WEST;
					speedY=0;
					speedX=-2*projectileInfo.speed-1;
				}
				else if (fineUp)
				{
					facing=ORIENTATION_NORTHWEST;
					speedY=-static_cast<int>(ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
					speedX=2*speedY;
				}
				else if (fineDown)
				{
					facing=ORIENTATION_SOUTHWEST;
					speedY=static_cast<int>(ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
					speedX=-2*speedY;
				}
			}
			else if (wallNE && wallSE)
			{
				if (vertex)
				{
					facing=ORIENTATION_WEST;
					speedY=0;
					speedX=-2*projectileInfo.speed-1;
				}
				else if (fineUp)
				{
					facing=ORIENTATION_SOUTH;
					speedY=2*projectileInfo.speed+1;
					speedX=0;
				}
				else if (fineDown)
				{
					facing=ORIENTATION_NORTH;
					speedY=-2*projectileInfo.speed-1;
					speedX=0;
				}
			}
			else if (wallNE && !wallSE)
			{
					facing=ORIENTATION_SOUTH;
					speedY=2*projectileInfo.speed+1;
					speedX=0;
			}
			else if (!wallNE && wallSE)
			{
				facing=ORIENTATION_NORTH;
				speedY=-2*projectileInfo.speed-1;
				speedX=0;
			}
		}
}
void cProjectile::resolveWest(int newX,int newY, bool wallNW, bool wallSW)
{
	int fineCoord = newY%TILE_HEIGHT;
	bool vertex = (fineCoord==(TILE_HEIGHT>>1))||(fineCoord==((TILE_HEIGHT>>1)+1));
	bool fineUp = fineCoord<(TILE_HEIGHT>>1);
	bool fineDown = fineCoord>((TILE_HEIGHT>>1)+1);
	if (facing==ORIENTATION_WEST)
	{	//There may be 4 configurations: isolated north wall, a north-northeast wall, a north-northwest wall, or a corner
		if(!wallSW && !wallNW)
		{
			if (vertex)
			{
					facing=ORIENTATION_EAST;
					speedY=0;
					speedX=2*projectileInfo.speed+1;
				}
				else if (fineUp)
				{
					facing=ORIENTATION_NORTHEAST;
					speedY=-static_cast<int>(ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
					speedX=-2*speedY;//positive result!
				}
				else if (fineDown)
				{
					facing=ORIENTATION_SOUTHEAST;
					speedY=static_cast<int>(ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
					speedX=2*speedY;
				}
			}
			else if (wallNW && wallSW)
			{
				if (vertex)
				{
					facing=ORIENTATION_EAST;
					speedY=0;
					speedX=2*projectileInfo.speed+1;
				}
				else if (fineUp)
				{
					facing=ORIENTATION_SOUTH;
					speedY=2*projectileInfo.speed+1;
					speedX=0;
				}
				else if (fineDown)
				{
					facing=ORIENTATION_NORTH;
					speedY=-2*projectileInfo.speed-1;
					speedX=0;
				}
			}
			else if (wallNW && !wallSW)
			{
					facing=ORIENTATION_SOUTH;
					speedY=2*projectileInfo.speed+1;
					speedX=0;
			}
			else if (!wallNW && wallSW)
			{
				facing=ORIENTATION_NORTH;
				speedY=-2*projectileInfo.speed-1;
				speedX=0;
			}
		}
}
void cProjectile::resolveNorthEast()
{
	switch(facing)
	{
	case ORIENTATION_NORTHEAST: //SOUTHWEST
		facing=ORIENTATION_SOUTHWEST;
		speedY=static_cast<int>(ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
		speedX=-2*speedY;
		break;
	case ORIENTATION_EAST:	//SOUTH
		facing=ORIENTATION_SOUTH;
		speedX=0;
		speedY=2*projectileInfo.speed+1;
		break;
	case ORIENTATION_NORTH: //WEST
		facing=ORIENTATION_WEST;
		speedX=-2*projectileInfo.speed-1;
		speedY=0;
		break;
	default:break; //The remaining cases will not be taken into account
	}
}
void cProjectile::resolveNorthWest()
{
	switch(facing)
	{
	case ORIENTATION_NORTHWEST:
		facing=ORIENTATION_SOUTHEAST;
		speedY=static_cast<int>(ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
		speedX=2*speedY;
		break;
	case ORIENTATION_NORTH:
		facing=ORIENTATION_EAST;
		speedX=2*projectileInfo.speed+1;
		speedY=0;
		break;
	case ORIENTATION_WEST:
		facing=ORIENTATION_SOUTH;
		speedX=0;
		speedY=2*projectileInfo.speed+1;
		break;
	default:break;
	}
}
void cProjectile::resolveSouthEast()
{
	switch(facing)
	{
	case ORIENTATION_SOUTHEAST:
		facing=ORIENTATION_NORTHWEST;
		speedY=-static_cast<int>(ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
		speedX=2*speedY;
		
		break;
	case ORIENTATION_SOUTH:
		facing=ORIENTATION_WEST;
		speedX=-2*projectileInfo.speed-1;
		speedY=0;
		break;
	case ORIENTATION_EAST:
		facing=ORIENTATION_NORTH;
		speedX=0;
		speedY=-2*projectileInfo.speed-1;
		break;
	default:break;
	}
}
void cProjectile::resolveSouthWest()
{
	switch(facing)
	{
	case ORIENTATION_SOUTHWEST:
		facing=ORIENTATION_NORTHEAST;
		speedY=-static_cast<int>(ceil((projectileInfo.speed*projectileInfo.speed/FIVE_SQRT)));
		speedX=-2*speedY;//result will be positive, as intended
		break;
	case ORIENTATION_SOUTH:
		facing=ORIENTATION_EAST;
		speedX=2*projectileInfo.speed+1;
		speedY=0;
		break;
	case ORIENTATION_WEST:
		facing=ORIENTATION_NORTH;
		speedX=0;
		speedY=-2*projectileInfo.speed-1;
		break;
	default:break;
	}
}
void cProjectile::decreaseLifetime()
{
	if(lifetime>0)lifetime--;
}
int cProjectile::getLifetime() const
{
	return lifetime;
}
TProjectileData cProjectile::getProjectileData() const
{
	return projectileInfo;
}