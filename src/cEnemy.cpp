#include "cEnemy.h"
#include "cGame.h"
#include "cScene.h"
#include "cUtils.h"


cEnemy::cEnemy(std::string enemyId, cGame* _parent):id(enemyId),parent(_parent){}
cEnemy::~cEnemy(){
}



void cEnemy::getCellPosition(int& cX, int& cY) const
{
	cX=cellX;
	cY=cellY;
}
void cEnemy::setCellPosition(int cX,int cY)
{
	cellX=cX;
	cellY=cY;
}
void cEnemy::getPosition(int& sX, int& sY) const
{
	sX=posX;
	sY=posY;
}
void cEnemy::setPosition(int sX, int sY)
{
	posX=sX;
	posY=sY;
}
void cEnemy::getInitialCellPosition(int& cX, int& cY) const
{
	cX=initialCellX;
	cY=initialCellY;
}
void cEnemy::setInitialCellPosition(int cX,int cY)
{
	initialCellX=cX;
	initialCellY=cY;
}
const TPlayerStatus& cEnemy::getStatus()const
{
	return status;
}
void cEnemy::setStatus(const TPlayerStatus& newStatus)
{
	status=newStatus;
	//seqFrame=0;
	seqDelay=animations[status].sequenceDelay;
	seqTotalFrames=animations[status].sequenceNumFrames;
}
int cEnemy::getTextureID() const
{
	return ENEMY_INDEX;
}
RECT cEnemy::getTextureDimensions() const
{
	RECT rc;
	int yOffset;
	switch(status)
	{
		case STATUS_STILL:
			SetRect(&rc,ENEMY_WIDTH*facing,0,ENEMY_WIDTH*(facing+1),ENEMY_HEIGHT);
			break;
		case STATUS_MOVING:
			yOffset=ENEMY_HEIGHT;
			break;
		case STATUS_DAMAGED: 
			yOffset=5*ENEMY_HEIGHT;
			break;
		case STATUS_DEAD:
			SetRect(&rc,ENEMY_WIDTH*seqFrame,ENEMY_HEIGHT*9,ENEMY_WIDTH*(seqFrame+1),ENEMY_HEIGHT*10);
			break;
		default: break;

	}
	int texSelect;
	switch(facing)
	{
		case ORIENTATION_SOUTH:
			texSelect=7;
			break;
		case ORIENTATION_SOUTHWEST: 
			texSelect=5;
			break;
		case ORIENTATION_WEST:
			texSelect=3;
			break;
		case ORIENTATION_SOUTHEAST: 
			texSelect=6;
			break;
		case ORIENTATION_EAST:
			texSelect=4;
			break;
		case ORIENTATION_NORTHWEST:
			texSelect=1;
			break;
		case ORIENTATION_NORTHEAST:
			texSelect=2;
			break;
		case ORIENTATION_NORTH:
			texSelect=0;
			break;
	}
	if (status!=STATUS_STILL && status!=STATUS_DEAD)
	{
		rc.left=(((texSelect%2)*seqTotalFrames)+seqFrame)*ENEMY_WIDTH;
		rc.right=rc.left+ENEMY_WIDTH;
		rc.top=yOffset+ENEMY_HEIGHT*(texSelect>>1);
		rc.bottom=rc.top+ENEMY_HEIGHT;
	}
	return rc;
}
void cEnemy::update(int ticks)
{
	if(!isDead)
	{
		if (status==STATUS_MOVING)		
		{
			//Determine at random if the enemy will take a detour
			bool canSwitchtoNEAxis=(speedX!=0 && (parent->Scene.isFreeCell(cellX,cellY+1) || parent->Scene.isFreeCell(cellX,cellY-1)));
			bool canSwitchtoNWAxis=(speedY!=0 && (parent->Scene.isFreeCell(cellX+1,cellY) || parent->Scene.isFreeCell(cellX-1,cellY)));
 			if (Utils::float_rand()<DETOUR_CHANCE && (canSwitchtoNEAxis||canSwitchtoNWAxis))
			{
				speedX=-speedX;
				speedY=speedY;
				correctFacing();
			}
			else
			{			
				int newX,newY;
				newX=posX+speedX;
				newY=posY+speedY;
				RECT rc=getCollisionRect();
				//If the enemy collides with a wall, reverse direction. Otherwise, perform movement.
				int cX,cY,scrX,scrY;
				parent->Scene.translateCoordinates(0,0,scrX,scrY);
				parent->Scene.TileClicked(newX-rc.left+scrX,newY-rc.top+scrY,&cX,&cY);
				bool validUpperLeft = parent->Scene.isFreeCell(cX,cY);
				parent->Scene.TileClicked(newX-rc.left+scrX,newY+(rc.bottom-rc.top)+scrY,&cX,&cY);
				bool validLowerLeft = parent->Scene.isFreeCell(cX,cY);
				parent->Scene.TileClicked(newX+(rc.right-rc.left)+scrX,newY-rc.top+scrY,&cX,&cY);
				bool validUpperRight = parent->Scene.isFreeCell(cX,cY);
				parent->Scene.TileClicked(newX+(rc.right-rc.left)+scrX,newY+(rc.bottom-rc.top)+scrY,&cX,&cY);
				bool validLowerRight = parent->Scene.isFreeCell(cX,cY);
				bool canMove = validUpperLeft && validUpperRight && validLowerLeft && validLowerRight;
				if(canMove)	
					move(&(parent->Scene));
				else
				{
					speedX=-speedX;
					speedY=-speedY;
					correctFacing();
				}
			}
			//}
		}
		if((ticks-this->ticks)>=seqDelay)
		{
			seqFrame++;
			if (seqFrame==seqTotalFrames)
			{
				if (status==STATUS_DEAD)
				{
					parent->addLevelScore(score);
					isDead=true;
				}
				else if (status==STATUS_DAMAGED)
					setStatus(STATUS_MOVING);
					seqFrame=0;
			}
			this->ticks=ticks;
		}
		//Trajectory update
	}
}
const TOrientation& cEnemy::getOrientation()const
{
	return facing;
}
void cEnemy::setOrientation(const TOrientation& orientation)
{
	facing=orientation;
}
void cEnemy::changeSpeed(int speedX, int speedY)
{
	this->speedX=speedX;
	this->speedY=speedY;
}
void cEnemy::move(cScene* scene)
{
	posX+=speedX;
	posY+=speedY;
	//Ensure the cell coordinates are kept updated as well.
	int scrX,scrY;
	scene->translateCoordinates(0,0,scrX,scrY);
	scene->TileClicked(posX+scrX,posY+scrY,&cellX,&cellY);	
}
void cEnemy::setTicks(int ticks)
{
	this->ticks=ticks;
}
void cEnemy::initAnimations(std::string fname)
{
	animations.clear();
	animations[STATUS_STILL].status=STATUS_STILL;
	animations[STATUS_STILL].sequenceDelay=25;
	animations[STATUS_STILL].sequenceNumFrames=1;
	animations[STATUS_MOVING].status=STATUS_MOVING;
	animations[STATUS_MOVING].sequenceDelay=25;
	animations[STATUS_MOVING].sequenceNumFrames=4;
	animations[STATUS_DAMAGED].status=STATUS_DAMAGED;
	animations[STATUS_DAMAGED].sequenceDelay=25;
	animations[STATUS_DAMAGED].sequenceNumFrames=4;
	animations[STATUS_DEAD].status=STATUS_DEAD;
	animations[STATUS_DEAD].sequenceDelay=25;
	animations[STATUS_DEAD].sequenceNumFrames=4;
}
RECT cEnemy::getCollisionRect() const
{
	return collisionRect;
}
void cEnemy::setCollisionRect(const RECT& rect)
{
	collisionRect=rect;
}
cGame* cEnemy::getParent()
{
	return parent;
}
void cEnemy::correctFacing()
{
	if (speedX>0){
		if(speedY>0)
			facing=ORIENTATION_SOUTHEAST;
		else if (speedY<0)
			facing=ORIENTATION_NORTHEAST;
	}
	else if (speedX<0){
		if(speedY>0)
		facing=ORIENTATION_SOUTHWEST;
		else if (speedY<0)
		facing=ORIENTATION_NORTHWEST;
	}
}
bool cEnemy::hitTest(cEnemy* enemy)
{
	RECT enemyRect = enemy->getCollisionRect();
	int enemyX,enemyY;
	enemy->getPosition(enemyX,enemyY);
	return !((posX-collisionRect.left>=enemyX+((enemyRect.right-enemyRect.left)>>1)) || 
		(posX+((collisionRect.right-collisionRect.left)>>1)<=enemyX-enemyRect.left) ||
		(posY-collisionRect.top>=enemyY+((enemyRect.bottom-enemyRect.top)>>1)) || 
		(posY+((collisionRect.bottom-collisionRect.top)>>1)<=enemyY-enemyRect.top));
}
bool cEnemy::hitTest(cProjectile* projectile)
{
	RECT projRect = projectile->getCollisionRect();
	int projX,projY;
	projectile->getPosition(projX,projY);
	return !((posX-collisionRect.left>=projX+((projRect.right-projRect.left)>>1)) || 
		(posX+((collisionRect.right-collisionRect.left)>>1)<=projX-projRect.left) ||
		(posY-collisionRect.top>=projY+((projRect.bottom-projRect.top)>>1)) || 
		(posY+((collisionRect.bottom-collisionRect.top)>>1)<=projY-projRect.top));

}
int cEnemy::getHP() const
{
	return hp;
}
void cEnemy::decreaseHP(int amount)
{
	hp-=amount;
}
void cEnemy::increaseHP(int amount)
{
	hp+=amount;
}
void cEnemy::setHP(int hp)
{
	this->hp=hp;
}
void cEnemy::resetSeqFrame()
{
	seqFrame=0;
}
int cEnemy::getScore() const
{
	return score;
}
void cEnemy::setScore(int newScore)
{
	score=newScore;
}