#ifndef __GRAPHICSLAYER_H__
#define __GRAPHICSLAYER_H__

#pragma comment(lib,"dxguid.lib")
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")
#pragma comment(lib,"dxerr.lib")
#pragma comment(lib,"winmm.lib")

#include <D3D9.h>
#include <D3DX9.h>
#include <map>
#include <string>
#include "cCommonTypes.h"

#define DATA_FOLDER_IMAGES	"img"

#define SELECTION_PATH	"Border.tga"
#define SELECTION_INDEX	4

#define INTRO_PATH "intro01.jpg"
#define INTRO_INDEX 0

#define SHADOWS_PATH "shadows00.tga"
#define SHADOWS_INDEX 8

#define GUI_PATH	"gui00.png"
#define GUI_INDEX 9
#define GUI_HEIGHT 88

#define GOVER_PATH "gover00.jpg"
#define GOVER_INDEX 10
#define GCLEARED_PATH "gcleared00.jpg"
#define GCLEARED_INDEX 11

#define LVLCLEARED_BG_PATH "lvlcleared00.jpg"
#define LVLCLEARED_BG_INDEX 12

#define MENU_CURSORS_PATH "menu-cursors.tga"
#define MENU_CURSORS_INDEX 0
#define MENU_BG_PATH "menu01.jpg"
#define MENU_BG_INDEX 1

#define MENU_MOUSEOVER_WIDTH 186
#define MENU_MOUSEOVER_HEIGHT 88

#define MENU_CURSOR_HEIGHT 32
#define MENU_CURSOR_WIDTH  32

#define MENU_BUTTON_WIDTH	180
#define	MENU_BUTTON_HEIGHT	84


typedef std::map<int,LPDIRECT3DTEXTURE9> TTextureMap;
typedef TTextureMap::iterator TTextureMapIterator;
typedef std::map<int,LPD3DXFONT> TFontMap;

class cTestStateRenderer;
class cScene;
class cPlayer;
class cEnemy;
class cProjectile;
class cGraphicsLayer  
{
private:
	void drawNumericString(const std::string& digits,int startX,int startY);

public:
	
	cGraphicsLayer();
	virtual ~cGraphicsLayer();

	bool Init(HWND hWnd); 
	void Finalize();
	
	void Render(int mouseX,int mouseY,cScene *Scene);

	void Render(TRenderingData*,FPRENDERER renderer);
	void LoadData(FPDATALOADER dataLoader);
	void UnLoadData(FPDATAUNLOADER dataUnloader);

	void loadIntroData();
	void loadGameData();
	void loadMenuData();
	void loadGameEndedData();
	void loadLevelClearedData();

	void unloadAllData();

	void renderIntroScene(TRenderingData* renderingData);
	void renderMenuScene(TRenderingData* renderingData);
	void renderGameScene(TRenderingData* renderingData);
	void renderGameEndedScene(TRenderingData* renderingData);
	void renderLevelClearedScene(TRenderingData* renderingData);

	LPDIRECT3D9 g_pD3D;
	LPDIRECT3DDEVICE9 g_pD3DDevice;
	LPD3DXSPRITE g_pSprite;

	TTextureMap textures;
	TFontMap fonts;

	void drawPlayer(cPlayer* player, int cX, int cY, int scrX, int scrY,D3DCOLOR inkColor);
	void drawItems(TItemList* items, int cX, int cY, int scrX, int scrY, D3DCOLOR inkColor);
	void drawEnemies(TEnemyList* enemies, int cX, int cY, int scrX, int scrY,D3DCOLOR inkColor);
	void drawProjectile(cProjectile* player, int cX, int cY, int scrX, int scrY,D3DCOLOR inkColor);
	void drawInGameGUI(TRenderingData* renderingData,D3DCOLOR inkColor);
	void drawMinimap(TRenderingData* renderingData,D3DCOLOR inkColor);

};


#endif