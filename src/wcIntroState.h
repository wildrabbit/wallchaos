#ifndef INTROSTATEH_H
#define INTROSTATEH_H

#include "wcGameState.h"

//#define INTRO_DELAY 3000

class IntroState:public GameState
{
	private:
		//int ticks;
		bool pressed;
		bool quit;

	public:
		IntroState(cGame* parent);
		void handleInput();
		void logic(int ticks);
		void render();
		void init();
		void finalize();
		~IntroState(){};

};

#endif