#ifndef MENUSTATEH_H
#define MENUSTATEH_H

#include <map>
#include "cCommonTypes.h"
#include "wcGameState.h"

typedef std::map<TMenuOptions,TGameStateID>	TMenuStatesMap;
typedef std::map<TMenuOptions,RECT>	TMenuOptionRectsMap;

class MenuState:public GameState
{
	private:
		int ticks;
		TMenuStatesMap menuStates;
		TMenuOptionRectsMap optionRects;
		TMenuOptions mouseOverOption;
		RECT mouseCursorClippingRect;//Selects the rect for the cursor from the texture
		RECT mouseOverClippingRect;//Selects the rect for the button border drawn on mouse over
		int	clickedX,clickedY;

	public:
		MenuState(cGame* parent);
		void handleInput();
		void logic(int ticks);
		void render();
		void init();
		void finalize();
		~MenuState(){};

};

#endif