#include "cGame.h"
#include "wcGameRunningState.h"
#include "cMouse.h"
#include "cKeyboard.h"
#include "cSound.h"
#include "cLog.h"

GameRunningState::GameRunningState(cGame *parent):
	GameState(parent)
{
}
void GameRunningState::init()
{	
	cLog* log = cLog::Instance();
	keyCommands.clear();
	keyCommands[KEY_DIR_E]=ORIENTATION_EAST;
	keyCommands[KEY_DIR_SE]=ORIENTATION_SOUTHEAST;
	keyCommands[KEY_DIR_S]=ORIENTATION_SOUTH;
	keyCommands[KEY_DIR_SW]=ORIENTATION_SOUTHWEST;
	keyCommands[KEY_DIR_W]=ORIENTATION_WEST;
	keyCommands[KEY_DIR_NW]=ORIENTATION_NORTHWEST;
	keyCommands[KEY_DIR_N]=ORIENTATION_NORTH;
	keyCommands[KEY_DIR_NE]=ORIENTATION_NORTHEAST;

	changeState=false;
	keyBuffer=parent->getTimer()->getTicks();

	if (!parent->paused)
	{
		std::string levelToLoad;
		if(parent->firstLoad)
		{
			parent->nextLevelPath.clear();
			levelToLoad= INITIAL_LEVEL_PATH;
		}
		else levelToLoad=parent->nextLevelPath;

		if (!parent->loadLevel(levelToLoad))
		{
			log->Msg("Error loading level");
			return;
		}		
		parent->firstLoad=false;
	}
	parent->paused=false;
	parent->levelLastTick=parent->getTimer()->getTicks();

	parent->Graphics.LoadData(&cGraphicsLayer::loadGameData);
	parent->Sound.LoadData(&cSoundLayer::loadGameData);
	parent->Sound.playSound(ALEXKIDD_ID,true);
	if ((parent->levelCompletionTime-parent->levelEllapsedTime)/1000<=HURRYUP_DELAY)
	{
		parent->Sound.speedUpSound(ALEXKIDD_ID,HURRYUP_FREQ_INC);
	}
	else
		parent->Sound.restoreSoundSpeed(ALEXKIDD_ID);
}
void GameRunningState::finalize()
{
	if (parent)
	{
		parent->Graphics.UnLoadData(&cGraphicsLayer::unloadAllData);
		parent->Sound.UnLoadData(&cSoundLayer::unloadAllData);
		if(parent->levelCleared)
		{
			for (TEnemyList::iterator it=parent->enemies.begin();it!=parent->enemies.end();it++)
			{	
				if (*it)
				{
					delete (*it);
					(*it)=NULL;
				}
			}
			for (TItemList::iterator it=parent->items.begin();it!=parent->items.end();it++)
			{	
				if (*it)
				{
					delete (*it);
					(*it)=NULL;
				}
			}
			parent->items.clear();
			if (parent->Player)
			{
				parent->Player->clearData();
			}

		}

		parent=NULL;
	}
}
//------------------------
void GameRunningState::handleInput()
{
	if (parent)
	{
		cScene* scene = &parent->Scene; 
		cKeyboard* keyboard = parent->Input.GetKeyboard();
		queryForScrolling(keyboard);
		if(keyboard)
		{
			if (parent->Player && parent->Player->getStatus()==STATUS_MOVING && (keyboard->KeyUp(DIK_S)||keyboard->KeyUp(DIK_W)||keyboard->KeyUp(DIK_A)||keyboard->KeyUp(DIK_D)))
				parent->Player->setStatus(STATUS_STILL);
			//Sound controlling keys
			if (keyboard->KeyDown(DIK_U) && (parent->getTimer()->getTicks()-keyBuffer)>KEYBUFFER_TIME)
			{
				keyBuffer=parent->getTimer()->getTicks();
				if (parent->Sound.isSoundPaused(ALEXKIDD_ID))
					parent->Sound.unpauseSound(ALEXKIDD_ID);
				else parent->Sound.pauseSound(ALEXKIDD_ID);
			}
			else if (keyboard->KeyDown(DIK_I)&& (parent->getTimer()->getTicks()-keyBuffer)>KEYBUFFER_TIME)
			{
				keyBuffer=parent->getTimer()->getTicks();
				if (parent->Sound.isSoundPlaying(ALEXKIDD_ID))
					parent->Sound.stopSound(ALEXKIDD_ID);
				else parent->Sound.playSound(ALEXKIDD_ID,true);
			}
			else if (keyboard->KeyDown(DIK_O)&& (parent->getTimer()->getTicks()-keyBuffer)>KEYBUFFER_TIME)
			{
				keyBuffer=parent->getTimer()->getTicks();
				if (parent->Sound.isMute())
					parent->Sound.setMute(false);
				else parent->Sound.setMute(true);
			}
			else if (keyboard->KeyDown(DIK_P)&& (parent->getTimer()->getTicks()-keyBuffer)>KEYBUFFER_TIME)
			{
				keyBuffer=parent->getTimer()->getTicks();
				parent->Sound.playSound(PAUSE_ID);
				changeState=true;
			}
			else if (keyboard->KeyDown(DIK_M)&& (parent->getTimer()->getTicks()-keyBuffer)>KEYBUFFER_TIME)
			{
				keyBuffer=parent->getTimer()->getTicks();
				parent->showMinimap=!parent->showMinimap;
			}
			//Player movement handling
			else if (parent->Player && parent->Player->getStatus()!=STATUS_DEAD &&!parent->gameOver)
				{
					int keyMask=0;
					if (keyboard->KeyDown(DIK_A))
						keyMask|= DIK_A_MASK;
					if(keyboard->KeyDown(DIK_W))
						keyMask|=DIK_W_MASK;
					if(keyboard->KeyDown(DIK_S))
						keyMask|=DIK_S_MASK;
					if(keyboard->KeyDown(DIK_D))
						keyMask|=DIK_D_MASK;
					if(keyMask)
						dirKeyPressed(keyMask);

					if (keyboard->KeyDown(DIK_J))
					{
						if (parent->Player->shoot(parent->getTimer()->getTicks()))
						{
							parent->Sound.playSound(FIREBALL_ID);
							parent->Player->resetSeqFrame();
							parent->Player->setStatus(STATUS_SHOOTING);
						}
					}
					//[[TODO]] Spell selection keys, and others
				}
			}
		}
	}
void GameRunningState::dirKeyPressed(int mask)
{
	TOrientation direction=keyCommands[(TKeyDirs)mask];
	cPlayer* player=parent->Player;
	if (player->getStatus()!=STATUS_DEAD)
	{	
		player->setStatus(STATUS_MOVING);
		if(player->getOrientation()==direction)
		{
			player->move(&(parent->Scene));
		}
		else {
			player->setOrientation(direction);
			int speedX=0,speedY=0;
			if(mask&DIK_W_MASK)
				speedY=-PLAYER_SPEED_Y;
			else if (mask & DIK_S_MASK)
				speedY=PLAYER_SPEED_Y;
			if(mask&DIK_A_MASK)
				speedX=-PLAYER_SPEED_X;
			else if (mask&DIK_D_MASK)
				speedX=+PLAYER_SPEED_X;
			player->changeSpeed(speedX,speedY);
		}					
	}
}
void GameRunningState::logic(int ticks){
	if (changeState)
	{ 
		if(!parent->Sound.isSoundPlaying(PAUSE_ID))
			parent->setNextStateID(GAME_PAUSED);
	}
	else
	{
		parent->doLogic(ticks);
		if (parent->levelCleared)
			parent->setNextStateID(LEVEL_CLEARED);
		else if(parent->gameOver)
			parent->setNextStateID(GAME_ENDED);
	}
}
void GameRunningState::render()
{
	if (parent)
	{	
		TGameRunningRenderingData gameData;
		gameData.scene=&(parent->Scene);
		gameData.player=parent->Player;
		gameData.enemies=&parent->enemies;
		gameData.score=parent->getTotalScore()+parent->getLevelScore();
		gameData.remainingTime=(parent->levelCompletionTime-parent->levelEllapsedTime)/1000;
		gameData.paused=parent->paused;
		gameData.showMinimap=parent->showMinimap;
		gameData.items=&parent->items;

		TRenderingData renderingData;
		renderingData.gameData=gameData;

		parent->Graphics.Render(&renderingData,&cGraphicsLayer::renderGameScene);
	}
}
void GameRunningState::queryForScrolling(cKeyboard* keyboard)
{
	if(keyboard)
	{
		cScene* scene = &(parent->Scene);
		int oldBaseX,oldBaseY;
		scene->getBasePosition(oldBaseX,oldBaseY);
		if (keyboard->KeyDown(DIK_LEFTARROW))
		{
			if(oldBaseX>(1-SCENE_WIDTH)*(TILE_WIDTH>>1)-OFF_LIMITS_INC_X)	
					scene->setBasePosition(oldBaseX-SCROLL_AMOUNT_X,oldBaseY);
		}
		else if (keyboard->KeyDown(DIK_RIGHTARROW))
		{
			if(oldBaseX<(((1-SCENE_WIDTH)*TILE_WIDTH >>1)+(TILE_WIDTH*SCENE_WIDTH))-SCREEN_RES_X+OFF_LIMITS_INC_X)				
				scene->setBasePosition(oldBaseX+SCROLL_AMOUNT_X,oldBaseY);
		}
		else if (keyboard->KeyDown(DIK_UPARROW))
			{
				if (oldBaseY>-OFF_LIMITS_INC_Y)
					scene->setBasePosition(oldBaseX,oldBaseY-SCROLL_AMOUNT_Y);
			}
			else if (keyboard->KeyDown(DIK_DOWNARROW))
			{
				if((oldBaseY<TILE_HEIGHT*SCENE_HEIGHT-SCREEN_RES_Y_GAMEVP+OFF_LIMITS_INC_Y))
					scene->setBasePosition(oldBaseX,oldBaseY+SCROLL_AMOUNT_Y);											
			}
	}
}
