#ifndef GAMEENDEDSTATEH_H
#define GAMEENDEDSTATEH_H

#include "cCommonTypes.h"
#include "wcGameState.h"
#include <map>

typedef std::map<TGOverOptions,TGameStateID>	TGOverStatesMap;
typedef std::map<TGOverOptions,RECT>	TGOverOptionRectsMap;

class GameEndedState:public GameState
{
	private:
		int ticks;
		TGOverStatesMap menuStates;
		TGOverOptionRectsMap optionRects;
		TGOverOptions mouseOverOption;
		RECT mouseCursorClippingRect;//Selects the rect for the cursor from the texture
		RECT mouseOverClippingRect;//Selects the rect for the button border drawn on mouse over
		int	clickedX,clickedY;

	public:
		GameEndedState(cGame* parent);
		void handleInput();
		void logic(int ticks);
		void render();
		void init();
		void finalize();
		~GameEndedState(){};
};

#endif