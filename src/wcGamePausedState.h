#ifndef GAMEPAUSEDH_H
#define GAMEPAUSEDH_H

#include "wcGameState.h"

class GamePausedState:public GameState
{
	private:
		bool changeState;
		bool quit;
	public:
		GamePausedState(cGame* parent);
		void handleInput();
		void logic(int ticks);
		void render();
		void init();
		void finalize();
		~GamePausedState(){};

};

#endif