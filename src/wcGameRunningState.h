#ifndef GAMERUNNINGSTATEH_H
#define GAMERUNNINGSTATEH_H

#include "wcGameState.h"

#define DIK_W_MASK	1
#define DIK_A_MASK	2
#define	DIK_S_MASK	4
#define DIK_D_MASK	8

class cGame;
class cKeyboard;
class cPlayer;
class GameRunningState:public GameState
{
	private:
		int clickedX,clickedY;
		TKeyDirMapper keyCommands;

		void dirKeyPressed(int mask);
		void scrollKeyPressed();

		bool changeState;

		int keyBuffer;

	public:
		GameRunningState(cGame* parent);
		void handleInput();
		void logic(int ticks);
		void render();
		void init();
		void finalize();
		~GameRunningState(){};
		
		void queryForScrolling(cKeyboard* keyboard);
};

#endif