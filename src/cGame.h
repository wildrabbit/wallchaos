#ifndef __GAME_H__
#define __GAME_H__

#include "cCommonTypes.h"
#include "cGraphicsLayer.h"
#include "cInputLayer.h"
#include "cSoundLayer.h"
#include "cScene.h"
#include "cPlayer.h"
#include "cEnemy.h"
#include "cItem.h"
#include "cTimer.h"

#define DATA_FOLDER_LEVELS	"lvl"

#define FPS	50

#define INITIAL_LEVEL_PATH "level01.wcl"

#define LEVELFILE_COMMENT	'#'
#define LEVELFILE_CATEGORY_LIST_SEPARATOR '$'
#define LEVELFILE_CATEGORY_SEPARATOR "$$"
#define LEVELFILE_LAST_LEVEL	"%%"

#define HURRYUP_DELAY 30
#define HURRYUP_FREQ_INC 1.2f

#define KEYBUFFER_TIME 500

#define BONUS_SCORE_PER_SECOND	50

typedef enum
{
	
	LEVEL_INITIAL=0,
	LEVEL_NAME=LEVEL_INITIAL,
	LEVEL_NEXTLEVELPATH,
	//LEVEL_MAPINFO, [[TODO]] Uncomment when the scene map is re-implemented
	LEVEL_MAPLAYERS,
	LEVEL_PLAYERINI,
	LEVEL_ENEMIES,
	LEVEL_ITEMS,
	LEVEL_CLEARTIME,

} TLevelReadStage;


//#include "cGameState.h"
class GameState;
class Timer;

class cGame
{
public:
	cGame();
	virtual ~cGame();

	bool Init(HWND hWnd,HINSTANCE hInst,bool exclusive);
	bool Loop(); 
	void Finalize();

	cGraphicsLayer Graphics;
	cInputLayer Input;
	cScene Scene;
	cSoundLayer Sound;

	//GameObject* Player;
	cPlayer* Player;
	TEnemyList enemies;
	TItemList items;
	
	
	//State management
	void changeState();
    TGameStateID getNextStateID() const;
    void setNextStateID(const TGameStateID& nextStateId);
	Timer* getTimer();

	int levelCompletionTime;//in seconds
	int levelEllapsedTime;
	int levelLastTick;
	
	bool NoEnemies(int cellX,int cellY);

	bool loadLevel(const std::string& filename);

	void removeDeadEnemies();
	bool isDead(cEnemy* enemy);

	void removeDeadItems();
	bool isDeadItem(cItem* item);

	void collisionsPE(cEnemy* e);
	void collisionsPPr();
	void collisionsEPr(cEnemy* e);
	void collisionsPI(cItem* i);

	void initPlayer();
	void initEnemy();
	void initDefaultProjectileData();

	void doLogic(int ticks);
	
	//several state flags
	bool gameOver;
	bool firstLoad;
	bool paused;
	bool levelCleared;
	bool hurryUp;

	bool showMinimap;
	
	//level management attributes
	std::string nextLevelPath;
	std::string currentLevelName;
	std::string currentLevelPath;
	
	int getTotalScore() const;
	void addTotalScore(int amount);
	void resetScores();
	int getLevelScore() const;
	void setLevelScore(int kscore);
	void addLevelScore(int amount);

	void startLevel();
	//prepare for new Game
	void resetData();
	void resetForNewLevel();


private:
	/*void Render(int mouseX,int mouseY);*/
	//Player, enemies,...
	Timer* timer;
    //State management.
    TGameStateID    currentStateID;
    TGameStateID    nextStateID;
    GameState*     currentState;

	int levelScore;
	int bonusScore;
	int totalScore;
};
#endif
