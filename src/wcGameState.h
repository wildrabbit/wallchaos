#ifndef WCGAMESTATE_H
#define WCGAMESTATE_H

#include "cGameState.h"

class cGame;
class WallChaosGameStateFactory:GameStateFactory
{
	public:
		GameState* createGameState(int stateId, cGame* app);
		static WallChaosGameStateFactory* getInstance();
	private:
		static WallChaosGameStateFactory* _instance;

};
#endif