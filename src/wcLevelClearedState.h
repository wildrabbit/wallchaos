#ifndef LEVELCLEAREDSTATEH_H
#define LEVELCLEAREDSTATEH_H

#include "cCommonTypes.h"
#include "wcGameState.h"

//#define LEVELCLEARED_INIT_DELAY 1000

class LevelClearedState:public GameState
{
	private:
		int ticks;
		bool pressed;

		int bonusScore;
		int baseLevelScore;
		int accumulatedScore;
		int bonusTime;

	public:
		LevelClearedState(cGame* parent);
		void handleInput();
		void logic(int ticks);
		void render();
		void init();
		void finalize();
		~LevelClearedState(){};
};

#endif
