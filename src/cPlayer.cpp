#include <algorithm>
#include <functional>
#include "cGame.h"
#include "cScene.h"
#include "cEnemy.h"
#include "cPlayer.h"
#include "cProjectile.h"
#include "cItem.h"

#ifdef _DEBUG
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif

cPlayer::cPlayer(std::string playerId,cGame* _parent):id(playerId),parent(_parent){
	selectedProjectileData=NULL;
	projectiles.clear();
}
cPlayer::~cPlayer(){
}
void cPlayer::clearData()
{
	while (!projectiles.empty())
	{
		delete projectiles.back();
		projectiles.pop_back();
	}
	if (selectedProjectileData)
	{
		delete selectedProjectileData;
		selectedProjectileData=NULL;
	}
}



void cPlayer::getCellPosition(int& cX, int& cY) const
{
	cX=cellX;
	cY=cellY;
}
void cPlayer::setCellPosition(int cX,int cY)
{
	cellX=cX;
	cellY=cY;
}
void cPlayer::getPosition(int& sX, int& sY) const
{
	sX=posX;
	sY=posY;
}
void cPlayer::setPosition(int sX, int sY)
{
	posX=sX;
	posY=sY;
}
void cPlayer::getInitialCellPosition(int& cX, int& cY) const
{
	cX=initialCellX;
	cY=initialCellY;
}
void cPlayer::setInitialCellPosition(int& cX,int& cY)
{
	initialCellX=cX;
	initialCellY=cY;
}
const TPlayerStatus& cPlayer::getStatus()const
{
	return status;
}
void cPlayer::setStatus(const TPlayerStatus& newStatus)
{
	status=newStatus;	
	//seqFrame=0;
	seqDelay=animations[status].sequenceDelay;
	seqTotalFrames=animations[status].sequenceNumFrames;
	if(newStatus==STATUS_DAMAGED || newStatus==STATUS_DEAD)
	{
		gracePeriod = GRACE_PERIOD_DELAY;
		gracePeriodTicks = parent->getTimer()->getTicks();
	}
}
int cPlayer::getTextureID() const
{
	return SORA_INDEX;
}
RECT cPlayer::getTextureDimensions() const
{
	RECT rc;
	int yOffset;
	switch(status)
	{
		case STATUS_STILL:
				SetRect(&rc,PLAYER_WIDTH*facing,0,PLAYER_WIDTH*(facing+1),PLAYER_HEIGHT);
				break;
		case STATUS_MOVING:
			yOffset=PLAYER_HEIGHT;
			break;
		case STATUS_DEAD: case STATUS_DAMAGED:
			yOffset=PLAYER_HEIGHT*5;
			break;
		case STATUS_SHOOTING:
			yOffset=PLAYER_HEIGHT*9;
			break;
	}
	if (status!=STATUS_STILL)
	{
		rc.left = PLAYER_WIDTH*((facing%2)*seqTotalFrames+seqFrame);
		rc.right = rc.left+PLAYER_WIDTH;
		rc.top = yOffset + PLAYER_HEIGHT*(facing>>1);
		rc.bottom = rc.top + PLAYER_HEIGHT;
	}
	return rc;
}

void cPlayer::update(int ticks)
{
	if (inGracePeriod())
	{
		if(ticks-gracePeriodTicks>=gracePeriod)
		{
			gracePeriod=0;
			gracePeriodTicks=0;
		}
	}
	if((ticks-this->animationTicks)>=seqDelay)
	{		
		switch(status)
		{
		case STATUS_DAMAGED: 
			seqFrame=seqFrame+1;
			if(seqFrame>=seqTotalFrames)
			{
				setStatus(STATUS_STILL);
				seqFrame=0;
			}
			break;
		case STATUS_DEAD:
			seqFrame=seqFrame+1;
			if(seqFrame>=seqTotalFrames)
			{
				if(numLives>0)
				{
					//Go back to the starting position
					//[[TODO]] Load starting position from the level info
					setStatus(STATUS_STILL);
					int baseX,baseY;
					parent->Scene.getBasePosition(baseX,baseY);
					int screenX,screenY;
					parent->Scene.translateCoordinates(initialCellX,initialCellY,screenX,screenY);
					setCellPosition(initialCellX,initialCellY);
					setPosition(screenX+baseX,screenY+baseY+(PLAYER_HEIGHT>>2));
					seqFrame=0;
					parent->Scene.setBasePosition(-(SCREEN_RES_X)>>1,-OFF_LIMITS_INC_Y);
				}
				else {
					parent->gameOver=true;
					//parent->Sound.playSound(DEATH_ID);
					parent->addTotalScore(parent->getLevelScore());
				}
			}
			break;
		case STATUS_SHOOTING:
			seqFrame++;
			if(seqFrame>=seqTotalFrames)
			{
				setStatus(STATUS_STILL);
				seqFrame=0;
			}
			break;
		default:
			seqFrame=(seqFrame+1)%seqTotalFrames; //This will depend on some other variables (f.instance, we
										//might want to change the status when the current animation ends)
			break;

		}
		this->animationTicks=ticks;	
	}
	TProjectileList::iterator itBegin,itEnd;
	itBegin=projectiles.begin();
	itEnd=projectiles.end();
	for (TProjectileList::iterator it=projectiles.begin();it!=projectiles.end();it++)
	{
		(*it)->update(ticks);
	}
	removeDeadProjectiles();
}
const TOrientation& cPlayer::getOrientation()const
{
	return facing;
}
void cPlayer::setOrientation(const TOrientation& orientation)
{
	facing=orientation;
}
void cPlayer::changeSpeed(int speedX, int speedY)
{
	this->speedX=speedX;
	this->speedY=speedY;
}
void cPlayer::move(cScene* scene)
{
	if(status==STATUS_MOVING)
	{
		//Reference point: feet+height rectangle.
		int newX=posX+speedX;
		int newY=posY+speedY;
		
		RECT rc=getCollisionRect();
		//If player collides with a wall, reverse direction. Otherwise, perform movement.
		int cX,cY,scrX,scrY;
		scene->translateCoordinates(0,0,scrX,scrY);
		scene->TileClicked(newX-rc.left+scrX,newY-rc.top+scrY,&cX,&cY);
		bool validUpperLeft = scene->isFreeCell(cX,cY);
		scene->TileClicked(newX-rc.left+scrX,newY+(rc.bottom-rc.top)+scrY,&cX,&cY);
		bool validLowerLeft = scene->isFreeCell(cX,cY);
		scene->TileClicked(newX+(rc.right-rc.left)+scrX,newY-rc.top+scrY,&cX,&cY);
		bool validUpperRight = scene->isFreeCell(cX,cY);
		scene->TileClicked(newX+(rc.right-rc.left)+scrX,newY+(rc.bottom-rc.top)+scrY,&cX,&cY);
		bool validLowerRight = scene->isFreeCell(cX,cY);
		bool canMove = validUpperLeft && validUpperRight && validLowerLeft && validLowerRight;

		if(canMove)
		{			
			posX+=speedX;
			posY+=speedY;
			//update cell coordinates
			scene->TileClicked(scrX+posX,scrY+posY,&cellX,&cellY);

		}
		//Scroll scene:
		int baseX,baseY;
		scene->getBasePosition(baseX,baseY);
		int nBaseX=baseX,nBaseY=baseY;
		//Remember to tune this when there's a GUI on top
		int absX=posX+scrX;
		int absY=posY+scrY-((SCREEN_RES_Y-SCREEN_RES_Y_GAMEVP));
		bool mustScroll = absX<=20*SCROLL_AMOUNT_X || absX>=SCREEN_RES_X-20*SCROLL_AMOUNT_X
			||absY<=20*SCROLL_AMOUNT_Y||absY>=SCREEN_RES_Y-20*SCROLL_AMOUNT_Y;
		if (absX<=20*SCROLL_AMOUNT_X)
			baseX-=SCROLL_AMOUNT_X;
		else if (absX>=(SCREEN_RES_X-20*SCROLL_AMOUNT_X))
			baseX+=SCROLL_AMOUNT_X;
		if(absY<=20*SCROLL_AMOUNT_Y)
			baseY-=SCROLL_AMOUNT_Y;
		else if(absY>=SCREEN_RES_Y_GAMEVP-20*SCROLL_AMOUNT_Y)
			baseY+=SCROLL_AMOUNT_Y;
		if(mustScroll) scene->setBasePosition(baseX,baseY);

	}
}
void cPlayer::setAnimationTicks(int ticks)
{
	this->animationTicks=ticks;
}
void cPlayer::setProjectileTicks(int ticks)
{
	this->projectileTicks=ticks;
}
void cPlayer::initAnimations(std::string fname)
{
	animations.clear();
	animations[STATUS_STILL].status=STATUS_STILL;
	animations[STATUS_STILL].sequenceDelay=25;
	animations[STATUS_STILL].sequenceNumFrames=1;
	animations[STATUS_MOVING].status=STATUS_MOVING;	
	animations[STATUS_MOVING].sequenceDelay=50;
	animations[STATUS_MOVING].sequenceNumFrames=8;
	animations[STATUS_DAMAGED].status=STATUS_DAMAGED;
	animations[STATUS_DAMAGED].sequenceDelay=50;
	animations[STATUS_DAMAGED].sequenceNumFrames=8;
	animations[STATUS_DEAD].status=STATUS_DEAD;
	animations[STATUS_DEAD].sequenceDelay=50;
	animations[STATUS_DEAD].sequenceNumFrames=8;
	animations[STATUS_SHOOTING].status=STATUS_SHOOTING;
	animations[STATUS_SHOOTING].sequenceDelay=50;
	animations[STATUS_SHOOTING].sequenceNumFrames=8;
}
void cPlayer::resetSeqFrame()
{
	seqFrame=0;
}
cGame* cPlayer::getParent()
{
	return parent;
}
bool cPlayer::hitTest(cEnemy* enemy)
{
	RECT enemyRect = enemy->getCollisionRect();
	int enemyX,enemyY;
	enemy->getPosition(enemyX,enemyY);		
	return !((posX-collisionRect.left>=enemyX+((enemyRect.right-enemyRect.left)>>1)) || 
		(posX+((collisionRect.right-collisionRect.left)>>1)<=enemyX-enemyRect.left) ||
		(posY-collisionRect.top>=enemyY+((enemyRect.bottom-enemyRect.top)>>1)) || 
		(posY+((collisionRect.bottom-collisionRect.top)>>1)<=enemyY-enemyRect.top));
}
bool cPlayer::hitTest(cProjectile* projectile)
{
	RECT projRect = projectile->getCollisionRect();
	int projX,projY;
	projectile->getPosition(projX,projY);
	return projectile->getStatus()==STATUS_TRAVELLING && !((posX-collisionRect.left>=projX+((projRect.right-projRect.left)>>1)) || 
		(posX+((collisionRect.right-collisionRect.left)>>1)<=projX-projRect.left) ||
		(posY-collisionRect.top>=projY+((projRect.bottom-projRect.top)>>1)) || 
		(posY+((collisionRect.bottom-collisionRect.top)>>1)<=projY-projRect.top));

}
bool cPlayer::hitTest(cItem* item)
{
	RECT itemRect = item->getCollisionRect();
	int itemX,itemY;
	item->getPosition(itemX,itemY);		
	return !((posX-collisionRect.left>=itemX+((itemRect.right-itemRect.left)>>1)) || 
		(posX+((collisionRect.right-collisionRect.left)>>1)<=itemX-itemRect.left) ||
		(posY-collisionRect.top>=itemY+((itemRect.bottom-itemRect.top)>>1)) || 
		(posY+((collisionRect.bottom-collisionRect.top)>>1)<=itemY-itemRect.top));
}

RECT cPlayer::getCollisionRect() const
{
	return collisionRect;
}
void cPlayer::setCollisionRect(const RECT& rect)
{
	collisionRect=rect;
}

int cPlayer::getHP() const
{
	return hitPoints;
}
void cPlayer::increaseHP(int amount)
{
	hitPoints+=amount;
}
void cPlayer::decreaseHP(int amount)
{
	hitPoints-=amount;
}
void cPlayer::increaseLives(int amount)
{
	numLives+=amount;
}
void cPlayer::decreaseLives(int amount)
{
	numLives-=amount;
	setHP(3);
}
int cPlayer::getNumLives() const
{
	return numLives;
}
void cPlayer::setHP(int hp)
{
	hitPoints=hp;
}
void cPlayer::setNumLives(int lives)
{
	numLives=lives;
}
void cPlayer::removeDeadProjectiles()
{
	if (!projectiles.empty())
	{
		for (TProjectileList::iterator it=projectiles.begin();it!=projectiles.end();it++)
		{
			if (*it && (*it)->isDead)
			{
				delete (*it);
				(*it)=NULL;
			}
		}
		projectiles.erase(std::remove_if(projectiles.begin(),projectiles.end(),std::bind1st(std::mem_fun(&cPlayer::isDead), this)),projectiles.end());
	}
}
bool cPlayer::isDead(cProjectile* proj)
{
	return proj==NULL;
}
bool cPlayer::canShoot (int ticks)
{
	return projectiles.empty() || ticks-projectileTicks>=selectedProjectileData->castingDelay;
}
bool cPlayer::shoot(int ticks)
{
	if (projectiles.empty() || ticks-projectileTicks>=selectedProjectileData->castingDelay)
	{
		cProjectile* newProj = new cProjectile("DEFAULT",selectedProjectileData,this);
		newProj->initData(ticks);
		newProj->setCellPosition(cellX,cellY);
		newProj->setPosition(posX,posY);
		newProj->setElevation(16);
		int projSpeedX, projSpeedY;
		switch(facing)
		{
			case ORIENTATION_EAST:
				projSpeedX=2*selectedProjectileData->speed+1;
				projSpeedY=0;
				break;
			case ORIENTATION_WEST:
				projSpeedX=-2*selectedProjectileData->speed-1;
				projSpeedY=0;
				break;
			case ORIENTATION_NORTH:
				projSpeedX=0;
				projSpeedY=-2*selectedProjectileData->speed-1;
				break;
			case ORIENTATION_SOUTH:
				projSpeedX=0;
				projSpeedY=2*selectedProjectileData->speed+1;
				break;
			case ORIENTATION_NORTHEAST:
				projSpeedY=-static_cast<int>(ceil((selectedProjectileData->speed*selectedProjectileData->speed/FIVE_SQRT)));
				projSpeedX=-2*projSpeedY;
				break;
			case ORIENTATION_SOUTHEAST:
				projSpeedY=static_cast<int>(ceil((selectedProjectileData->speed*selectedProjectileData->speed/FIVE_SQRT)));
				projSpeedX=2*projSpeedY;
				break;
			case ORIENTATION_NORTHWEST:				
				projSpeedY=-static_cast<int>(ceil((selectedProjectileData->speed*selectedProjectileData->speed/FIVE_SQRT)));
				projSpeedX=2*projSpeedY;
				break;
			case ORIENTATION_SOUTHWEST:				
				projSpeedY=static_cast<int>(ceil((selectedProjectileData->speed*selectedProjectileData->speed/FIVE_SQRT)));
				projSpeedX=-2*projSpeedY;
				break;
		}
		newProj->changeSpeed(projSpeedX,projSpeedY);
		newProj->setOrientation(facing);
		newProj->setStatus(STATUS_SPAWNING);
		newProj->setSeqFrame(0);
		newProj->isDead=false;
		//speed, lifetime,...
		projectiles.push_back(newProj);
		projectileTicks=ticks;
		return true;
	}
	//{
	////	//projectiles.push_back(new Projectile("DEFAULT",DEFAULT_PROJECTILE,this));
	//}
	return false;
}
bool cPlayer::hasProjectiles() const
{
	return !projectiles.empty();
}
TProjectileList& cPlayer::getProjectiles()
{
	return projectiles;
}
TProjectileData* cPlayer::getSelectedProjectileData() const
{
	return selectedProjectileData;
}
void cPlayer::setSelectedProjectileData(TProjectileData* projData)
{
	//when the spellbook is implemented, the player won't be responsible for
	//deleting the data instance, since the pointer will just hold a reference
	//to the spellbook instance
	if(selectedProjectileData)
	{
		delete selectedProjectileData;
	}
	selectedProjectileData=projData;
}
void cPlayer::findNearestSafeCell()
{
	int radius=1;
	bool found=false;
	while(!found && radius<(SCENE_HEIGHT>>1))
	{
		int dY,dX;
		for (dY=-radius;dY<=radius && !found;dY++)
		{
			for(dX=-radius;dX<=radius;dX++)
			{
				bool freeCell = this->parent->Scene.isFreeCell(cellX+dX,cellY+dY);
				bool safeCell = this->parent->NoEnemies(cellX+dX,cellY+dY) && noProjectiles(cellX+dX,cellY+dY);
				found = freeCell && safeCell;
				break;//inner
			}
			if(found) break; //outer
		}
		if (found)
		{
			setCellPosition(cellX+dX,cellY+dY);
			int baseX,baseY;
			parent->Scene.getBasePosition(baseX,baseY);
			int screenX,screenY;
			parent->Scene.translateCoordinates(cellX,cellY,screenX,screenY);
			parent->Player->setPosition(screenX+baseX,screenY+baseY+(PLAYER_HEIGHT>>2));

			break;//while
		}
		else radius++;
	}
}
bool cPlayer::noProjectiles(int cX,int cY)
{
	int x,y;
	for (TProjectileList::iterator it=projectiles.begin();it!=projectiles.end();it++)
	{
		(*it)->getCellPosition(x,y);
		if (x==cX && y==cY)
			return false;
	}
	return true;
}
bool cPlayer::inGracePeriod() const
{
	return gracePeriod>0;
}
void cPlayer::setGracePeriod(int amount)
{
	gracePeriod=amount;
}
void cPlayer::resetForNewLevel()
{
	clearData();
}