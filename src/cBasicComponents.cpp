#include "cBasicComponents.h"
PositionComponent::PositionComponent(GameObject* parent):GameComponent(parent)
{
	setPosition(-1,-1);
	setCellPosition(-1,-1);

}
PositionComponent::~PositionComponent(){}
void PositionComponent::getCellPosition(int& cellX, int& cellY) const
{
	cellX=this->cellX;
	cellY=this->cellY;
}
void PositionComponent::setCellPosition(int cellX, int cellY)
{
	this->cellX=cellX;
	this->cellY=cellY;
}
void PositionComponent::getPosition(int& fineX, int& fineY) const
{
	posX=this->fineX;
	posY=this->fineY;
}
void PositionComponent::setPosition(int fineX, int fineY)
{
	this->posX=fineX;
	this->posY=fineY;
}
const TGameComponentID& PositionComponent::getFamilyID() const
{
	return TGameComponentID("CP_POSITION");
}
const TGameComponentID& PositionComponent::getComponentID() const
{
	return TGameComponentID("CP_POSITION");
}
//-------------------------------------------------------------