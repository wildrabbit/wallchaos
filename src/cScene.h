#ifndef __SCENE_H__
#define __SCENE_H__

#define SCREEN_RES_X	800
#define SCREEN_RES_Y	600

#define SCREEN_RES_Y_GAMEVP 512

#define SCENE_WIDTH		20
#define SCENE_HEIGHT	20
#define SCENE_LAYERS	3

#define SCENE_Xo		400
#define SCENE_Yo		0

#define TILE_WIDTH  64
#define TILE_HEIGHT 32

#define MINIMAP_TILE_WIDTH 8
#define MINIMAP_TILE_HEIGHT 4
#define MINIMAP_RES_X 160
#define MINIMAP_RES_Y 80
#define MINIMAP_SCENE_Xo 80
#define MINIMAP_SCENE_Yo 88

#define OFF_LIMITS_INC_X   64
#define OFF_LIMITS_INC_Y   160

#define SCROLL_AMOUNT_X	5
#define SCROLL_AMOUNT_Y 5

#define TILESET_PATH "tileset00.png"
#define TILESET_INDEX 20
#define MINIMAP_TILESET_PATH "minimap00.png"
#define MINIMAP_TILESET_INDEX 30

#define TILE00_PATH	"Tile00.tga"
#define TILE01_PATH	"Tile01A.tga"
#define TILE02_PATH	"Tile02.tga"
#define TILE03_PATH	"Tile03.tga"

#define NOTILE_INDEX -1
#define TILE00_INDEX 0
#define TILE01_INDEX 1
#define TILE02_INDEX 2
#define TILE03_INDEX 3

#include "cCommonTypes.h"

class cScene
{
private:
	int baseX, baseY;
	void initTile(TTile* tile);
public:
	
	cScene();
	virtual ~cScene();

	void setBasePosition(int bX, int bY);
	void getBasePosition(int& bX,int& bY) const;

	void setTicks(int ticks);
	void update(int ticks);

	//copy information in map
	void setMap(TTile newMap[SCENE_HEIGHT][SCENE_WIDTH][SCENE_LAYERS]);

	void TileSelected(int mx,int my,int *tx,int *ty,int *atx,int *aty,int *Dx,int *Dy);
	void TileClicked(int mouseX, int mouseY, int* selectedX,int* selectedY);

	void translateCoordinates(int cX, int cY, int& screenX, int& screenY) const;

	TTile map[SCENE_HEIGHT][SCENE_WIDTH][SCENE_LAYERS];

	void getCellsAt(int cX, int cY, TTile*& cells, int& cellsSize) ;
	bool findFirstFreeCell(int& cX,int&cY) const;

	bool isFreeCell(int cX,int cY);
	bool isWallCell(int cX,int cY);

	RECT getTileRect(TTile* tileIndex) const;
};


#endif
