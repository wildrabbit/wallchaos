#include "cGame.h"
#include "cKeyboard.h"
#include "cMouse.h"
#include "wcGameEndedState.h"

GameEndedState::GameEndedState(cGame* parent):GameState(parent)
{}

void GameEndedState::init()
{
	menuStates.clear();
	menuStates[GOVER_MENU]=MENU;
	menuStates[GOVER_RESTART]=GAME_RUNNING;
	menuStates[GOVER_QUIT]=EXIT;
	SetRect(&mouseCursorClippingRect,0,0,MENU_CURSOR_WIDTH,MENU_CURSOR_HEIGHT);
	SetRect(&mouseOverClippingRect,0,MENU_CURSOR_HEIGHT,MENU_MOUSEOVER_WIDTH,MENU_MOUSEOVER_HEIGHT+MENU_CURSOR_HEIGHT);

	optionRects.clear();
	RECT rc;
	//New game
	SetRect(&rc,24,460,24+MENU_BUTTON_WIDTH,460+MENU_BUTTON_HEIGHT);
	optionRects[GOVER_MENU]=rc;
	SetRect(&rc,304,460,304+MENU_BUTTON_WIDTH,460+MENU_BUTTON_HEIGHT);
	optionRects[GOVER_RESTART]=rc;
	SetRect(&rc,587,460,587+MENU_BUTTON_WIDTH,460+MENU_BUTTON_HEIGHT);
	optionRects[GOVER_QUIT]=rc;
	
	mouseOverOption=GOVER_NONE;	
	parent->Graphics.LoadData(&cGraphicsLayer::loadGameEndedData);
	parent->Sound.LoadData(&cSoundLayer::loadGameEndedData);

	parent->Sound.playSound(parent->enemies.empty()?WIN_ID:DEATH_ID);
}
void GameEndedState::finalize()
{
	menuStates.clear();
	optionRects.clear();
	parent->Graphics.UnLoadData(&cGraphicsLayer::unloadAllData);
	parent->Sound.UnLoadData(&cSoundLayer::unloadAllData);
}

void GameEndedState::handleInput()
{
	if(parent)
	{
		cKeyboard* keyboard = parent->Input.GetKeyboard();
		cMouse* mouse = parent->Input.GetMouse();
		if (keyboard)
		{
			if (keyboard->KeyDown(DIK_M))
			{
				parent->resetData();
				parent->setNextStateID(MENU);
			}
			else if (keyboard->KeyDown(DIK_R) || keyboard->KeyDown(DIK_C))
			{
				parent->resetData();
				parent->setNextStateID(GAME_RUNNING);
			}
			else if (keyboard->KeyDown(DIK_E)||keyboard->KeyDown(DIK_ESCAPE) || keyboard->KeyDown(DIK_Q))
			{
				parent->setNextStateID(EXIT);
			}
		}
		if (mouse)
		{
			if(mouse->ButtonDown(LEFT))
			{
				parent->Input.GetMousePosition(&clickedX,&clickedY);
				TGOverOptions clickedOption = GOVER_NONE;
				for (TGOverOptionRectsMap::iterator it=optionRects.begin();it!=optionRects.end();it++)
				{	
					if (mouse->InRect(&(it->second)))
					{
						clickedOption=it->first;
						parent->Sound.playSound(MENU_CLICK_ID);
						break;
					}
				}
				if (clickedOption!=GOVER_NONE && menuStates.count(clickedOption)>0)
				{
						parent->setNextStateID(menuStates[clickedOption]);
						if (clickedOption==GOVER_MENU || clickedOption==GOVER_RESTART)
							parent->resetData();
				}
			}
			else if (mouse->IsMoving())
			{
				int pX,pY;
				mouse->GetPosition(&pX,&pY);
				mouseOverOption=GOVER_NONE;
				for (TGOverOptionRectsMap::iterator it=optionRects.begin();it!=optionRects.end();it++)
				{
					if (mouse->InRect(&(it->second)))
					{
						mouseOverOption=it->first;
						break;
					}
				}
			}
		}
	}
}
void GameEndedState::logic(int ticks)
{
	while(parent->Sound.isSoundPlaying(MENU_CLICK_ID));
}
void GameEndedState::render()
{
	if (parent)
	{	
		TGameEndedRenderingData gData;
		parent->Input.GetMousePosition(&gData.mouseData.mouseX,&gData.mouseData.mouseY);
		gData.mouseData.clickedX=clickedX;
		gData.mouseData.clickedY=clickedY;
		gData.mouseData.mouseOverRect=(mouseOverOption!=MENU_NONE)?&optionRects[mouseOverOption]:NULL;
		gData.mouseData.mouseOverClippingRect=&mouseOverClippingRect;
		gData.mouseData.mouseCursorClippingRect=&mouseCursorClippingRect;
		gData.score=parent->getTotalScore();
		gData.gameCleared=parent->enemies.empty();

		TRenderingData renderingData;
		renderingData.gameEndedData=gData;

		parent->Graphics.Render(&renderingData,&cGraphicsLayer::renderGameEndedScene);
	}
}
