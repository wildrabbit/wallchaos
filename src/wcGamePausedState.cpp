#include "cGame.h"
#include "wcGamePausedState.h"

GamePausedState::GamePausedState(cGame* parent):GameState(parent){}

void GamePausedState::init()
{
	if(parent)
	{
		parent->Graphics.LoadData(&cGraphicsLayer::loadGameData);
		parent->Sound.LoadData(&cSoundLayer::loadGameData);
		changeState=false;
		quit=false;
	}
}
void GamePausedState::finalize()
{
	if (parent)
	{
		parent->Graphics.UnLoadData(&cGraphicsLayer::unloadAllData);
		parent->Sound.UnLoadData(&cSoundLayer::unloadAllData);
		parent=NULL;
	}
}
void GamePausedState::handleInput()
{
	if (parent && parent->Input.GetKeyboard())
	{		
		if (parent->Input.KeyDown(DIK_P))
		{
			changeState=true;
			parent->Sound.playSound(PAUSE_ID);
		}
		else if (parent->Input.KeyDown(DIK_ESCAPE))
		{
			quit=true;
		}
	}
}
void GamePausedState::logic(int ticks)
{
	if(quit) parent->setNextStateID(EXIT);
	else{
		if(!parent->paused) parent->paused=true;
		if(changeState && !parent->Sound.isSoundPlaying(PAUSE_ID))
			parent->setNextStateID(GAME_RUNNING);
	}
}
void GamePausedState::render()
{
	if (parent)
	{	
		TGameRunningRenderingData gameData;
		gameData.scene=&(parent->Scene);
		gameData.player=parent->Player;
		gameData.enemies=&parent->enemies;
		gameData.score=parent->getTotalScore()+parent->getLevelScore();
		gameData.remainingTime=(parent->levelCompletionTime-parent->levelEllapsedTime)/1000;//data to render in seconds
		gameData.paused=parent->paused;
		gameData.showMinimap=parent->showMinimap;
		gameData.items=&parent->items;

		TRenderingData renderingData;
		renderingData.gameData=gameData;
		parent->Graphics.Render(&renderingData,&cGraphicsLayer::renderGameScene);		
	}
}
