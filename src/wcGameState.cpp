#include "cGame.h"
#include "wcGameState.h"
#include "wcIntroState.h"
#include "wcMenuState.h"
#include "wcGameRunningState.h"
#include "wcGamePausedState.h"
#include "wcGameEndedState.h"
#include "wcLevelClearedState.h"

GameState* WallChaosGameStateFactory::createGameState(int stateID, cGame* app)
{
	if(stateID<MIN_ID || stateID>MAX_ID)
			return NULL;
	switch((TGameStateID)stateID){
		case INTRO:
			return new IntroState(app);
			break;
		case MENU:
			return new MenuState(app);
		case GAME_RUNNING:
			return new GameRunningState(app);
			break;
		case GAME_PAUSED:
			return new GamePausedState(app);
		case GAME_ENDED:
			return new GameEndedState(app);
		case LEVEL_CLEARED:
			return new LevelClearedState(app);
		default:break;
	}
		return NULL;
}
WallChaosGameStateFactory* WallChaosGameStateFactory::_instance = NULL;
WallChaosGameStateFactory* WallChaosGameStateFactory::getInstance()
{
	if (!_instance)
	{
        _instance = new WallChaosGameStateFactory();
    }
    return _instance;
}
