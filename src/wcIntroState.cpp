#include "cGame.h"
#include "cKeyboard.h"
#include "wcIntroState.h"

IntroState::IntroState(cGame* parent):GameState(parent){}

void IntroState::finalize()
{
	if (parent)
	{
		parent->Graphics.UnLoadData(&cGraphicsLayer::unloadAllData);
		parent->Sound.UnLoadData(&cSoundLayer::unloadAllData);
		parent=NULL;
	}
}

void IntroState::init()
{
	if(parent)
	{
		parent->Graphics.LoadData(&cGraphicsLayer::loadIntroData);
		parent->Sound.LoadData(&cSoundLayer::loadIntroData);
		parent->Sound.playSound(ZELDA_ID);
		pressed=false;
		quit=false;
	}
}
void IntroState::handleInput()
{
	if (parent && parent->Input.GetKeyboard())
	{
		cKeyboard* keyboard = parent->Input.GetKeyboard();
		if (keyboard->KeyDown(DIK_ESCAPE))
		{
			quit=true;
		}
		if (keyboard->isAnyKeyPressed())
		{
			pressed=true;
		}

	}
}
void IntroState::logic(int ticks)
{
	if(quit)
		parent->setNextStateID(EXIT);
	else if (!parent->Sound.isSoundPlaying(ZELDA_ID) || pressed)
		parent->setNextStateID(MENU);
}
void IntroState::render()
{
	if (parent)
	{	
		parent->Graphics.Render(NULL,&cGraphicsLayer::renderIntroScene);
	}
}
