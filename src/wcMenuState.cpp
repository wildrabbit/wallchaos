#include "cGame.h"
#include "cKeyboard.h"
#include "cMouse.h"
#include "wcMenuState.h"

MenuState::MenuState(cGame* parent):GameState(parent)
{}

void MenuState::init()
{
	menuStates.clear();
	menuStates[MENU_NEW_GAME]=GAME_RUNNING;
	menuStates[MENU_EXIT]=EXIT;
	SetRect(&mouseCursorClippingRect,0,0,MENU_CURSOR_WIDTH,MENU_CURSOR_HEIGHT);
	SetRect(&mouseOverClippingRect,0,MENU_CURSOR_HEIGHT,MENU_MOUSEOVER_WIDTH,MENU_MOUSEOVER_HEIGHT+MENU_CURSOR_HEIGHT);

	optionRects.clear();
	RECT rc;
	//New game
	SetRect(&rc,24,460,24+MENU_BUTTON_WIDTH,460+MENU_BUTTON_HEIGHT);
	optionRects[MENU_NEW_GAME]=rc;
	//Reserved
	//SetRect(&rc,304);
	//Exit
	SetRect(&rc,587,460,587+MENU_BUTTON_WIDTH,460+MENU_BUTTON_HEIGHT);
	optionRects[MENU_EXIT]=rc;
	mouseOverOption=MENU_NONE;	
	parent->Graphics.LoadData(&cGraphicsLayer::loadMenuData);
	parent->Sound.LoadData(&cSoundLayer::loadMenuData);
	parent->Sound.playSound(MENU_MUSIC_ID,true);
}
void MenuState::finalize()
{
	menuStates.clear();
	optionRects.clear();
	parent->Graphics.UnLoadData(&cGraphicsLayer::unloadAllData);
	parent->Sound.UnLoadData(&cSoundLayer::unloadAllData);
}

void MenuState::handleInput()
{
	if(parent)
	{
		cKeyboard* keyboard = parent->Input.GetKeyboard();
		cMouse* mouse = parent->Input.GetMouse();
		if (keyboard)
		{
			if (keyboard->KeyDown(DIK_N))
			{
				parent->setNextStateID(GAME_RUNNING);
			}
			else if (keyboard->KeyDown(DIK_Q) || keyboard->KeyDown(DIK_ESCAPE) || keyboard->KeyDown(DIK_E))
			{
				parent->setNextStateID(EXIT);
			}
		}
		if (mouse)
		{
			if(mouse->ButtonDown(LEFT))
			{
				parent->Input.GetMousePosition(&clickedX,&clickedY);
				TMenuOptions clickedOption = MENU_NONE;
				for (TMenuOptionRectsMap::iterator it=optionRects.begin();it!=optionRects.end();it++)
				{	
					if (mouse->InRect(&(it->second)))
					{
						clickedOption=it->first;
						parent->Sound.playSound(MENU_CLICK_ID);
						break;
					}
				}
				if (clickedOption!=MENU_NONE && menuStates.count(clickedOption)>0)
				{
						parent->setNextStateID(menuStates[clickedOption]);
				}
			}
			else if (mouse->IsMoving())
			{
				int pX,pY;
				mouse->GetPosition(&pX,&pY);
				mouseOverOption=MENU_NONE;
				for (TMenuOptionRectsMap::iterator it=optionRects.begin();it!=optionRects.end();it++)
				{
					if (mouse->InRect(&(it->second)))
					{
						mouseOverOption=it->first;
						break;
					}
				}
			}
		}
	}
}
void MenuState::logic(int ticks)
{
	while(parent->Sound.isSoundPlaying(MENU_CLICK_ID));
}
void MenuState::render()
{
	if (parent)
	{	
		TMouseRenderingData mouseData;
		parent->Input.GetMousePosition(&mouseData.mouseX,&mouseData.mouseY);
		mouseData.clickedX=clickedX;
		mouseData.clickedY=clickedY;
		mouseData.mouseOverRect=(mouseOverOption!=MENU_NONE)?&optionRects[mouseOverOption]:NULL;
		mouseData.mouseOverClippingRect=&mouseOverClippingRect;
		mouseData.mouseCursorClippingRect=&mouseCursorClippingRect;

		TRenderingData renderingData;
		renderingData.menuData=mouseData;

		parent->Graphics.Render(&renderingData,&cGraphicsLayer::renderMenuScene);
	}
}
