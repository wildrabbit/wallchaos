#ifndef BASIC_COMPONENTSH_H
#define BASIC_COMPONENTSH_H
#include "cGameObject.h"
class PositionComponent:public GameComponent
{
public:
	PositionComponent(GameObject* parent);
	virtual ~PositionComponent();
	void getCellPosition(int& cellX, int& cellY) const;
	void setCellPosition(int cellX, int cellY);
	
	void getPosition(int& fineX, int& fineY) const;
	void setPosition(int fineX, int fineY);

	const TGameComponentID& getFamilyID() const;
	virtual const TGameComponentID& getComponentID() const;

private:
	int posX,posY;
	int cellX,cellY;
};
#endif