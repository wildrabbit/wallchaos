#include "cMouse.h"
#include "cLog.h"
#include <dxerr.h>

/************************************************************************
cMouse::Constructor()

Initialize DI device
*************************************************************************/
cMouse::cMouse(LPDIRECTINPUT8 pDI, HWND hwnd, bool isExclusive)
{
	cLog *Log = cLog::Instance();
	HRESULT hr;

	hr = pDI->CreateDevice(GUID_SysMouse, &m_pDIDev, NULL);
	if(FAILED(hr))
	{
		Log->Error(hr,"Creating mouse device");
	}
	hr = m_pDIDev->SetDataFormat(&c_dfDIMouse);
	if(FAILED(hr))
	{
		Log->Error(hr,"Setting mouse data format");
	}
	//DIMOUSESTATE Structure - http://msdn.microsoft.com/library/default.asp?url=/library/en-us/directx9_c/directx/input/ref/structs/dimousestate.asp
	
	DWORD flags;
	if (isExclusive)
	{
		flags = DISCL_FOREGROUND | DISCL_EXCLUSIVE | DISCL_NOWINKEY;
	}
	else
	{
		flags = DISCL_FOREGROUND | DISCL_NONEXCLUSIVE;
		ShowCursor(false);
	}

	hr = m_pDIDev->SetCooperativeLevel(hwnd, flags);
	if (FAILED(hr))
	{
		Log->Error(hr,"Setting mouse cooperative level");
	}
	hr = m_pDIDev->Acquire();
	if(FAILED(hr))
	{
		Log->Error(hr,"Acquiring mouse");
	}
	hr = m_pDIDev->GetDeviceState(sizeof(DIMOUSESTATE), &m_state);
	if(FAILED(hr))
	{
		Log->Error(hr,"Getting mouse device state");
	}
	GetWindowRect(hwnd,&screenBoundaries);
}


/***************************************************************************
cMouse::Destructor()

Release DI device
***************************************************************************/
cMouse::~cMouse()
{
	if(m_pDIDev)
	{
		m_pDIDev->Unacquire();
		m_pDIDev->Release();
	}
}

/******************************************************************************
cMouse::Read()

Queuries the current state of the mouse and stores it in the member variables
********************************************************************************/
bool cMouse::Read()
{
	cLog *Log = cLog::Instance();
	HRESULT hr;

	hr = m_pDIDev->GetDeviceState(sizeof(DIMOUSESTATE), &m_state);
	if(FAILED(hr))
	{
		Log->Error(hr,"Getting mouse device state");

		hr = m_pDIDev->Acquire();
		if(FAILED(hr))
		{
			Log->Error(hr,"Acquiring mouse");
			return false;
		}
		hr = m_pDIDev->GetDeviceState(sizeof(DIMOUSESTATE), &m_state);
		if(FAILED(hr))
		{
			Log->Error(hr,"Getting mouse device state");
			return false;
		}
	}
	x+=m_state.lX;
	//Constrain the coordinates inside the screen
	if (x<screenBoundaries.left) x=screenBoundaries.left;
	else if (x>screenBoundaries.right)x=screenBoundaries.right;
	y+=m_state.lY;
	if (y<screenBoundaries.top) y=screenBoundaries.top;
	else if (y>screenBoundaries.bottom)y=screenBoundaries.bottom;
	return true;
}

/******************************************************************************
cMouse::Acquire()

Acquires mouse
********************************************************************************/
bool cMouse::Acquire()
{
	cLog *Log = cLog::Instance();
	HRESULT hr;

	hr = m_pDIDev->Acquire();
	if(FAILED(hr))
	{
		Log->Error(hr,"Acquiring mouse");
		return false;
	}
	return true;
}


/******************************************************************************
cMouse::Acquire()

Unacquires mouse
********************************************************************************/
bool cMouse::Unacquire()
{
	cLog *Log = cLog::Instance();
	HRESULT hr;

	hr = m_pDIDev->Unacquire();
	if(FAILED(hr))
	{
		Log->Error(hr,"Unacquiring mouse");
		return false;
	}
	return true;
}

/******************************************************************************
Mouse queries
********************************************************************************/
bool cMouse::ButtonDown(int button)
{
	return (m_state.rgbButtons[button] & 0x80) ? true : false;
}
bool cMouse::ButtonUp(int button)
{
	return (m_state.rgbButtons[button] & 0x80) ? false : true;
}
int  cMouse::GetWheelMovement()
{
	return m_state.lZ;
}
void cMouse::GetMovement(int *dx, int *dy)
{
	*dx = m_state.lX;
	*dy = m_state.lY;
}
void cMouse::SetPosition(int xo, int yo)
{
	x=xo;
	y=yo;
}
void cMouse::GetPosition(int *xpos, int *ypos)
{
	*xpos=x;
	*ypos=y;
}
bool cMouse::InRect(LPRECT rect)
{
	return x>=rect->left && x<=rect->right && y>=rect->top && y<=rect->bottom;
}
bool cMouse::IsMoving() const
{
	return m_state.lX!=0 || m_state.lY!=0;
}
