#ifndef UTILSH_H
#define UTILSH_H

#include <string>

namespace Utils
{
	float float_rand();
	inline void int_swap(int& val1, int& val2);
	std::string getPaddedIntegerString(char fillChar, int width, int value);
};
#endif