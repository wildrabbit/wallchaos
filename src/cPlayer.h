#ifndef PLAYERH_H
#define PLAYERH_H

#include <string>
#include <map>
#include <vector>
#include <windows.h>

#include "cCommonTypes.h"
#include "cProjectile.h"

#define SORA_INDEX 5
#define SORA_PATH "player00.png"

#define PLAYER_SPEED_X 4
#define PLAYER_SPEED_Y 2

#define PLAYER_WIDTH 32
#define PLAYER_HEIGHT 32

#define PLAYER_ANCHOR_OFFSET_X	16
#define PLAYER_ANCHOR_OFFSET_Y	24

#define GRACE_PERIOD_DELAY 2500

#define PLAYER_DEFAULT_LIVES 3
#define PLAYER_DEFAULT_HP 3


typedef enum
{
	STATUS_STILL,
	STATUS_MOVING,
	STATUS_RUNNING,
	STATUS_SHOOTING,
	STATUS_DAMAGED,
	STATUS_DEAD
} TPlayerStatus;

class cScene;
class cGame;
class cEnemy;
class cProjectile;
class cItem;


typedef std::vector<cProjectile*> TProjectileList;

class cPlayer
{
private:
	cGame* parent;
	//Global entity identifier
	std::string id;
	//Positional data
	int cellX,cellY;	//in cells
	int posX,posY;		//in pixels (world coordinates!)
	int initialCellX, initialCellY;
	//Direction the char's facing to
	TOrientation facing;
	int speedX,speedY;

	//Movement data
	//ITrajectory* trajectory;
	int animationTicks;
	int projectileTicks;

	//Behaviour data (i.e: status)
	TPlayerStatus status;	
	
	//Current animation data
	int seqFrame;
	int seqTotalFrames;
	int seqDelay;
	TAnimations animations;

	//Collision data
	RECT collisionRect;

	int numLives;
	int hitPoints;

	TProjectileList projectiles;	
	TProjectileData* selectedProjectileData;	//Replace with a mere ID when a "spellbook" is implemented
	//std::string selectedProjectileID;
	int spellTicks;
	
	int gracePeriod;
	int gracePeriodTicks;

public:
	cPlayer(std::string id,cGame* _parent);
	virtual ~cPlayer();
	const std::string& getId() const;
	void getCellPosition(int& cX, int& cY) const;
	void setCellPosition(int cX,int cY);
	
	void getPosition(int& sX, int& sY) const;
	void setPosition(int sX, int sY);

	void getInitialCellPosition(int& cX, int& cY) const;
	void setInitialCellPosition(int& cX,int& cY);

	const TOrientation& getOrientation()const;
	void setOrientation(const TOrientation& orientation);

	const TPlayerStatus& getStatus()const;
	void setStatus(const TPlayerStatus& newStatus);

	int getTextureID() const;
	RECT getTextureDimensions() const;

	void changeSpeed(int speedX, int speedY);
	void move(cScene* scene);

	void setAnimationTicks(int ticks);
	void setProjectileTicks(int ticks);
	void update(int ticks);

	void initAnimations(std::string fname="");
	void resetSeqFrame();

	bool hitTest(cEnemy* enemy);
	bool hitTest(cProjectile* projectile);
	bool hitTest(cItem* item);

	cGame* cPlayer::getParent();

	RECT getCollisionRect() const;
	void setCollisionRect(const RECT& rect);

	void increaseHP(int amount=1);
	void decreaseHP(int amount=1);
	void increaseLives(int amount=1);
	void decreaseLives(int amount=1);
	int getHP() const;
	int getNumLives() const;
	void setHP(int hp);
	void setNumLives(int lives);
	
	bool shoot(int ticks);
	bool canShoot (int ticks);
	bool hasProjectiles() const;
	TProjectileList& getProjectiles();

	void clearData();
	TProjectileData* getSelectedProjectileData() const;
	void setSelectedProjectileData(TProjectileData* projData);

	void removeDeadProjectiles();
	bool isDead(cProjectile* proj);
	
	void findNearestSafeCell();
	bool noProjectiles(int cellX,int cellY);

	bool inGracePeriod() const;
	void setGracePeriod(int amount);

	void resetForNewLevel();
};
#endif