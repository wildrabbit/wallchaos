#include "cGame.h"
#include "cKeyboard.h"
#include "wcLevelClearedState.h"

LevelClearedState::LevelClearedState(cGame* parent):GameState(parent)
{}

void LevelClearedState::init()
{
	parent->Graphics.LoadData(&cGraphicsLayer::loadLevelClearedData);
	parent->Sound.LoadData(&cSoundLayer::loadLevelClearedData);
	parent->Sound.playSound(LVL_CLEARED_SND_ID);

	accumulatedScore=parent->getTotalScore();
	baseLevelScore=parent->getLevelScore();
	bonusScore=0;
	bonusTime=(parent->levelCompletionTime-parent->levelEllapsedTime)/1000;
	
	pressed=false;
	ticks= parent->getTimer()->getTicks();
}
void LevelClearedState::finalize()
{
	parent->Graphics.UnLoadData(&cGraphicsLayer::unloadAllData);
	parent->Sound.UnLoadData(&cSoundLayer::unloadAllData);
}

void LevelClearedState::handleInput()
{
	if(bonusTime==0)//Score's been set
	{
		cKeyboard* keyboard = parent->Input.GetKeyboard();
		if (keyboard && keyboard->isAnyKeyPressed())
		{
			pressed=true;
			parent->Sound.playSound(MENU_CLICK_ID);
		}
	}
}
void LevelClearedState::logic(int ticks)
{
	if (!parent->Sound.isSoundPlaying(LVL_CLEARED_SND_ID))
	{
		if (!pressed)
		{
			if(bonusTime>0)
			{
				bonusTime--;
				bonusScore+=BONUS_SCORE_PER_SECOND;
			}
		}
		else
		{
			parent->addTotalScore(baseLevelScore+bonusScore);
			if(parent->nextLevelPath.empty())
			{
				parent->setNextStateID(GAME_ENDED);
			}
			else
			{
				parent->setNextStateID(GAME_RUNNING);
				//Remember the total score in "game running" is computed
				//from adding together the total score (the punctuation in the previous
				//levels) and the killingsScore;
				parent->resetForNewLevel();
			}
		}
	}
}
void LevelClearedState::render()
{
	if (parent)
	{	
		TLevelClearedRenderingData lcData;
		lcData.accumulatedScore=accumulatedScore;
		lcData.levelBaseScore=baseLevelScore;
		lcData.levelTotalScore=baseLevelScore+bonusScore;
		lcData.timeBonus=bonusTime;
		lcData.timeScore=bonusScore;
		lcData.totalScore=lcData.levelTotalScore+accumulatedScore;

		TRenderingData renderingData;
		renderingData.levelClearedData=lcData;

		parent->Graphics.Render(&renderingData,&cGraphicsLayer::renderLevelClearedScene);
	}
}
