#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <functional>
#include "wcGameState.h"
#include "cGame.h"
#include "cLog.h"

#ifdef _DEBUG
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif


#define SAFEDELETE(x) if(x) {delete x;x=NULL;}


cGame::cGame() {
	timer=NULL;
	Player=NULL;
	enemies.clear();
	items.clear();
}
cGame::~cGame(){}

bool cGame::Init(HWND hWnd,HINSTANCE hInst,bool exclusive)
{
	bool res=true;
	cLog *Log = cLog::Instance();

	res = Graphics.Init(hWnd);
	if(!res)
	{
		Log->Msg("Error initializing Graphics!");
		return false;
	}

	timer = new Timer();
	timer->start();

	res = Input.Init(hInst,hWnd,exclusive,USE_MOUSE|USE_KEYBOARD);
	if(!res)
	{
		Log->Msg("Error initializing Input!");
		return false;
	}
	Input.SetMousePosition(SCREEN_RES_X >> 1,SCREEN_RES_Y >> 1);

	if (!Sound.Init())
	{
		Log->Msg("Error initializing Sound!");
		return false;
	}

	currentStateID=INTRO;
	nextStateID=STATE_NULL;
	currentState=WallChaosGameStateFactory::getInstance()->createGameState(currentStateID,this);
	currentState->init();

	gameOver=false;	
	levelCleared=false;
	paused=false;
	firstLoad=true;
	hurryUp=false;

	showMinimap=false;

	srand(24);//[[TODO]] Replace seed

	totalScore=0;
	levelEllapsedTime=0;
	
	return res;
}

bool cGame::Loop()
{    
    int t1;
	cLog *Log = cLog::Instance();

    if (currentStateID!=EXIT)
    {
		t1=timer->getTicks();
		//Input
		if (!Input.Read())
		{
			Log->Msg("Error reading Input!");
			return false;
		}
		//Input.GetMousePosition(&mouseX,&mouseY);
		
		currentState->handleInput();
		currentState->logic(timer->getTicks());
        changeState();
        currentState->render();

		do{//Frame-rate synchronization.
                //TODO: Check http://dewitters.koonsolo.com/gameloop.html and refine if needed
        } while(timer->getTicks()-t1<=1000/FPS);
		return true;
    }
	else
	{
		currentState->finalize();
		SAFEDELETE(currentState);
		return false;
	}

}
Timer* cGame::getTimer()
{
	return timer;
}
void cGame::changeState()
{
	if(nextStateID!=STATE_NULL)
	{
		if(nextStateID!=EXIT)//currentState'll be null
		{
			currentState->finalize();
			SAFEDELETE(currentState);
		}		
		GameState* newState=WallChaosGameStateFactory::getInstance()->createGameState(nextStateID,this);
		if(newState){
			currentState=newState;
			newState=NULL;
			currentState->init();
		}
		currentStateID=nextStateID;
		nextStateID=STATE_NULL;
	}
}

void cGame::Finalize()
{	
	Graphics.Finalize();
	Input.UnacquireAll();
	Input.Finalize();
	Sound.Finalize();
	SAFEDELETE(timer);
	if(Player)
		Player->clearData();
	SAFEDELETE(Player);
	for (TEnemyList::iterator it=enemies.begin();it!=enemies.end();it++)
	{
		SAFEDELETE((*it));
	}
	enemies.clear();
	for (TItemList::iterator it=items.begin();it!=items.end();it++)
	{
		SAFEDELETE((*it));
	}
	items.clear();
}


/*
 * Return the value of the next state's id
 */
TGameStateID cGame::getNextStateID() const
{
	return nextStateID;
}
/*
 * Set the value of the next state id
 */
void cGame::setNextStateID(const TGameStateID& nextStateId)
{
	this->nextStateID=nextStateId;
}

bool cGame::NoEnemies(int cellX,int cellY)
{
	int cX,cY;	
	for (TEnemyList::iterator it=enemies.begin();it!=enemies.end();it++)
	{
		cEnemy* enemy=*it;
		enemy->getCellPosition(cX,cY);
		if (cX==cellX && cY==cellY) return false;
	}
	return true;
}

bool cGame::loadLevel(const std::string& filename)
{
	std::string line;
	std::string path(DATA_FOLDER_LEVELS);
	path.append("\\").append(filename);
	std::ifstream levelData(path.c_str());
	std::istringstream lineReader;

	bool newGame=nextLevelPath.empty();
	
	//Current stage of parsing
	TLevelReadStage readStage= LEVEL_INITIAL;
		
	//[[TODO]] Read the map dimensions from the file as well
	//This will involve a complete re-design of the cScene class
	//Scene data local vars
	TTile readMap[SCENE_HEIGHT][SCENE_WIDTH][SCENE_LAYERS];
	int z=0,y=0,x=0;

	//Enemy variables
	bool isEnemyTypeRead=false;	
	cEnemy* currentEnemy=NULL;

	bool isItemTypeRead=false;
	cItem* currentItem=NULL;

	if (levelData.is_open())
	{
		currentLevelPath=filename;
		while (std::getline(levelData,line))
		{			
			if (line.at(0)!=LEVELFILE_COMMENT) //Ignore comments
			{
				if (line.substr(0,2).compare(LEVELFILE_CATEGORY_SEPARATOR)==0)
				{
					switch(readStage)
					{
						case LEVEL_MAPLAYERS:
							Scene.setMap(readMap);
							Scene.setTicks(timer->getTicks());
							break;
						case LEVEL_ENEMIES:
							if (currentEnemy)
								enemies.push_back(currentEnemy);
							isEnemyTypeRead=false;
							break;
						case LEVEL_ITEMS:
							if(currentItem)
								items.push_back(currentItem);
							isItemTypeRead=false;
							break;
					}
					readStage=(TLevelReadStage)((int)readStage+1);
				}
				else 
				switch(readStage)
				{
				//case LEVEL_MAPINFO:
				//	std::istringstream intReader;
				//	intReader.str(line);
				//	int mLayers,mHeight,mWidth;
				//	line>>mLayers>>mHeight>>mWidth;
				//	break;
				case LEVEL_NAME:
					currentLevelName=line;
					break;
				case LEVEL_NEXTLEVELPATH:
					if (line.substr(0,2).compare(LEVELFILE_LAST_LEVEL)==0)
						nextLevelPath.clear();
					else
						nextLevelPath=line;
					break;
				case LEVEL_MAPLAYERS:
					if(line.at(0)==LEVELFILE_CATEGORY_LIST_SEPARATOR && (line.length()<=1 || line.at(1)!=LEVELFILE_CATEGORY_LIST_SEPARATOR))
					{						
						z++;
						x=y=0;
					}
					else 
					{
						lineReader.clear();
						lineReader.str(line);
						std::string index;
						while(std::getline(lineReader,index,' '))
						{
							if (!index.empty()){
								readMap[y][x][z].index=((TTileIndex)atoi(index.c_str()));
								x++;
							}
						}
						y++;
						x=0;
					}
					break;
				case LEVEL_PLAYERINI:
					if(!Player)
						Player = new cPlayer("PLAYER",this);
					int initX,initY;
					//read
					lineReader.clear();
					lineReader.str(line);
					lineReader>>initX>>initY;
					//initialize position
					int baseX,baseY;
					Scene.getBasePosition(baseX,baseY);
					Player->setInitialCellPosition(initX,initY);
					Player->setCellPosition(initX,initY);
					int screenX,screenY;
					Scene.translateCoordinates(initX,initY,screenX,screenY);
					Player->setPosition(screenX+baseX,screenY+baseY+(PLAYER_HEIGHT>>1));

					//remaining initialization
					Player->initAnimations();
					Player->setStatus(STATUS_STILL);
					Player->setOrientation(ORIENTATION_SOUTH);		
					Player->changeSpeed(0,PLAYER_SPEED_Y);
					Player->setAnimationTicks(getTimer()->getTicks());
					Player->setProjectileTicks(getTimer()->getTicks());
					RECT rc;
					rc.left=11;
					rc.right=21;
					rc.top=2;
					rc.bottom=10;
					Player->setCollisionRect(rc);
					Player->setHP(PLAYER_DEFAULT_HP);
					if (newGame) Player->setNumLives(PLAYER_DEFAULT_LIVES);
					initDefaultProjectileData();

					break;
				case LEVEL_ENEMIES:
					if(line.at(0)==LEVELFILE_CATEGORY_LIST_SEPARATOR && (line.length()<=1 || line.at(1)!=LEVELFILE_CATEGORY_LIST_SEPARATOR))
					{						
						enemies.push_back(currentEnemy);
						isEnemyTypeRead=false;
					}
					else if(isEnemyTypeRead)
						{
							int initX,initY;
							//read
							lineReader.clear();
							lineReader.str(line);
							lineReader>>initX>>initY;
							//initialize position
							int baseX,baseY;
							Scene.getBasePosition(baseX,baseY);
							currentEnemy->setInitialCellPosition(initX,initY);
							currentEnemy->setCellPosition(initX,initY);
							int screenX,screenY;
							Scene.translateCoordinates(initX,initY,screenX,screenY);
							currentEnemy->setPosition(screenX+baseX,screenY+baseY+(ENEMY_HEIGHT>>1));
							
							//Remaining initialization
							currentEnemy->initAnimations();
							currentEnemy->setStatus(STATUS_MOVING);
							currentEnemy->setOrientation(ORIENTATION_SOUTHWEST);		
							currentEnemy->changeSpeed(-ENEMY_SPEED_X,ENEMY_SPEED_Y);
							currentEnemy->setTicks(getTimer()->getTicks());
							currentEnemy->setHP(ENEMY_DEFAULT_HP);
							currentEnemy->setScore(ENEMY_DEFAULT_SCORE);
							RECT rc={8,3,24,9};
							currentEnemy->setCollisionRect(rc);
							currentEnemy->resetSeqFrame();
							currentEnemy->isDead=false;

							isEnemyTypeRead=false;
						}
						else
						{							
							currentEnemy = new cEnemy(line,this);
							isEnemyTypeRead=true;
						}
					break;
				case LEVEL_ITEMS:
					if(line.at(0)==LEVELFILE_CATEGORY_LIST_SEPARATOR && (line.length()<=1 || line.at(1)!=LEVELFILE_CATEGORY_LIST_SEPARATOR))
					{						
						items.push_back(currentItem);
						isItemTypeRead=false;
					}
					else if(isItemTypeRead)
						{
							int initX,initY;
							//read
							lineReader.clear();
							lineReader.str(line);
							lineReader>>initX>>initY;
							//initialize position
							int baseX,baseY;
							Scene.getBasePosition(baseX,baseY);							
							currentItem->setCellPosition(initX,initY);
							int screenX,screenY;
							Scene.translateCoordinates(initX,initY,screenX,screenY);
							currentItem->setPosition(screenX+baseX,screenY+baseY+(ENEMY_HEIGHT>>1));
							
							//Remaining initialization
							currentItem->initAnimations();
							currentItem->setTicks(getTimer()->getTicks());;
							RECT rc={8,3,24,9};
							currentItem->setCollisionRect(&rc);
							currentItem->isDead=false;

							isItemTypeRead=false;
						}
						else
						{							
							if(line.substr(0,2).compare(LEVELFILE_LAST_LEVEL)!=0)
							{
								int type;
								//read
								lineReader.clear();
								lineReader.str(line);
								lineReader>>type;
								
								currentItem = new cItem((TItemType)type);
								isItemTypeRead=true;
							}
						}
					break;
				case LEVEL_CLEARTIME:
					lineReader.clear();
					lineReader.str(line);
					lineReader>>levelCompletionTime;
					levelCompletionTime*=1000;//time is measured in milliseconds
					levelEllapsedTime=0;
					break;
				}
			}
		}
		levelData.close();
		return true;
	}
	else
	{
		//Instantiate the logger and print error message
		cLog *Log = cLog::Instance();		
		Log->Msg("Error loading level");
		return false;
	}
}
void cGame::initDefaultProjectileData()
{
	if(Player)
	{
		TProjectileData* projData = new TProjectileData;
		projData->castingDelay=DEFAULT_PROJECTILE_CASTING_DELAY;
		SetRect(&(projData->collisionRect),4,4,12,12);
		projData->damage=1;
		projData->isPiercing=false;
		projData->lifetime=3;
		projData->speed=PROJECTILE_BASE_SPEED;
		projData->spellName="DEFAULT SPELL";
		projData->textureID=PROJECTILE_INDEX;
		Player->setSelectedProjectileData(projData);
	}
}

void cGame::removeDeadEnemies()
{
	if (!enemies.empty())
	{
		for (TEnemyList::iterator it=enemies.begin();it!=enemies.end();it++)
		{
			if (*it && (*it)->isDead)
			{
				delete (*it);
				(*it)=NULL;
			}
		}
		enemies.erase(std::remove_if(enemies.begin(),enemies.end(),std::bind1st(std::mem_fun(&cGame::isDead), this)),enemies.end());
	}
}
bool cGame::isDead(cEnemy* enemy)
{
	return enemy==NULL;
}
void cGame::removeDeadItems()
{
	if (!items.empty())
	{
		for (TItemList::iterator it=items.begin();it!=items.end();it++)
		{
			if (*it && (*it)->isDead)
			{
				delete (*it);
				(*it)=NULL;
			}
		}
		items.erase(std::remove_if(items.begin(),items.end(),std::bind1st(std::mem_fun(&cGame::isDeadItem), this)),items.end());
	}
}
bool cGame::isDeadItem(cItem* item)
{
	return item==NULL;
}
void cGame::collisionsPE(cEnemy* enemy)
{
	if (Player->hitTest(enemy))
	{
		Player->decreaseHP();
		if (Player->getHP()<=0)
		{
			Player->decreaseLives();
			Player->setStatus(STATUS_DEAD);
			Sound.playSound(PL_DEAD_ID);
		}
		else
		{
			if (Player->getStatus()!=STATUS_DAMAGED && Player->getStatus()!=STATUS_DEAD)
			{
				Sound.playSound(PL_DAMAGED_ID);
				Player->setStatus(STATUS_DAMAGED);
				//Player->findNearestSafeCell();
			}
		}
	}

}
void cGame::collisionsPI(cItem* item)
{
	if (Player->hitTest(item))
	{
		item->onPickup(Player);
		Sound.playSound(item->getSoundId());
		item->isDead=true;
	}

}
void cGame::collisionsPPr()
{
	TProjectileList projectiles=Player->getProjectiles();
	TProjectileList::iterator it=projectiles.begin();
	while (Player->getStatus()!=STATUS_DEAD && it!=projectiles.end())
	{
		cProjectile* currentProj = *it;
		if (Player->hitTest(currentProj) && currentProj->getStatus()==STATUS_TRAVELLING)
		{	
			currentProj->decreaseLifetime();					
			if(!currentProj->getProjectileData().isPiercing || currentProj->getLifetime()==0)
			{
				currentProj->setStatus(STATUS_EXPLODING);
				currentProj->setSeqFrame(0);
			}
			Player->decreaseHP(currentProj->getDamage());
			if (Player->getHP()<=0)	//Player has died
			{
				Player->decreaseLives();
				Player->setStatus(STATUS_DEAD);
				Sound.playSound(PL_DEAD_ID);
			}
			else
			{
				if (Player->getStatus()!=STATUS_DAMAGED && Player->getStatus()!=STATUS_DEAD)
				{
					Sound.playSound(PL_DAMAGED_ID);
					Player->setStatus(STATUS_DAMAGED);				
					//Player->findNearestSafeCell();
				}
			}
		}
		it++;
	}
}
void cGame::collisionsEPr(cEnemy* enemy)
{
	TProjectileList projectiles=Player->getProjectiles();
	TProjectileList::iterator it=projectiles.begin();
	while (enemy->getStatus()!=STATUS_DEAD && it!=projectiles.end())
	{
		cProjectile* currentProj = *it;
		if (enemy->hitTest(currentProj) && currentProj->getStatus()==STATUS_TRAVELLING)
		{	
			currentProj->decreaseLifetime();					
			if(!currentProj->getProjectileData().isPiercing || currentProj->getLifetime()==0)
			{
				currentProj->setStatus(STATUS_EXPLODING);
				currentProj->setSeqFrame(0);
			}
			enemy->decreaseHP(currentProj->getDamage());
			if (enemy->getHP()<=0)	//Player has died
			{
				enemy->setStatus(STATUS_DEAD);
				enemy->resetSeqFrame();
				
				Sound.playSound(ENEMY_DEAD_ID);
			}
			else
				if (enemy->getStatus()!=STATUS_DAMAGED)
				{
					enemy->setStatus(STATUS_DAMAGED);				
					Sound.playSound(ENEMY_DAMAGED_ID);
				}
		}
		it++;
	}
}
void cGame::doLogic(int ticks)
{
	if(!paused){
		//On gameOver/level Cleared, we'll wait for the completion sounds to end
		if ((!gameOver && !levelCleared)|| Sound.isSoundPlaying(DEATH_ID)||Sound.isSoundPlaying(WIN_ID))
		{
			if(!gameOver && !levelCleared)
			{
				levelEllapsedTime+=(ticks-levelLastTick);
				int remainingTime=levelCompletionTime-levelEllapsedTime;
				if (remainingTime/1000<=HURRYUP_DELAY && !hurryUp)
				{
					Sound.playSound(HURRYUP_ID);
					hurryUp=true;
					Sound.speedUpSound(ALEXKIDD_ID,HURRYUP_FREQ_INC);		
					Sound.pauseSound(ALEXKIDD_ID);
				}
				if(hurryUp && !Sound.isSoundPlaying(HURRYUP_ID) && Sound.isSoundPaused(ALEXKIDD_ID))
				{
					Sound.unpauseSound(ALEXKIDD_ID);
				}
				if (levelEllapsedTime>=levelCompletionTime)
				{
					gameOver=true;
					Sound.playSound(DEATH_ID);
					addTotalScore(levelScore);
				}
				else levelLastTick=ticks;
			
				Scene.update(ticks);				
				Player->update(ticks);
				
				if (!Player->inGracePeriod())collisionsPPr();

				for (TItemList::iterator it=items.begin();it!=items.end();it++)
				{
					(*it)->update(ticks);
					if(!((*it)->isDead))
					{						
						collisionsPI((*it));
					}
				}			
				removeDeadItems();				

				for (TEnemyList::iterator it=enemies.begin();it!=enemies.end();it++)
				{
					(*it)->update(ticks);
					if(!((*it)->isDead))
					{
						if (!Player->inGracePeriod())collisionsPE((*it));
						collisionsEPr((*it));
					}
				}			
				removeDeadEnemies();				

				if (enemies.empty())
				{
					Sound.playSound(WIN_ID);
					levelCleared=true;
					//gameOver=true;//victory!!
				}
			}
			else Sound.stopSound(ALEXKIDD_ID);
		}
		//else
		//{	
		//	//if(gameOver)
		//		setNextStateID(EXIT);
		//	//else if (levelCleared)
		//	//	setNextStateID(GAME_LEVEL_CLEARED)
		//}		
	}
}
int cGame::getTotalScore() const
{
	return totalScore;
}
void cGame::addTotalScore(int amount)
{
	totalScore+=amount;
}
void cGame::resetScores()
{
	totalScore=bonusScore=levelScore=0;
}
void cGame::startLevel()
{
	levelCleared=false;
	levelLastTick = timer->getTicks();
	levelEllapsedTime=0;
	levelScore=0;
}
void cGame::resetData()
{
	gameOver=false;	
	paused=false;
	firstLoad=true;
	showMinimap=false;
	hurryUp=false;

	totalScore=0;
	startLevel();

	if(Player)
		Player->clearData();
//	SAFEDELETE(Player);
	for (TEnemyList::iterator it=enemies.begin();it!=enemies.end();it++)
	{
		SAFEDELETE((*it));
	}
	enemies.clear();
	
	for (TItemList::iterator it=items.begin();it!=items.end();it++)
	{
		SAFEDELETE((*it));
	}
	items.clear();
}
void cGame::resetForNewLevel()
{
	showMinimap=false;
	if(Player)
	{
		Player->resetForNewLevel();

	}
	else Player = new cPlayer("PLAYER",this);
	//modify other variables if needed
	for (TEnemyList::iterator it=enemies.begin();it!=enemies.end();it++)
	{
		SAFEDELETE((*it));
	}
	enemies.clear();
	for (TItemList::iterator it=items.begin();it!=items.end();it++)
	{
		SAFEDELETE((*it));
	}
	items.clear();
	startLevel();
}
int cGame::getLevelScore() const
{
	return levelScore;
}
void cGame::setLevelScore(int kscore)
{
	levelScore=kscore;
}
void cGame::addLevelScore(int amount)
{
	levelScore+=amount;
}