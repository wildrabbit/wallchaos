#include "cGame.h"
#include "wcTestState.h"
#include "cMouse.h"
#include "cKeyboard.h"
#include "cSound.h"
#include "cLog.h"

TestState::TestState(cGame *parent):
	GameState(parent)
{
}
void TestState::init()
{
	if(parent)
	{
		if (!parent->loadLevel("level01.wcl"))
		{
			cLog* log = cLog::Instance();
			log->Msg("Error loading level");
			return;
		}
		parent->Graphics.LoadData(&cGraphicsLayer::loadGameData);
		parent->Sound.LoadData(&cSoundLayer::loadGameData);
		parent->Sound.playSound(ALEXKIDD_ID,true);
		//if(parent->Player)
		//	initPlayer();
		//if(parent->Enemy)
		//	initEnemy();
	}
	keyCommands.clear();
	keyCommands[KEY_DIR_E]=ORIENTATION_EAST;
	keyCommands[KEY_DIR_SE]=ORIENTATION_SOUTHEAST;
	keyCommands[KEY_DIR_S]=ORIENTATION_SOUTH;
	keyCommands[KEY_DIR_SW]=ORIENTATION_SOUTHWEST;
	keyCommands[KEY_DIR_W]=ORIENTATION_WEST;
	keyCommands[KEY_DIR_NW]=ORIENTATION_NORTHWEST;
	keyCommands[KEY_DIR_N]=ORIENTATION_NORTH;
	keyCommands[KEY_DIR_NE]=ORIENTATION_NORTHEAST;
}
void TestState::finalize()
{
	if (parent)
	{
		parent->Graphics.UnLoadData(&cGraphicsLayer::unloadAllData);
		parent->Sound.UnLoadData(&cSoundLayer::unloadAllData);
		parent=NULL;
	}
}
//------------------------
void TestState::handleInput()
{	
}
void TestState::dirKeyPressed(int mask)
{
	TOrientation direction=keyCommands[(TKeyDirs)mask];
	cPlayer* player=parent->Player;
	if (player->getStatus()!=STATUS_DEAD)
	{	
		player->setStatus(STATUS_MOVING);
		if(player->getOrientation()==direction)
		{
			player->move(&(parent->Scene));
		}
		else {
			player->setOrientation(direction);
			int speedX=0,speedY=0;
			if(mask&DIK_W_MASK)
				speedY=-PLAYER_SPEED_Y;
			else if (mask & DIK_S_MASK)
				speedY=PLAYER_SPEED_Y;
			if(mask&DIK_A_MASK)
				speedX=-PLAYER_SPEED_X;
			else if (mask&DIK_D_MASK)
				speedX=+PLAYER_SPEED_X;
			player->changeSpeed(speedX,speedY);
		}					
	}
}
void TestState::logic(int ticks){
}
void TestState::render()
{
	if (parent)
	{	
		TRenderingData renderingData;
		parent->Input.GetMousePosition(&renderingData.mouseX,&renderingData.mouseY);
		renderingData.clickedX=clickedX;
		renderingData.clickedY=clickedY;
		renderingData.scene=&(parent->Scene);
		renderingData.player=parent->Player;
		//renderingData.enemy=parent->Enemy;
		renderingData.enemies=parent->enemies;
		parent->Graphics.Render(&renderingData,&cGraphicsLayer::renderGameScene);
	}
}
void TestState::queryForScrolling(cKeyboard* keyboard)
{
	if(keyboard)
	{
		cScene* scene = &(parent->Scene);
		int oldBaseX,oldBaseY;
		scene->getBasePosition(oldBaseX,oldBaseY);
		if (keyboard->KeyDown(DIK_LEFTARROW))
		{
			if(oldBaseX>(1-SCENE_WIDTH)*(TILE_WIDTH>>1)-OFF_LIMITS_INC_X)	
					scene->setBasePosition(oldBaseX-SCROLL_AMOUNT_X,oldBaseY);
		}
		else if (keyboard->KeyDown(DIK_RIGHTARROW))
		{
			if(oldBaseX<(((1-SCENE_WIDTH)*TILE_WIDTH >>1)+(TILE_WIDTH*SCENE_WIDTH))-SCREEN_RES_X+OFF_LIMITS_INC_X)				
				scene->setBasePosition(oldBaseX+SCROLL_AMOUNT_X,oldBaseY);
		}
		else if (keyboard->KeyDown(DIK_UPARROW))
			{
				if (oldBaseY>-OFF_LIMITS_INC_Y)
					scene->setBasePosition(oldBaseX,oldBaseY-SCROLL_AMOUNT_Y);
			}
			else if (keyboard->KeyDown(DIK_DOWNARROW))
			{
				if((oldBaseY<TILE_HEIGHT*SCENE_HEIGHT-SCREEN_RES_Y+OFF_LIMITS_INC_Y))
					scene->setBasePosition(oldBaseX,oldBaseY+SCROLL_AMOUNT_Y);											
			}
	}
}
