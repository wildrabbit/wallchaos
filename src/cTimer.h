#ifndef TIMER_H_INCLUDED
#define TIMER_H_INCLUDED

/**
	Timer adapted from Lazyfoo.net's tutorial, 'Advanced timers',
	at URL: http://lazyfoo.net/SDL_tutorials/lesson13/index.php
*/
//Type for the timer's statuses
typedef enum
{
	STOPPED=-1,
	PAUSED,
	RUNNING
} TimerStatus;
//Timer class declaration
 class Timer
 {
	private:
		 //clock time when the timer was started
		 int startTicks;
		 //ticks stored when the timer was paused
		 int pausedTicks;
		 //timer's status
		 TimerStatus status;
	public:
		//Initialization
		Timer();

		//clock actions
		void start();
		void stop();
		void pause();
		void unpause();

		//Get the time
		int getTicks() const;

		//Query the timer's status
		bool isStarted() const;
		bool isPaused() const;
 };
#endif
