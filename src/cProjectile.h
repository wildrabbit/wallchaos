#ifndef PROJECTILEH_H
#define PROJECTILEH_H

#include <map>
#include <string>

#define PROJECTILE_INDEX 7
#define PROJECTILE_PATH "proj00.tga"

#define PROJECTILE_HEIGHT	16
#define PROJECTILE_WIDTH	16

#define PROJECTILE_ANCHOR_OFFSET_X	7
#define PROJECTILE_ANCHOR_OFFSET_Y	7
#define DEFAULT_PROJECTILE_CASTING_DELAY 500

#define PROJECTILE_BASE_SPEED 2
#define FIVE_SQRT	2.236067	//Pre-computed value of sqrt(5), necessary to get speedX and speedY such as
								//the vector's modulo equals the projectile's absolute speed, keeping
								//the 2:1 proportion given by the iso map constraint.
								//Since the speeds and positions are given as integers

typedef enum
{
	STATUS_SPAWNING,
	STATUS_TRAVELLING,	//Default status
	STATUS_BOUNCING,	//Bouncing against walls
	STATUS_EXPLODING,	//Making impact against an enemy, or the player himself
	STATUS_DYING		//Bounce limit exceeded: the projectile dies
}TProjectileStatus;

typedef struct
{
	int castingDelay;
	int damage;
	int lifetime;
	int textureID;
	TAnimations animations;
	std::string spellName;
	RECT collisionRect;
	void initAnimations(std::string fname="");
	bool isPiercing;
	int speed;
} TProjectileData;

typedef std::map<std::string, TProjectileData> TProjectileSpellbook;
 
class cPlayer;
class cProjectile
{
private:
	std::string id;
	
	int cellX,cellY;
	int posX,posY;
	int elevation;//vertical offset to simulate height above the tile

	TOrientation facing;
	int speedX,speedY;

	int animationTicks;
	TProjectileStatus status;

	//Current animation data
	int seqFrame;
	int seqTotalFrames;
	int seqDelay;

	//ProjectileInfo holds the "projectile type" default values.
	//During the game, a projectile instance can have its attributes
	//influenced by items, collisions, or others.
	int lifetime;
	int damage;
	int castingDelay;

	cPlayer* caster;
	TProjectileData projectileInfo;

	RECT collisionRect;

	inline void resolveNorth(int newX,int newY,bool wallNE, bool wallNW);
	inline void resolveEast(int newX,int newY,bool wallNE, bool wallSE);
	inline void resolveSouth(int newX,int newY,bool wallSE, bool wallSW);
	inline void resolveWest(int newX,int newY,bool wallNW, bool wallSW);
	inline void resolveNorthEast();
	inline void resolveNorthWest();
	inline void resolveSouthEast();
	inline void resolveSouthWest();

public:
	cProjectile(std::string id,TProjectileData* projectileData, cPlayer* _parent);
	virtual ~cProjectile();	

	const std::string& getId() const;
	void getCellPosition(int& cX, int& cY) const;
	void setCellPosition(int cX,int cY);
	
	void getPosition(int& sX, int& sY) const;
	void setPosition(int sX, int sY);

	int getElevation() const;
	void setElevation(int elev);

	const TProjectileStatus& getStatus()const;
	void setStatus(const TProjectileStatus& newStatus);

	int getTextureID() const;
	RECT getTextureDimensions() const;

	void changeSpeed(int speedX, int speedY);
	void move(cScene* scene);

	void setTicks(int ticks);
	void update(int ticks);

	TProjectileData getProjectileData() const;
	void setProjectileData(const TProjectileData& projectileData);

	const TOrientation& getOrientation()const;
	void setOrientation(const TOrientation& orientation);

	void setSeqFrame(int seqFrame);

	void initAnimations(std::string fname="");
	RECT getCollisionRect() const;
	void setCollisionRect(const RECT& collisionRect);

	int getCastingDelay() const;
	void setCastingDelay(int delay);
	int getDamage() const;
	void setDamage(int damage);

	void updateAnimations(int ticks);
	void resolveBounce(TOrientation collisionDir,int newX, int newY);

	void initData(int ticks);
	void decreaseLifetime();
	int getLifetime() const;

	bool isDead;

};

#endif