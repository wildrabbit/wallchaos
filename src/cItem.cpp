#include "cItem.h"
#include "cPlayer.h"
#include "cGame.h"
cItem::cItem(TItemType type):type(type){}
cItem::~cItem(){}

void cItem::getCellPosition(int& cX, int& cY) const
{
	cX=cellX;
	cY=cellY;
}
void cItem::setCellPosition(int cX,int cY)
{
	cellX=cX;
	cellY=cY;
}
void cItem::getPosition(int& sX, int& sY) const
{
	sX=posX;
	sY=posY;
}
void cItem::setPosition(int sX, int sY)
{
	posX=sX;
	posY=sY;
}
void cItem::setTicks(int ticks)
{
	animTicks=ticks;
}
void cItem::onPickup (cPlayer* reciever)
{
	if (reciever)
	{
		switch(type)
		{
			case ITEM_HP:
				this->increaseHP(reciever);
				break;
			case ITEM_COIN:
				this->increaseScore(reciever);
				break;
		}
	}
}
RECT cItem::getCollisionRect() const
{
	return collisionRect;
}
void cItem::setCollisionRect(RECT* rect)
{
	collisionRect = *rect;
}
int cItem::getTextureID() const
{
	return ITEM_INDEX;
}
RECT cItem::getTextureDimensions() const
{
	RECT rc = {0,0,0,0};
	switch(type)
	{
		case ITEM_COIN:
			SetRect(&rc,seqFrame*ITEM_WIDTH,0,(seqFrame+1)*ITEM_WIDTH,ITEM_HEIGHT);
			break;
		case ITEM_HP:
			SetRect(&rc,seqFrame*ITEM_WIDTH,ITEM_HEIGHT,(seqFrame+1)*ITEM_WIDTH,2*ITEM_HEIGHT);
			break;
	}
	return rc;
}
void cItem::increaseHP(cPlayer* reciever)
{
	if (reciever && reciever->getHP()<PLAYER_DEFAULT_HP)
	{
		if (reciever->getHP()+HP_HEALTH_INCREASE>=PLAYER_DEFAULT_HP)
			reciever->setHP(PLAYER_DEFAULT_HP);
		else
		reciever->setHP((reciever->getHP()+HP_HEALTH_INCREASE));//So it doesn't exceed the limit
	}
}
void cItem::increaseScore(cPlayer* reciever)
{
	if(reciever && reciever->getParent())
	{
		reciever->getParent()->addLevelScore(COIN_SCORE_INCREASE);
	}
}
void cItem::initAnimations()
{
	seqFrame=0;
	seqDelay=ITEM_ANIM_DELAY;
	seqTotalFrames=ITEM_ANIM_TOTAL_FRAMES;
}

void cItem::update(int ticks)
{
	if (ticks-animTicks>=seqDelay)
	{
		seqFrame=(seqFrame+1)%seqTotalFrames;
		animTicks= ticks;
	}
}
std::string cItem::getSoundId() const
{
	switch(type)
	{
		case ITEM_COIN:
			return ITEM_COIN_SND_ID;
			break;
		case ITEM_HP:
			return ITEM_HP_SND_ID;
			break;
	}
	return "";
}
TItemType cItem::getType() const
{
	return type;
}