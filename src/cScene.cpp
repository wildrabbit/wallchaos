
#include "cScene.h"
#include <stdio.h>
#include <stdlib.h>


cScene::cScene():baseX(-(SCREEN_RES_X)>>1),baseY(-OFF_LIMITS_INC_Y) {}
cScene::~cScene(){}

void cScene::setBasePosition(int bX, int bY)
{
	baseX=bX;
	baseY=bY;
}
void cScene::getBasePosition(int& bX,int& bY) const
{
	bX=baseX;
	bY=baseY;
}
void cScene::TileClicked(int mouseX, int mouseY, int* selectedX,int* selectedY)
{
	//The algorithm used to resolve the clicked cell's coordinates
	//using an isometric Diamond map (as the one used in Age of Empires, for instance)
	//consists of a mix of another two:
	//The first one calculates the "rectangular" cell and  it was taken from
	//the book "Isometric games ..."
	
	//Screen coordinates-> World space
    int tmpX,tmpY;
    tmpX=mouseX+baseX;
    tmpY=mouseY+baseY;

	tmpX+=(TILE_WIDTH>>1);
    
	//tmpX-=((SCREEN_RES_X-TILE_WIDTH)>>1);
	//tmpY-=mouseY-OFF_LIMITS_INC_Y;
    //Then get a tentative tile (it'll have to be adjusted later,though)
	int coarseX=tmpX/TILE_WIDTH;
	int coarseY=tmpY/TILE_HEIGHT;
    
	//and the exact position inside a tile where it was clicked
    int fineX=tmpX%TILE_WIDTH;
    int fineY=tmpY%TILE_HEIGHT;

	//Corrections to adjust negative fine coordinates
    if(fineX<0)
    {
        fineX+=TILE_WIDTH;
        coarseX--;
    }
    if(fineY<0)
    {
        fineY+=TILE_HEIGHT;
        coarseY--;
    }
	//And then we'll get the actual "rectangular" coordinate
    int mapX=0,mapY=0;
    while(coarseY<0)//Move north
    {
        mapX--;
        mapY--;
        coarseY++;
    }
    while(coarseY>0)//Move south
    {
        mapX++;
        mapY++;
        coarseY--;
    }
    while(coarseX<0)//Move west
    {
        mapX--;
        mapY++;
        coarseX++;
    }
    while(coarseX>0)//...and finally, east
    {
        mapX++;
        mapY--;
        coarseX--;
    }
    //The second part of the algorithm is basically
	//an adaptation of the geometric approach used in
	//the original project for diamond maps.
	//The coordinate system in diamond maps is a bit
	//different from rectangular iso maps (staggered?), so
	//the corrections to get the actual cell must also 
	//change
    if(fineX<(TILE_WIDTH>>1))//32
	{
		//Left
		if(fineY<(TILE_HEIGHT>>1))//16
		{
			//Up
			if( (TILE_HEIGHT-fineX)>>1 > fineY ) { mapX-=1;}
		}
		else
		{
			//Down
			if( (TILE_HEIGHT+fineX)>>1 < fineY ) {mapY+=1; }
		}
	}
	else
	{
		fineX-=(TILE_WIDTH>>1);

		//Right
		if(fineY<(TILE_HEIGHT>>1))
		{
			//Up
			if( (fineX>>1) > fineY ) mapY-=1;
		}
		else
		{
			//Down
			if( (TILE_WIDTH-fineX)>>1 < fineY ) mapX+=1;
		}
	}
	*selectedX=mapX;
	*selectedY=mapY;

}

void cScene::TileSelected(int mx, int my, int *tx, int *ty, int *atx, int *aty, int *Dx, int *Dy)
{
	int rx,ry,dx,dy;

	*tx=mx-(SCENE_Xo);
	*ty=my-(SCENE_Yo);

	rx=*tx%64;
	ry=*ty%32;

	*tx=*tx>>6;
	*ty=(*ty>>5)<<1;

	*atx=*tx;
	*aty=*ty;

	dx=0;
	dy=0;

	//         /\
	// -1,-1  /  \   0,-1
	//       /0,0 \
	//       \    /
	// -1, 1  \  /   0, 1
	//         \/

	if(rx<32)
	{
		//Left
		if(ry<16)
		{
			//Up
			if( (16-(rx>>1)) > ry ) { dx=-1; dy=-1; }
		}
		else
		{	
			//Down
			if( (16+(rx>>1)) < ry ) { dx=-1; dy=1; }
		}
	}
	else
	{
		rx-=32;

		//Right
		if(ry<16)
		{
			//Up
			if( (rx>>1) > ry ) dy=-1;
		}
		else
		{	
			//Down
			if( (32-(rx>>1)) < ry ) dy=1;
		}
	}

	*tx+=dx;
	*ty+=dy;

	*Dx=dx;
	*Dy=dy;
}
void cScene::initTile(TTile* tile)
{
	switch(tile->index)
	{
		case FLOOR_00:case FLOOR_01:case FLOOR_02:case FLOOR_03:
		{
			tile->delay=0;
			tile->frame=0;
			tile->totalFrames=1;
			tile->walkable=true;
			tile->wall=false;
			break;
		}
		case TOXIC_FLOOR:
		{
			tile->delay=200;
			tile->frame=0;
			tile->totalFrames=4;
			tile->walkable=false;
			tile->wall=false;
			break;
		}
		case WALL_00: case WALL_01: case WALL_02: case WALL_03:
		{
			tile->delay=0;
			tile->frame=0;
			tile->totalFrames=1;
			tile->walkable=false;
			tile->wall=true;
			break;
		}
		default:break;
	}
	
}

void cScene::translateCoordinates(int cX, int cY, int &screenX, int &screenY)const
{
	screenX=(cX-cY)*(TILE_WIDTH>>1)-baseX;
	screenY=(cX+cY)*(TILE_HEIGHT>>1)-baseY;
}
void cScene::getCellsAt(int cX, int cY, TTile*& cells, int& cellsSize)
{
	if (cX>=0 && cX<SCENE_WIDTH && cY>=0 && cY<SCENE_HEIGHT)
	{
		cells=map[cY][cX];
		cellsSize=SCENE_LAYERS;
	}
}
bool cScene::findFirstFreeCell(int& cX,int&cY) const
{
	bool freeCell=false;
	int y=0;
	int x,z;
	cX=-1;cY=-1;
	while (!freeCell && y<SCENE_HEIGHT)
	{
		x=0;
		while(x<SCENE_WIDTH)
		{
			z=1;
			while (z<SCENE_LAYERS)
			{
				if (map[y][x][z].index!=NOTILE)
					break;//exit z loop
				z++;
			}
			if (z==SCENE_LAYERS){
				cX=x;cY=y;
				break;//exit x loop
			}
			x++;
		}
		if (cX>=0 && cY>=0)
			break;//exit y loop
		y++;
	}
	return freeCell;
}
bool cScene::isFreeCell(int cX,int cY)
{
	//true if map[cy][cx][0] is walkable and the subsequent layers
	//aren't walls
	TTile* cells;
	//Ensure coordinates are valid!
	if(cX<0 || cY<0 || cX>=SCENE_WIDTH || cY>=SCENE_HEIGHT)
		return false;
	int cellsSize;
	getCellsAt(cX,cY,cells,cellsSize);
	if (cells)
	{
		bool isFree=cells[0].walkable;
		if(isFree)
		{
			for (int i=1;i<cellsSize && isFree;i++)
			{
				if (cells[i].index!=NOTILE)
					isFree=false;
			}
		}
		return isFree;
	}
	return false;
}
bool cScene::isWallCell(int cX,int cY)
{
	//Ensure coordinates are valid!
	if(cX<0 || cY<0 || cX>=SCENE_WIDTH || cY>=SCENE_HEIGHT)
		return true;
	int cellsSize;
	TTile* cells;
	getCellsAt(cX,cY,cells,cellsSize);
	if (cells)
		return cells[1].index!=NOTILE && !cells[1].walkable; //the second check will be
	return true;
}

void cScene::setTicks(int ticks)
{
for (int z=0;z<SCENE_LAYERS;z++)
		for (int y=0;y<SCENE_HEIGHT;y++)
			for (int x=0;x<SCENE_WIDTH;x++)
				map[y][x][z].ticks=ticks;
}
void cScene::update(int ticks)
{
	for (int z=0;z<SCENE_LAYERS;z++)
		for (int y=0;y<SCENE_HEIGHT;y++)
			for (int x=0;x<SCENE_WIDTH;x++)
			{
				TTile* currTile = &map[y][x][z];
				if (currTile->index!=NOTILE && ticks-(currTile->ticks)>currTile->delay)
				{
					currTile->frame=(currTile->frame+1)%currTile->totalFrames; 
					currTile->ticks=ticks;
				}				
			}
}
void cScene::setMap(TTile newMap[SCENE_HEIGHT][SCENE_WIDTH][SCENE_LAYERS])
{
	for (int z=0;z<SCENE_LAYERS;z++)
		for(int y=0;y<SCENE_HEIGHT;y++)
			for (int x=0;x<SCENE_WIDTH;x++)
			{
				map[y][x][z].index=(newMap[y][x][z]).index;
				initTile(&map[y][x][z]);
			}
}
RECT cScene::getTileRect(TTile* tile) const
{
	RECT rc={0,0,0,0};
	if(tile)
	{
		switch(tile->index)
		{		
		case FLOOR_00:
			SetRect(&rc,0,0,TILE_WIDTH,TILE_HEIGHT);
			break;
		case FLOOR_01:
			SetRect(&rc,TILE_WIDTH,0,2*TILE_WIDTH,TILE_HEIGHT);
			break;
		case FLOOR_02:
			SetRect(&rc,2*TILE_WIDTH,0,3*TILE_WIDTH,TILE_HEIGHT);
			break;
		case FLOOR_03:
			SetRect(&rc,3*TILE_WIDTH,0,4*TILE_WIDTH,TILE_HEIGHT);
			break;
		case TOXIC_FLOOR:
			SetRect(&rc,tile->frame*TILE_WIDTH,TILE_HEIGHT,(tile->frame+1)*TILE_WIDTH,2*TILE_HEIGHT);
			break;
		case WALL_00:
			SetRect(&rc,0,2*TILE_HEIGHT,TILE_WIDTH,4*TILE_HEIGHT);
			break;
		case WALL_01:
			SetRect(&rc,TILE_WIDTH,2*TILE_HEIGHT,2*TILE_WIDTH,4*TILE_HEIGHT);
			break;
		case WALL_02:
			SetRect(&rc,2*TILE_WIDTH,2*TILE_HEIGHT,3*TILE_WIDTH,4*TILE_HEIGHT);
			break;
		case WALL_03:
			SetRect(&rc,3*TILE_WIDTH,2*TILE_HEIGHT,4*TILE_WIDTH,4*TILE_HEIGHT);
			break;
		default: break;
		}
	}
	return rc;
}