#ifndef ENEMYH_H
#define ENEMYH_H

#include <string>
#include "cPlayer.h"

#define ENEMY_INDEX	6
#define ENEMY_PATH	"enemy00.png"

#define ENEMY_SPEED_X 4
#define ENEMY_SPEED_Y 2

#define ENEMY_WIDTH 32
#define ENEMY_HEIGHT 32

#define ENEMY_ANCHOR_OFFSET_X	16
#define ENEMY_ANCHOR_OFFSET_Y	26

#define ENEMY_DEFAULT_SCORE 500
#define ENEMY_DEFAULT_HP 3

#define DETOUR_CHANCE 0.005

class cGame;

//[[TODO]] This class and the Player one (well, and even cProjectile) have lots of common fields
// Implement a common base class, from which they'll both inherit.
// At least, until the experiment toying around with component systems proves to be actually 
// useful.

class cEnemy
{
protected:
	cGame* parent;
	std::string id;
	//Positional data
	int cellX,cellY;	//in cells
	int posX,posY;		//in pixels (world coordinates!)
	int initialCellX, initialCellY;
	//Direction the char's facing to
	TOrientation facing;
	int speedX,speedY;

	//Movement data
	//ITrajectory* trajectory;
	int ticks;

	//Behaviour data (i.e: status)
	TPlayerStatus status;	
	
	//Current animation data
	int seqFrame;
	int seqTotalFrames;
	int seqDelay;
	TAnimations animations;

	//Collision data
	RECT collisionRect;

	int hp;
	int score;

	void correctFacing();

public:
	cEnemy(std::string id, cGame* parent);
	virtual ~cEnemy();
	const std::string& getId() const;
	void getCellPosition(int& cX, int& cY) const;
	void setCellPosition(int cX,int cY);
	
	void getPosition(int& sX, int& sY) const;
	void setPosition(int sX, int sY);
	void getInitialCellPosition(int& cX, int& cY) const;
	void setInitialCellPosition(int cX,int cY);

	const TOrientation& getOrientation()const;
	void setOrientation(const TOrientation& orientation);

	const TPlayerStatus& getStatus()const;
	void setStatus(const TPlayerStatus& newStatus);

	int getTextureID() const;
	RECT getTextureDimensions() const;

	void changeSpeed(int speedX, int speedY);
	void move(cScene* scene);

	void setTicks(int ticks);
	void update(int ticks);
	void resetSeqFrame();

	void initAnimations(std::string fname="");
	RECT getCollisionRect() const;
	void setCollisionRect(const RECT& rect);

	bool hitTest(cEnemy* enemy);
	bool hitTest(cProjectile* projectile);

	int getHP() const;
	void setHP(int hp);
	void decreaseHP(int amount=1);
	void increaseHP(int amount=1);

	cGame* getParent();

	bool isDead;

	int getScore() const;
	void addScore(int newScore);
	void setScore(int newScore);
};

#endif