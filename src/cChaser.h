#ifndef CCHASERH_H
#define CCHASERH_H

#include <string>
#include "cEnemy.h"

class cGame;
class cChaser: public cEnemy
{
	private:
		//cMovementStrategy* strategy; //this should belong to a base class, and
									//the derived classes would instantiate
									//a particular movement strategy.
	public:
		cChaser(std::string id, cGame* parent);
		RECT getTextureDimensions() const;
		void move(cScene* scene);
		void update(int ticks);
		void initAnimations(std::string fname="");
		~cChaser();
};
#endif