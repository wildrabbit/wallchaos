#ifndef CSOUNDH_H
#define CSOUNDH_H

#include <string>
#include "fmod.h"

#define MAX_VOLUME		255
#define VOLUME_DELTA	5

class cSoundItem
{
protected: 
	std::string id;
	int channel;
	int volume;
	int defaultFreq;
public:
	const std::string& getId() const;
	const int& getChannel() const;
	const int& getVolume() const;
	const int& getDefaultFreq() const;
	cSoundItem(const std::string& soundId);
	virtual ~cSoundItem(){};
	virtual bool Play(bool loop=false)=0;
	virtual bool Pause()=0;
	virtual bool Unpause()=0;
	virtual bool Stop()=0;
	virtual bool Reset()=0;
	virtual bool isPlaying()=0;
	virtual bool isPaused()=0;
	virtual void setVolume (int volume)=0;
	void raiseVolume();
	void lowerVolume();
	virtual void speedUpSound(float amount=1.25f)=0;
	virtual void slowDownSound(float amount=1.25f)=0;
	virtual void restoreSoundSpeed()=0;

};
class cSample: public cSoundItem
{
private:
	FSOUND_SAMPLE* sample;
public:
	cSample(const std::string& id, FSOUND_SAMPLE* theSample);
	virtual ~cSample();
	bool Play(bool loop=false);
	bool Pause();
	bool Unpause();
	bool Stop();
	bool Reset();
	bool isPlaying();
	bool isPaused();
	void setVolume(int volume);
	void speedUpSound(float amount=1.25f);
	void slowDownSound(float amount=1.25f);
	void restoreSoundSpeed();

};
class cStream: public cSoundItem
{
private:
	FSOUND_STREAM* stream;
public:
	cStream(const std::string& id, FSOUND_STREAM* theStream);
	virtual ~cStream();
	bool Play(bool loop=false);
	bool Pause();
	bool Unpause();
	bool Stop();
	bool Reset();
	bool isPlaying();
	bool isPaused();
	void setVolume(int volume);
	void speedUpSound(float amount=1.25f);
	void slowDownSound(float amount=1.25f);
	void restoreSoundSpeed();
};
class cSequence: public cSoundItem
{
private:
	FMUSIC_MODULE* sequence;
public:
	cSequence(const std::string& id, FMUSIC_MODULE* theSequence);
	virtual ~cSequence();
	bool Play(bool loop=false);
	bool Pause();
	bool Unpause();
	bool Stop();
	bool Reset();
	bool isPlaying();
	bool isPaused();
	void setVolume(int volume);
	void speedUpSound(float amount=1.25f);
	void slowDownSound(float amount=1.25f);
	void restoreSoundSpeed();
};


#endif